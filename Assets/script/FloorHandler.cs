﻿using System;
using System.Collections.Generic;
public static class FloorHandler{

    private static RESTfulHandler RESTFUL_SERVICE;
    private static string message;
    private static Boolean responseFlag;
    private static Action<string> globalCallBack;
    static FloorHandler()
    {
        message = null;
        responseFlag = false;
        globalCallBack = setMessage;
        RESTFUL_SERVICE = new RESTfulHandler(globalCallBack);
    }

    public static void setMessage(string response)
    {
        message = response;
        responseFlag = true;
    }

    public static string getMessage()
    {
        if (responseFlag)
        {
            responseFlag = false;
            return message;
        }
        else return null;
    }

    public static void addFloor(int floorID, string floorName, int baseMultiplier, string monsterTable, string itemTable, int floorNumber)
    {
        
        responseFlag = false;
        Dictionary<string,object> jsonOBJ = new Dictionary<string,object>();
        
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_ID        ,floorID);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_NAME      ,floorName);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_BASE_MULTIPLIER   ,baseMultiplier);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_MONSTER_TABLE     ,monsterTable);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_ITEM_TABLE        ,itemTable);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_FLOOR_NUMBER      ,floorNumber);
        RESTFUL_SERVICE.WebReqPOSTclassAsync(DATABASE_CONSTANTS.DUNGEONDATA_CLASSNAME,jsonOBJ);

    }


    public static void getFloor(int floorID)
    {
        responseFlag = false;
        Dictionary<string, object> queryParam = new Dictionary<string, object>();
        queryParam.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_ID     ,floorID);
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.DUNGEONDATA_CLASSNAME,MiniJSON.Json.Serialize(queryParam));
    }

    public static void getShopFloorList()
    {
        responseFlag = false;
        //TO BE : TOWN LEVEL CONSTRAIN
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.DUNGEONDATA_CLASSNAME);
    }

    internal static void deleteFloor(string objectID)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqDELETEclass(DATABASE_CONSTANTS.DUNGEONDATA_CLASSNAME, objectID);
    }

    internal static void UpdateFloor(string objectID, int floorID, string floorName, int baseMultiplier, string monsterTable, string itemTable,int floorNumber)
    {
        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_ID, floorID);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_NAME, floorName);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_BASE_MULTIPLIER, baseMultiplier);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_MONSTER_TABLE, monsterTable);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_ITEM_TABLE, itemTable);
        jsonOBJ.Add(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_FLOOR_NUMBER, floorNumber);
        RESTFUL_SERVICE.WebReqPUTclassAsync(DATABASE_CONSTANTS.DUNGEONDATA_CLASSNAME,objectID,jsonOBJ);
    }

    public static Dictionary<int,int> RandomizeFloor(Dictionary<int, int> DropableFloors, int floorLevel = 1,int DropAmount = 1)
    {
        List<int> FloorsPool = new List<int>();
        foreach (KeyValuePair<int, int> anFloor in DropableFloors)
        {
            for (int i = 0; i < anFloor.Value; i++)
            {
                FloorsPool.Add(anFloor.Key);
            }
        }

        Dictionary<int,int> dropList = new Dictionary<int,int>();
        for (int i = 0; i < DropAmount; i++ )
        {
            dropList.Add(FloorsPool[new System.Random().Next(0, FloorsPool.Count - 1)],floorLevel);
        }
            return dropList;
    }


}
