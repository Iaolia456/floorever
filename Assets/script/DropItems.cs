﻿using System;
using System.Collections;
using System.Collections.Generic;

public class DropItems{

    private Action<string> callBackMethod;


    public DropItems(Action<string> callBackMethod)
    {
        this.callBackMethod = callBackMethod;
    }

    /*
     * Return a randomized item ID from Drop Table
     * @param DropableItems<int ItemID,int DropChance> 
     * */
    public static int RandomizeItem(Dictionary<int,int> DropableItems)
    {
        List<int> ItemsPool = new List<int>();
        foreach(KeyValuePair<int,int> anItem in DropableItems)
        {
            for(int i = 0; i < anItem.Value; i++)
            {
                ItemsPool.Add(anItem.Key);
            }
        }
        return ItemsPool[new System.Random().Next(0, ItemsPool.Count - 1)];
    }


    public void updateDropStat(string response)
    {
        Dictionary<int, int> DropPool = new Dictionary<int,int>();
        Dictionary<string,object> queriedItemData = (Dictionary<string,object>)MiniJSON.Json.Deserialize(response);
        string[] jsonArray = (string[])queriedItemData["results"];
        foreach(string anArray in jsonArray)
        {
            Dictionary<string, object> anJsonArray = (Dictionary<string,object>)MiniJSON.Json.Deserialize(anArray);
            DropPool.Add((int)anJsonArray["ItemID"], (int)anJsonArray["DropRate"]);
        }
        callBackMethod(MiniJSON.Json.Serialize(DropPool));
    }


    public void getDropStat(List<int> EligibleItemList)
    {
        RESTfulHandler restfulHandler = new RESTfulHandler(updateDropStat);
        Dictionary<string, object> itemID = new Dictionary<string, object>();
        Dictionary<string,object> whereIn = new Dictionary<string,object>();
        whereIn.Add("$in",EligibleItemList.ToArray());
        itemID.Add("ItemID",MiniJSON.Json.Serialize(whereIn));

        restfulHandler.WebReqGETclass("ItemData", MiniJSON.Json.Serialize(itemID));
    }
}
