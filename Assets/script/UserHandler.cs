﻿using System;
using System.Collections.Generic;
using MiniJSON;

public static class UserHandler
{

    private static RESTfulHandler RESTFUL_SERVICE;
    private static string message;
    private static Boolean responseFlag;
    private static Action<string> globalCallBack;
    static UserHandler()
    {
        message = null;
        responseFlag = false;
        globalCallBack = setMessage;
        RESTFUL_SERVICE = new RESTfulHandler(globalCallBack);
    }

    public static void setMessage(string response)
    {
        message = response;
        responseFlag = true;
    }

    public static string getMessage()
    {
        if (responseFlag)
        {
            responseFlag = false;
            return message;
        }
        else return null;
    }

    public static void addUser(string username, string password, string posessedConsumables, string inventory, int gold)
    {

        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.USER_COLUMN_CONSUMABLE, posessedConsumables);
        jsonOBJ.Add(DATABASE_CONSTANTS.USER_COLUMN_INVENTORY, inventory);
        jsonOBJ.Add(DATABASE_CONSTANTS.USER_COLUMN_GOLD, gold);

        RESTFUL_SERVICE.WebReqRegisUserAsync(username,password, Json.Serialize(jsonOBJ)); ;

    }


    public static void getCurrentUser(string sessionToken)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqGetCurrentUserAsync(sessionToken);
    }

    public static void login(string username, string password)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqLoginUserAsync(username, password);
    }

    

    /*internal static void deleteUser(string objectID)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqDELETEclass(DATABASE_CONSTANTS.USERDATA_CLASSNAME, objectID);
    
    }*/


    internal static void UpdateUser(string sessionToken, string objectID, string inventory, string posessedConsumables, int gold)
    {
        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.USER_COLUMN_CONSUMABLE, posessedConsumables);
        jsonOBJ.Add(DATABASE_CONSTANTS.USER_COLUMN_INVENTORY, inventory);
        jsonOBJ.Add(DATABASE_CONSTANTS.USER_COLUMN_GOLD, gold);

        RESTFUL_SERVICE.WebReqUpdateUserAsync(objectID,sessionToken,Json.Serialize(jsonOBJ));
    }

    /*public static Dictionary<int, int> RandomizeUser(Dictionary<int, int> DropableUsers, int userLevel = 1, int DropAmount = 1)
    {
        List<int> UsersPool = new List<int>();
        foreach (KeyValuePair<int, int> anUser in DropableUsers)
        {
            for (int i = 0; i < anUser.Value; i++)
            {
                UsersPool.Add(anUser.Key);
            }
        }

        Dictionary<int, int> dropList = new Dictionary<int, int>();
        for (int i = 0; i < DropAmount; i++)
        {
            dropList.Add(UsersPool[new System.Random().Next(0, UsersPool.Count - 1)], userLevel);
        }
        return dropList;
    }*/


}
