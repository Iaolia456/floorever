﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.Net;
using System.Text;
using System.IO;
using UnityEngine.UI;
using MiniJSON;

public class FloorEditorButtonHandler : MonoBehaviour
{
    public InputField FloorIDInput,FloorNameInput,MonsterTableInput,ItemTableInput,MonsterIDInput,ItemIDInput,BaseMultiplierInput,ItemChanceInput,objectIDField,httpResponseField,FloorNumberInput;
    Queue updateUIQueue = new Queue();

    // Use this for initialization
    void Start()
    {
        //Ignore MonoDev certificate Error. 
        //WHY U NO TRUST US 
        //Rong Hai Nak Mak T-T 
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        
        //Pass callback Method to Script

        
    }

    // Update is called once per frame
    /*
     * Handle Incoming Queue and update UI according to data received.
     * */
    void Update()
    {
        String responseReceiver = FloorHandler.getMessage();
        if(responseReceiver != null)
        {

            httpResponseField.text = responseReceiver;
            Dictionary<string,object> result = Json.Deserialize(responseReceiver) as Dictionary<string,object>;
            if (result.ContainsKey("results"))
            {
                List<object> resultList = result["results"] as List<object>;
                foreach (Dictionary<string, object> aResult in resultList)
                {
                    loopUpdateComponent(aResult);
                }
            }
            loopUpdateComponent(result);
            
        }

    }

    private void loopUpdateComponent(Dictionary<string,object> result)
    {
        if(result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_ID)) FloorIDInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_ID]).ToString();
        if(result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_NAME)) FloorNameInput.text = result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_NAME].ToString();
        if(result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_BASE_MULTIPLIER)) BaseMultiplierInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_BASE_MULTIPLIER]).ToString();
        if(result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_ITEM_TABLE)) ItemTableInput.text = result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_ITEM_TABLE].ToString();
        if(result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_MONSTER_TABLE)) MonsterTableInput.text = result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_MONSTER_TABLE].ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_FLOOR_NUMBER)) FloorNumberInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_FLOOR_NUMBER]).ToString();
        if(result.ContainsKey(DATABASE_CONSTANTS.OBJECT_ID)) objectIDField.text = result[DATABASE_CONSTANTS.OBJECT_ID].ToString();


    }

    /*
     * Callback method to be pass down async methods. (Http Response)
     * */
    public void addResponseToQueue(string response)
    {
        updateUIQueue.Enqueue(response);
    }

    private void FloorIdOrTypeFieldError()
    {
        httpResponseField.text = "Please ensure that FloorID and FloorType are having value more than 0";
    }
        

    private void ObjectIdError()
    {
        httpResponseField.text = "ObjectID is Empty. Please Get Object from FloorID.";
    }

    public void AddFloor()
    {
        //Dictionary<string,StatStruct<int,float>> StatDict= new Dictionary<string,StatStruct<int,float>>();
        //StatDict.Add(DATABASE_CONSTANTS.ITEMDATA_ITEMSTAT_HPNAME, [Int32.Parse(HPbase.text), float.Parse(HPgrowth.text)]);
        //StatDict.Add(DATABASE_CONSTANTS.ITEMDATA_ITEMSTAT_ATKNAME, new StatStruct<int, float>(Int32.Parse(ATKbase.text), float.Parse(ATKgrowth.text)));
        //StatDict.Add(DATABASE_CONSTANTS.ITEMDATA_ITEMSTAT_DEFNAME, new StatStruct<int, float>(Int32.Parse(DEFbase.text), float.Parse(DEFgrowth.text)));

        //Debug.Log("FloorID : " + Int32.Parse(FloorID.text) + "\t\tFloor Name : " + FloorName.text + "\t\tFloor Price : " + Int32.Parse(FloorPrice.text) + "\t\tFloor Type : " + Int32.Parse(FloorType.text));
        //foreach(KeyValuePair<string,StatStruct<int,float>> aPair in StatDict)
        //{
        //    Debug.Log(aPair.Key + " " + aPair.Value.BaseStat() + " " + aPair.Value.BaseGrowth());
        //}
        int floorID = -1;
        string floorName = "UNDEFINED";
        int multiplier = 0;
        string monsterTable= "";
        string itemTable = "";
        int floorNumber = 1;



        if (FloorIDInput.text.Length > 0) floorID = Int32.Parse(FloorIDInput.text);
        if (FloorNameInput.text.Length > 0) floorName = FloorNameInput.text;
        if (BaseMultiplierInput.text.Length > 0) multiplier = Int32.Parse(BaseMultiplierInput.text);
        if (MonsterTableInput.text.Length > 0) monsterTable = MonsterTableInput.text;
        if (ItemTableInput.text.Length > 0) itemTable = ItemTableInput.text;
        if (FloorNumberInput.text.Length > 0) floorNumber = Int32.Parse(FloorNumberInput.text);

        if (floorID >= 0 )
        {
            FloorHandler.addFloor(floorID, floorName, multiplier, monsterTable, itemTable,floorNumber);
            httpResponseField.text = "Adding Floor...";
        }else
        {
            FloorIdOrTypeFieldError();
        }
    }

    public void GetFloorFromID()
    {
        int floorID = -1;
        if (FloorIDInput.text.Length > 0) floorID = Int32.Parse(FloorIDInput.text);
        
        if(floorID >= 0)
        {
            FloorHandler.getFloor(floorID);
            httpResponseField.text = "Getting Floor...";
        }else
        {
            FloorIdOrTypeFieldError();
        }
    }

    public void DeleteFloor()
    {
        if(objectIDField.text.Length > 0)
        {
            string objectID = objectIDField.text;
            FloorHandler.deleteFloor(objectID);
        }
        else
        {
            ObjectIdError();
        }
    }

    public void UpdateFloor()
    {
        if(objectIDField.text.Length > 0)
        {
            int floorID = -1;
            string floorName = "UNDEFINED";
            int multiplier = 0;
            string monsterTable = "";
            string itemTable = "";
            int floorNumber = 1;


            if (FloorIDInput.text.Length > 0) floorID = Int32.Parse(FloorIDInput.text);
            if (FloorNameInput.text.Length > 0) floorName = FloorNameInput.text;
            if (BaseMultiplierInput.text.Length > 0) multiplier = Int32.Parse(BaseMultiplierInput.text);
            if (MonsterTableInput.text.Length > 0) monsterTable = MonsterTableInput.text;
            if (ItemTableInput.text.Length > 0) itemTable = ItemTableInput.text;
            if (FloorNumberInput.text.Length > 0) floorNumber = Int32.Parse(FloorNumberInput.text);

            if (floorID >= 0)
            {
                FloorHandler.UpdateFloor(objectIDField.text, floorID, floorName, multiplier, monsterTable, itemTable, floorNumber);
                httpResponseField.text = "Updating Floor...";
            }else
            {
                FloorIdOrTypeFieldError();
            }
            }else
            {
                ObjectIdError();
            }
        }

    public void addItemtoTable()
    {
        List<object> oldItemTable = new List<object>();
        if (ItemTableInput.text.Length > 0) oldItemTable = Json.Deserialize(ItemTableInput.text) as List<object>;
        oldItemTable.Add(new KeyValuePair<int, int>(Convert.ToInt32(ItemIDInput.text), Convert.ToInt32(ItemChanceInput.text)));

        ItemTableInput.text = Json.Serialize(oldItemTable);
        
    }
    public void addMonstertoTable()
    {
        List<object> oldMonsterTable = new List<object>();
        if (MonsterTableInput.text.Length > 0) oldMonsterTable = Json.Deserialize(MonsterTableInput.text) as List<object>;
        oldMonsterTable.Add(MonsterIDInput.text);
        MonsterTableInput.text = Json.Serialize(oldMonsterTable);
    }
}
