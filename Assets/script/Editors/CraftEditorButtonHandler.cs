﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.Net;
using System.Text;
using System.IO;
using UnityEngine.UI;
using MiniJSON;

public class CraftEditorButtonHandler : MonoBehaviour
{

    public InputField CraftIDInput,ResultItemIDInput,GoldCostInput,ResourceTableInput,ItemIDInput,ItemNumberInput,httpResponseField,objectIDField;
    Queue updateUIQueue = new Queue();

    // Use this for initialization
    void Start()
    {
        //Ignore MonoDev certificate Error. 
        //WHY U NO TRUST US 
        //Rong Hai Nak Mak T-T 
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        
        //Pass callback Method to Script
        
    }

    // Update is called once per frame
    /*
     * Handle Incoming Queue and update UI according to data received.
     * */
    void Update()
    {
        String responseReceiver = CraftHandler.getMessage();
        if(responseReceiver != null)
        {

            httpResponseField.text = responseReceiver;
            Dictionary<string,object> result = Json.Deserialize(responseReceiver) as Dictionary<string,object>;
            if (result.ContainsKey("results"))
            {
                List<object> resultList = result["results"] as List<object>;
                foreach (Dictionary<string, object> aResult in resultList)
                {
                    loopUpdateComponent(aResult);
                }
            }
            loopUpdateComponent(result);
            
        }

    }

    private void loopUpdateComponent(Dictionary<string,object> result)
    {
        if (result.ContainsKey(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_CRAFTID)) CraftIDInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.CRAFTDATA_COLUMN_CRAFTID]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_ITEMID)) ResultItemIDInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.CRAFTDATA_COLUMN_ITEMID]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_GOLD_COST)) GoldCostInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.CRAFTDATA_COLUMN_GOLD_COST]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_RESOURCE_TABLE)) ResourceTableInput.text = Convert.ToString(result[DATABASE_CONSTANTS.CRAFTDATA_COLUMN_RESOURCE_TABLE]);
        if(result.ContainsKey(DATABASE_CONSTANTS.OBJECT_ID)) objectIDField.text = result[DATABASE_CONSTANTS.OBJECT_ID].ToString();


    }

    /*
     * Callback method to be pass down async methods. (Http Response)
     * */
    public void addResponseToQueue(string response)
    {
        updateUIQueue.Enqueue(response);
    }

    private void CraftIdOrTypeFieldError()
    {
        httpResponseField.text = "Please ensure that CraftID and CraftType are having value more than 0";
    }
        

    private void ObjectIdError()
    {
        httpResponseField.text = "ObjectID is Empty. Please Get Object from CraftID.";
    }

    public void AddCraft()
    {

        int craftID = -1;
        int itemID = -1;
        int goldCost = 0;
        string resourceTable = "";



        if (CraftIDInput.text.Length > 0) craftID = Int32.Parse(CraftIDInput.text);
        if (ItemIDInput.text.Length > 0) itemID = Int32.Parse(ItemIDInput.text);
        if (GoldCostInput.text.Length > 0) goldCost = Int32.Parse(GoldCostInput.text);
        if (ResourceTableInput.text.Length > 0) resourceTable = ResourceTableInput.text;
        if (craftID >= 0  && itemID >= 0)
        {
            CraftHandler.addCraft(craftID, itemID, goldCost, resourceTable);
            httpResponseField.text = "Adding Craft...";
        }else
        {
            CraftIdOrTypeFieldError();
        }
    }

    public void GetCraftFromID()
    {
        int craftID = -1;
        if (CraftIDInput.text.Length > 0) craftID = Int32.Parse(CraftIDInput.text);
        
        if(craftID >= 0)
        {
            CraftHandler.getCraft(craftID);
            httpResponseField.text = "Getting Craft...";
        }else
        {
            CraftIdOrTypeFieldError();
        }
    }

    public void DeleteCraft()
    {
        if(objectIDField.text.Length > 0)
        {
            string objectID = objectIDField.text;
            CraftHandler.deleteCraft(objectID);
        }
        else
        {
            ObjectIdError();
        }
    }

    public void UpdateCraft()
    {
        if(objectIDField.text.Length > 0)
        {
            int craftID = -1;
            int itemID = -1;
            int goldCost = 0;
            string resourceTable = "";



            if (CraftIDInput.text.Length > 0) craftID = Int32.Parse(CraftIDInput.text);
            if (ItemIDInput.text.Length > 0) itemID = Int32.Parse(ItemIDInput.text);
            if (GoldCostInput.text.Length > 0) goldCost = Int32.Parse(GoldCostInput.text);
            if (ResourceTableInput.text.Length > 0) resourceTable = ResourceTableInput.text;
            if (craftID >= 0 && itemID >= 0)
            {
                CraftHandler.updateCraft(objectIDField.text,craftID, itemID, goldCost, resourceTable);
                httpResponseField.text = "Updating Craft...";
            }else
            {
                CraftIdOrTypeFieldError();
            }
            }else
            {
                ObjectIdError();
            }
        }

    public void addItemtoTable()
    {
        List<object> oldItemTable = new List<object>();
        if (ResourceTableInput.text.Length > 0) oldItemTable = Json.Deserialize(ResourceTableInput.text) as List<object>;
        oldItemTable.Add(new KeyValuePair<int, int>(Convert.ToInt32(ItemIDInput.text), Convert.ToInt32(ItemNumberInput.text)));

        ResourceTableInput.text = Json.Serialize(oldItemTable);

    }

}
