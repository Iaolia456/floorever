﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.Net;
using System.Text;
using System.IO;
using UnityEngine.UI;
using MiniJSON;

public class DeviceIDUserEditorButtonHandler : MonoBehaviour
{
    public InputField DeviceIDInput, ConsumableInput, GoldInput, InventoryInput, ItemIDInput, ItemLevelInput, httpResponseField, objectIDField;
    public bool loggingInFlag;
    Queue updateUIQueue = new Queue();

    // Use this for initialization
    void Start()
    {
        //Ignore MonoDev certificate Error. 
        //WHY U NO TRUST US 
        //Rong Hai Nak Mak T-T 
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        loggingInFlag = false;
        //Pass callback Method to Script

    }

    // Update is called once per frame
    /*
     * Handle Incoming Queue and update UI according to data received.
     * */
    void Update()
    {
        String responseReceiver = DeviceIDUserHandler.getMessage();
        if (!loggingInFlag)
        {
            if (responseReceiver != null)
            {

                httpResponseField.text = responseReceiver;
                Dictionary<string, object> result = Json.Deserialize(responseReceiver) as Dictionary<string, object>;
                if (result.ContainsKey("results"))
                {
                    List<object> resultList = result["results"] as List<object>;
                    foreach (Dictionary<string, object> aResult in resultList)
                    {
                        loopUpdateComponent(aResult);
                    }
                }
                loopUpdateComponent(result);

            }
        }else
        {
            if (responseReceiver != null)
            {

                httpResponseField.text = responseReceiver;
                Dictionary<string, object> result = Json.Deserialize(responseReceiver) as Dictionary<string, object>;
                if (result.ContainsKey("results"))
                {
                    List<object> resultList = result["results"] as List<object>;
                    if(resultList.Count > 0)
                    {
                        foreach (Dictionary<string, object> aResult in resultList)
                        {
                            loopUpdateComponent(aResult);
                        }
                        loggingInFlag = false;
                    }else
                    {
                        Debug.Log("User Not Found, Creating New User");
                        AddUser();
                        loggingInFlag = false;
                    }
                    
                }
            }
        }

    }

    private void loopUpdateComponent(Dictionary<string, object> result)
    {
        if (result.ContainsKey(DATABASE_CONSTANTS.OBJECT_ID)) objectIDField.text = result[DATABASE_CONSTANTS.OBJECT_ID].ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.DEVICEID_COLUMN_DEVICEID)) DeviceIDInput.text = Convert.ToString(result[DATABASE_CONSTANTS.DEVICEID_COLUMN_DEVICEID]);
        if (result.ContainsKey(DATABASE_CONSTANTS.DEVICEID_COLUMN_CONSUMABLE)) ConsumableInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.DEVICEID_COLUMN_CONSUMABLE]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.DEVICEID_COLUMN_GOLD)) GoldInput.text = Convert.ToInt32(result[DATABASE_CONSTANTS.DEVICEID_COLUMN_GOLD]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.DEVICEID_COLUMN_INVENTORY)) InventoryInput.text = Convert.ToString(result[DATABASE_CONSTANTS.DEVICEID_COLUMN_INVENTORY]);
    }

    /*
     * Callback method to be pass down async methods. (Http Response)
     * */
    public void addResponseToQueue(string response)
    {
        updateUIQueue.Enqueue(response);
    }

    private void UserIdOrTypeFieldError()
    {
        httpResponseField.text = "Please ensure that UserID and UserType are having value more than 0";
    }


    private void ObjectIdError()
    {
        httpResponseField.text = "ObjectID is Empty. Please Get Object from UserID.";
    }

    public void AddUser()
    {

        string deviceID = "";
        int consumable = 0;
        int gold = 0;
        string inventory = "";



        if (DeviceIDInput.text.Length > 0) deviceID = DeviceIDInput.text;
        if (ItemIDInput.text.Length > 0) consumable = Int32.Parse(ItemIDInput.text);
        if (GoldInput.text.Length > 0) gold = Int32.Parse(GoldInput.text);
        if (InventoryInput.text.Length > 0) inventory = InventoryInput.text;
        if (deviceID.Length >= 0)
        {
            DeviceIDUserHandler.addUser(SystemInfo.deviceUniqueIdentifier, consumable.ToString(), inventory, gold);
            httpResponseField.text = "Adding User...";
        }
        else
        {
            UserIdOrTypeFieldError();
        }
    }

    public void GetUserFromID()
    {
        int deviceID = -1;
        if (DeviceIDInput.text.Length > 0) deviceID = Int32.Parse(DeviceIDInput.text);

        if (deviceID >= 0)
        {
            DeviceIDUserHandler.getUser(SystemInfo.deviceUniqueIdentifier);
            httpResponseField.text = "Getting User...";
        }
        else
        {
            UserIdOrTypeFieldError();
        }
    }

    /*public void DeleteUser()
    {
        if (objectIDField.text.Length > 0)
        {
            string objectID = objectIDField.text;
            DeviceIDUserHandler.deleteUser(objectID);
        }
        else
        {
            ObjectIdError();
        }
    }*/

    public void UpdateUser()
    {
        if (objectIDField.text.Length > 0)
        {
            string deviceID = "";
            int consumable = 0;
            int gold = 0;
            string inventory = "";



            if (DeviceIDInput.text.Length > 0) deviceID = DeviceIDInput.text;
            if (ConsumableInput.text.Length > 0) consumable = Int32.Parse(ConsumableInput.text);
            if (GoldInput.text.Length > 0) gold = Int32.Parse(GoldInput.text);
            if (InventoryInput.text.Length > 0) inventory = InventoryInput.text;
            if (deviceID.Length >= 0)
            {
                DeviceIDUserHandler.UpdateUser(objectIDField.text,SystemInfo.deviceUniqueIdentifier, consumable.ToString(), inventory, gold);
                httpResponseField.text = "Adding User...";
            }
            else
            {
                UserIdOrTypeFieldError();
            }
        }
        else
        {
            ObjectIdError();
        }
    }

    public void addItemtoTable()
    {
        List<object> oldItemTable = new List<object>();
        if (InventoryInput.text.Length > 0) oldItemTable = Json.Deserialize(InventoryInput.text) as List<object>;
        oldItemTable.Add(new KeyValuePair<int, int>(Convert.ToInt32(ItemIDInput.text), Convert.ToInt32(ItemLevelInput.text)));

        InventoryInput.text = Json.Serialize(oldItemTable);

    }

    public void LoginSequence()
    {
        loggingInFlag = true;
        DeviceIDUserHandler.getUser(SystemInfo.deviceUniqueIdentifier);
    }

}
