﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.Net;
using System.Text;
using System.IO;
using UnityEngine.UI;
using MiniJSON;

public class ItemEditorButtonHandler : MonoBehaviour
{
    public InputField ItemIDInput,ItemNameInput,ItemPriceInput,ItemTypeInput,HPBaseInput,HPGrowthInput,ATKBaseInput,ATKGrowthInput,DEFBaseInput,DefGrowthInput,objectIDField,httpResponseField,rarityInput,Buyable;
    Queue updateUIQueue = new Queue();

    // Use this for initialization
    void Start()
    {
        //Ignore MonoDev certificate Error. 
        //WHY U NO TRUST US 
        //Rong Hai Nak Mak T-T 
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        

    }

    // Update is called once per frame
    /*
     * Handle Incoming Queue and update UI according to data received.
     * */
    void Update()
    {
        String responseReceiver = ItemHandler.getMessage();
        if(responseReceiver != null)
        {

            httpResponseField.text = responseReceiver;
            Dictionary<string,object> result = Json.Deserialize(responseReceiver) as Dictionary<string,object>;
            if (result.ContainsKey("results"))
            {
                List<object> resultList = result["results"] as List<object>;
                foreach (Dictionary<string, object> aResult in resultList)
                {
                    loopUpdateComponent(aResult);
                }
            }
            loopUpdateComponent(result);
            
        }

    }

    private void loopUpdateComponent(Dictionary<string,object> result)
    {
        if (result.ContainsKey("ItemID")) ItemIDInput.text = Convert.ToInt32(result["ItemID"]).ToString();
        if (result.ContainsKey("ItemType")) ItemTypeInput.text = Convert.ToInt32(result["ItemType"]).ToString();
        if (result.ContainsKey("ItemPrice")) ItemPriceInput.text = Convert.ToInt32(result["ItemPrice"]).ToString();
        if (result.ContainsKey("HpBase")) HPBaseInput.text = Convert.ToInt32(result["HpBase"]).ToString();
        if (result.ContainsKey("AtkBase")) ATKBaseInput.text = Convert.ToInt32(result["AtkBase"]).ToString();
        if (result.ContainsKey("DefBase")) DEFBaseInput.text = Convert.ToInt32(result["DefBase"]).ToString();
        if (result.ContainsKey("HpGrowth")) HPGrowthInput.text = Convert.ToDecimal(result["HpGrowth"]).ToString();
        if (result.ContainsKey("AtkGrowth")) ATKGrowthInput.text = Convert.ToDecimal(result["AtkGrowth"]).ToString();
        if (result.ContainsKey("DefGrowth")) DefGrowthInput.text = Convert.ToDecimal(result["DefGrowth"]).ToString();
        if (result.ContainsKey("objectId")) objectIDField.text = (string)result["objectId"];
        if (result.ContainsKey("ItemName")) ItemNameInput.text = (string)result["ItemName"];
        if (result.ContainsKey("Rarity")) rarityInput.text = Convert.ToDecimal(result["Rarity"]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.ITEMDATA_COLUMN_BUYABLE)) Buyable.text = Convert.ToInt32(result[DATABASE_CONSTANTS.ITEMDATA_COLUMN_BUYABLE]).ToString();
    }

    /*
     * Callback method to be pass down async methods. (Http Response)
     * */
    public void addResponseToQueue(string response)
    {
        updateUIQueue.Enqueue(response);
    }

    private void ItemIdOrTypeFieldError()
    {
        httpResponseField.text = "Please ensure that ItemID and ItemType are having value more than 0";
    }
        

    private void ObjectIdError()
    {
        httpResponseField.text = "ObjectID is Empty. Please Get Object from ItemID.";
    }

    public void AddItem()
    {
        //Dictionary<string,StatStruct<int,float>> StatDict= new Dictionary<string,StatStruct<int,float>>();
        //StatDict.Add(DATABASE_CONSTANTS.ITEMDATA_ITEMSTAT_HPNAME, [Int32.Parse(HPbase.text), float.Parse(HPgrowth.text)]);
        //StatDict.Add(DATABASE_CONSTANTS.ITEMDATA_ITEMSTAT_ATKNAME, new StatStruct<int, float>(Int32.Parse(ATKbase.text), float.Parse(ATKgrowth.text)));
        //StatDict.Add(DATABASE_CONSTANTS.ITEMDATA_ITEMSTAT_DEFNAME, new StatStruct<int, float>(Int32.Parse(DEFbase.text), float.Parse(DEFgrowth.text)));

        //Debug.Log("ItemID : " + Int32.Parse(ItemID.text) + "\t\tItem Name : " + ItemName.text + "\t\tItem Price : " + Int32.Parse(ItemPrice.text) + "\t\tItem Type : " + Int32.Parse(ItemType.text));
        //foreach(KeyValuePair<string,StatStruct<int,float>> aPair in StatDict)
        //{
        //    Debug.Log(aPair.Key + " " + aPair.Value.BaseStat() + " " + aPair.Value.BaseGrowth());
        //}
        int itemID = -1;
        string itemName = "UNDEFINED";
        int itemPrice = 0;
        int itemType = -1;
        int HPBase = 0;
        float HPGrowth = 0;
        int ATKBase = 0;
        float ATKGrowth = 0;
        int DEFBase = 0;
        float DEFGrowth = 0;
        int rarity = 0;
        int buyable = 0;

        if (ItemIDInput.text.Length > 0) itemID = Int32.Parse(ItemIDInput.text);
        if (ItemNameInput.text.Length > 0) itemName = ItemNameInput.text;
        if (ItemPriceInput.text.Length > 0) itemPrice = Int32.Parse(ItemPriceInput.text);
        if (ItemTypeInput.text.Length > 0) itemType = Int32.Parse(ItemTypeInput.text);
        if (HPBaseInput.text.Length > 0) HPBase = Int32.Parse(HPBaseInput.text);
        if (HPGrowthInput.text.Length > 0) HPGrowth = float.Parse(HPGrowthInput.text);
        if (ATKBaseInput.text.Length > 0) ATKBase = Int32.Parse(ATKBaseInput.text);
        if (ATKGrowthInput.text.Length > 0) ATKGrowth = float.Parse(ATKGrowthInput.text);
        if (DEFBaseInput.text.Length > 0) DEFBase = Int32.Parse(DEFBaseInput.text);
        if (DefGrowthInput.text.Length > 0) DEFGrowth = float.Parse(DefGrowthInput.text);
        if (rarityInput.text.Length > 0) rarity = Int32.Parse(rarityInput.text);
        if (Buyable.text.Length > 0) buyable = Int32.Parse(Buyable.text);

        if (itemID >= 0 && itemType >= 0)
        {
            ItemHandler.addItem(itemID, itemName, itemPrice, itemType, HPBase, HPGrowth, ATKBase, ATKGrowth, DEFBase, DEFGrowth,rarity,buyable);
            httpResponseField.text = "Adding Item...";
        }else
        {
            ItemIdOrTypeFieldError();
        }
    }

    public void GetItemFromID()
    {
        int itemID = -1;
        if (ItemIDInput.text.Length > 0) itemID = Int32.Parse(ItemIDInput.text);
        
        if(itemID >= 0)
        {
            ItemHandler.getItem(itemID);
            httpResponseField.text = "Getting Item...";
        }else
        {
            ItemIdOrTypeFieldError();
        }
    }

    public void DeleteItem()
    {
        if(objectIDField.text.Length > 0)
        {
            string objectID = objectIDField.text;
            ItemHandler.deleteItem(objectID);
        }
        else
        {
            ObjectIdError();
        }
    }

    public void UpdateItem()
    {
        if(objectIDField.text.Length > 0)
        {
            int itemID = -1;
            string itemName = "UNDEFINED";
            int itemPrice = 0;
            int itemType = -1;
            int HPBase = 0;
            float HPGrowth = 0;
            int ATKBase = 0;
            float ATKGrowth = 0;
            int DEFBase = 0;
            float DEFGrowth = 0;
            int rarity = 0;
            int buyable = 0;

            if (ItemIDInput.text.Length > 0) itemID = Int32.Parse(ItemIDInput.text);
            if (ItemNameInput.text.Length > 0) itemName = ItemNameInput.text;
            if (ItemPriceInput.text.Length > 0) itemPrice = Int32.Parse(ItemPriceInput.text);
            if (ItemTypeInput.text.Length > 0) itemType = Int32.Parse(ItemTypeInput.text);
            if (HPBaseInput.text.Length > 0) HPBase = Int32.Parse(HPBaseInput.text);
            if (HPGrowthInput.text.Length > 0) HPGrowth = float.Parse(HPGrowthInput.text);
            if (ATKBaseInput.text.Length > 0) ATKBase = Int32.Parse(ATKBaseInput.text);
            if (ATKGrowthInput.text.Length > 0) ATKGrowth = float.Parse(ATKGrowthInput.text);
            if (DEFBaseInput.text.Length > 0) DEFBase = Int32.Parse(DEFBaseInput.text);
            if (DefGrowthInput.text.Length > 0) DEFGrowth = float.Parse(DefGrowthInput.text);
            if (rarityInput.text.Length > 0) rarity = Int32.Parse(rarityInput.text);
            if (Buyable.text.Length > 0) buyable = Int32.Parse(Buyable.text);

            if (itemID >= 0 && itemType >= 0)
            {
                ItemHandler.UpdateItem(objectIDField.text,itemID, itemName, itemPrice, itemType, HPBase, HPGrowth, ATKBase, ATKGrowth, DEFBase, DEFGrowth,rarity,buyable);
                httpResponseField.text = "Updating Item...";
            }else
            {
                ItemIdOrTypeFieldError();
            }
            }else
            {
                ObjectIdError();
            }
        }


}
