﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.Net;
using System.Text;
using System.IO;
using UnityEngine.UI;
using MiniJSON;

public class MonsterEditorButtonHandler : MonoBehaviour
{
    public InputField MonsterIDInput,MonsterNameInput,EXPBaseInput,EXPGrowthInput,GoldBaseInput,GoldGrowthInput,HPBaseInput,HPGrowthInput,ATKBaseInput,ATKGrowthInput,DEFBaseInput,DEFGrowthInput,objectIDField,httpResponseField;
    Queue updateUIQueue = new Queue();

    // Use this for initialization
    void Start()
    {
        //Ignore MonoDev certificate Error. 
        //WHY U NO TRUST US 
        //Rong Hai Nak Mak T-T 
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        
        

        
            
    }

    // Update is called once per frame
    /*
     * Handle Incoming Queue and update UI according to data received.
     * */
    void Update()
    {
        String responseReceiver = MonsterHandler.getMessage();
        if(responseReceiver != null)
        {

            httpResponseField.text = responseReceiver;
            Dictionary<string,object> result = Json.Deserialize(responseReceiver) as Dictionary<string,object>;
            if (result.ContainsKey("results"))
            {
                List<object> resultList = result["results"] as List<object>;
                foreach (Dictionary<string, object> aResult in resultList)
                {
                    loopUpdateComponent(aResult);
                }
            }
            loopUpdateComponent(result);
            
        }

    }

    private void loopUpdateComponent(Dictionary<string,object> result)
    {
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERID))        MonsterIDInput.text     = Convert.ToInt32(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERID]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERNAME))      MonsterNameInput.text   = result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERNAME].ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPBASE))          EXPBaseInput.text       = Convert.ToInt32(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPBASE]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPGROWTH))        EXPGrowthInput.text     = Convert.ToDecimal(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPGROWTH]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDBASE))         GoldBaseInput.text      = Convert.ToInt32(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDBASE]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDGROWTH))       GoldGrowthInput.text    = Convert.ToDecimal(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDGROWTH]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPBASE))      HPBaseInput.text        = Convert.ToInt32(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPBASE]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPGROWTH))    HPGrowthInput.text      = Convert.ToDecimal(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPGROWTH]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKBASE))     ATKBaseInput.text       = Convert.ToInt32(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKBASE]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKGROWTH))   ATKGrowthInput.text     = Convert.ToDecimal(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKGROWTH]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFBASE))     DEFBaseInput.text       = Convert.ToInt32(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFBASE]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFGROWTH))   DEFGrowthInput.text     = Convert.ToDecimal(result[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFGROWTH]).ToString();
        if (result.ContainsKey(DATABASE_CONSTANTS.OBJECT_ID))                           objectIDField.text      = result[DATABASE_CONSTANTS.OBJECT_ID].ToString();
    }

    /*
     * Callback method to be pass down async methods. (Http Response)
     * */
    public void addResponseToQueue(string response)
    {
        updateUIQueue.Enqueue(response);
    }

    private void MonsterIdOrTypeFieldError()
    {
        httpResponseField.text = "Please ensure that MonsterID and MonsterType are having value more than 0";
    }
        

    private void ObjectIdError()
    {
        httpResponseField.text = "ObjectID is Empty. Please Get Object from MonsterID.";
    }

    public void AddMonster()
    {
        //Dictionary<string,StatStruct<int,float>> StatDict= new Dictionary<string,StatStruct<int,float>>();
        //StatDict.Add(DATABASE_CONSTANTS.MONSTERDATA_MONSTERSTAT_HPNAME, [Int32.Parse(HPbase.text), float.Parse(HPgrowth.text)]);
        //StatDict.Add(DATABASE_CONSTANTS.MONSTERDATA_MONSTERSTAT_ATKNAME, new StatStruct<int, float>(Int32.Parse(ATKbase.text), float.Parse(ATKgrowth.text)));
        //StatDict.Add(DATABASE_CONSTANTS.MONSTERDATA_MONSTERSTAT_DEFNAME, new StatStruct<int, float>(Int32.Parse(DEFbase.text), float.Parse(DEFgrowth.text)));

        //Debug.Log("MonsterID : " + Int32.Parse(MonsterID.text) + "\t\tMonster Name : " + MonsterName.text + "\t\tMonster Price : " + Int32.Parse(MonsterPrice.text) + "\t\tMonster Type : " + Int32.Parse(MonsterType.text));
        //foreach(KeyValuePair<string,StatStruct<int,float>> aPair in StatDict)
        //{
        //    Debug.Log(aPair.Key + " " + aPair.Value.BaseStat() + " " + aPair.Value.BaseGrowth());
        //}
        int monsterID = -1;
        string monsterName = "UNDEFINED";
        
        int HPBase = 0;
        float HPGrowth = 0;
        int ATKBase = 0;
        float ATKGrowth = 0;
        int DEFBase = 0;
        float DEFGrowth = 0;
        int EXPBase = 0;
        float EXPGrowth = 0;
        int GoldBase = 0;
        float GoldGrowth = 0;


        if (MonsterIDInput.text.Length > 0) monsterID = Int32.Parse(MonsterIDInput.text);
        if (MonsterNameInput.text.Length > 0) monsterName = MonsterNameInput.text;
        if (HPBaseInput.text.Length > 0) HPBase = Int32.Parse(HPBaseInput.text);
        if (HPGrowthInput.text.Length > 0) HPGrowth = float.Parse(HPGrowthInput.text);
        if (ATKBaseInput.text.Length > 0) ATKBase = Int32.Parse(ATKBaseInput.text);
        if (ATKGrowthInput.text.Length > 0) ATKGrowth = float.Parse(ATKGrowthInput.text);
        if (DEFBaseInput.text.Length > 0) DEFBase = Int32.Parse(DEFBaseInput.text);
        if (DEFGrowthInput.text.Length > 0) DEFGrowth = float.Parse(DEFGrowthInput.text);
        if (EXPBaseInput.text.Length > 0) EXPBase = Int32.Parse(EXPBaseInput.text);
        if (EXPGrowthInput.text.Length > 0) EXPGrowth = float.Parse(EXPGrowthInput.text);
        if (GoldBaseInput.text.Length > 0) GoldBase = Int32.Parse(GoldBaseInput.text);
        if (GoldGrowthInput.text.Length > 0) GoldGrowth = float.Parse(GoldGrowthInput.text);
        


        if (monsterID >= 0)
        {
            MonsterHandler.addMonster(monsterID, monsterName, HPBase, HPGrowth, ATKBase, ATKGrowth, DEFBase, DEFGrowth, EXPBase, EXPGrowth, GoldBase, GoldGrowth);
            httpResponseField.text = "Adding Monster...";
        }else
        {
            MonsterIdOrTypeFieldError();
        }
    }

    public void GetMonsterFromID()
    {
        int monsterID = -1;
        if (MonsterIDInput.text.Length > 0) monsterID = Int32.Parse(MonsterIDInput.text);
        
        if(monsterID >= 0)
        {
            MonsterHandler.getMonster(monsterID);
            httpResponseField.text = "Getting Monster...";
        }else
        {
            MonsterIdOrTypeFieldError();
        }
    }

    public void DeleteMonster()
    {
        if(objectIDField.text.Length > 0)
        {
            string objectID = objectIDField.text;
            MonsterHandler.deleteMonster(objectID);
        }
        else
        {
            ObjectIdError();
        }
    }

    public void UpdateMonster()
    {
        if(objectIDField.text.Length > 0)
        {
            int monsterID = -1;
            string monsterName = "UNDEFINED";

            int HPBase = 0;
            float HPGrowth = 0;
            int ATKBase = 0;
            float ATKGrowth = 0;
            int DEFBase = 0;
            float DEFGrowth = 0;
            int EXPBase = 0;
            float EXPGrowth = 0;
            int GoldBase = 0;
            float GoldGrowth = 0;


            if (MonsterIDInput.text.Length > 0) monsterID = Int32.Parse(MonsterIDInput.text);
            if (MonsterNameInput.text.Length > 0) monsterName = MonsterNameInput.text;
            if (HPBaseInput.text.Length > 0) HPBase = Int32.Parse(HPBaseInput.text);
            if (HPGrowthInput.text.Length > 0) HPGrowth = float.Parse(HPGrowthInput.text);
            if (ATKBaseInput.text.Length > 0) ATKBase = Int32.Parse(ATKBaseInput.text);
            if (ATKGrowthInput.text.Length > 0) ATKGrowth = float.Parse(ATKGrowthInput.text);
            if (DEFBaseInput.text.Length > 0) DEFBase = Int32.Parse(DEFBaseInput.text);
            if (DEFGrowthInput.text.Length > 0) DEFGrowth = float.Parse(DEFGrowthInput.text);
            if (EXPBaseInput.text.Length > 0) EXPBase = Int32.Parse(EXPBaseInput.text);
            if (EXPGrowthInput.text.Length > 0) EXPGrowth = float.Parse(EXPGrowthInput.text);
            if (GoldBaseInput.text.Length > 0) GoldBase = Int32.Parse(GoldBaseInput.text);
            if (GoldGrowthInput.text.Length > 0) GoldGrowth = float.Parse(GoldGrowthInput.text);

            if (monsterID >= 0)
            {
                MonsterHandler.UpdateMonster(objectIDField.text, monsterID, monsterName, HPBase, HPGrowth, ATKBase, ATKGrowth, DEFBase, DEFGrowth, EXPBase, EXPGrowth, GoldBase, GoldGrowth);
                httpResponseField.text = "Updating Monster...";
            }else
            {
                MonsterIdOrTypeFieldError();
            }
            }else
            {
                ObjectIdError();
            }
        }


}
