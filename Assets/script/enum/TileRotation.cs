﻿using UnityEngine;
using System.Collections;

public class TileRotation {
    public const int NORTH = 1;
    public const int EAST = 2;
    public const int SOUTH = 3;
    public const int WEST = 4;
}
