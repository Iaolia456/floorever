﻿using UnityEngine;
using System.Collections;

public class TileType {
    public const int AIR = 0;
    public const int WALL = 1;
    public const int DOOR = 2;
    public const int ROOM = 3;

    public const int PATH_VERTICES = 4;
    public const int PATH = 5;
    public const int PATH_WITH_DOOR = 6;

    public const int PROP_MARGIN = 7;
    public const int PROP = 8;

    public const int MONSTER = 9;
    public const int CHEST = 10;
}
