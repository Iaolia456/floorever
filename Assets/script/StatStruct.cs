﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class StatStruct<T1,T2>{
    private T1 baseStat;
    private T2 baseGrowth;

    public StatStruct(T1 baseStat, T2 baseGrowth)
    {
        this.baseStat = baseStat;
        this.baseGrowth = baseGrowth;
    }

    public T1 BaseStat()
    {
        return this.baseStat;
    }

    public T2 BaseGrowth()
    {
        return this.baseGrowth;
    }



}
