﻿using System.Collections;

public static class DATABASE_CONSTANTS{

    public static string    SESSION_TOKEN                       = "sessionToken";
    public static string    OBJECT_ID                           = "objectId";

    public static string    USER_COLUMN_USERNAME                = "username";
    public static string    USER_COLUMN_PASSWORD                = "password";
    public static string    USER_COLUMN_INVENTORY               = "Inventory";
    public static string    USER_COLUMN_CONSUMABLE              = "Consumable";
    public static string    USER_COLUMN_GOLD                    = "Gold";

    public static string    DEVICEID_CLASSNAME                  = "DeviceIDUser";
    public static string    DEVICEID_COLUMN_DEVICEID            = "DeviceID";
    public static string    DEVICEID_COLUMN_INVENTORY           = "Inventory";
    public static string    DEVICEID_COLUMN_CONSUMABLE          = "Consumable";
    public static string    DEVICEID_COLUMN_GOLD                = "Gold";

    public static string    ITEMDATA_CLASSNAME                  = "ItemData";
    public static string    ITEMDATA_COLUMN_ITEMID              = "ItemID";
    public static string    ITEMDATA_COLUMN_ITEMNAME            = "ItemName";
    public static string    ITEMDATA_COLUMN_PRICE               = "ItemPrice";
    public static string    ITEMDATA_COLUMN_ITEMTYPE            = "ItemType";
    public static int       ITEMDATA_ITEMTYPE_EMPTY             = 0;
    public static int       ITEMDATA_ITEMTYPE_EQUIPMENT         = 1;
    public static int       ITEMDATA_ITEMTYPE_EQUIPMENT_WEAPON  = 11;
    public static int       ITEMDATA_ITEMTYPE_EQUIPMENT_SHIELD  = 12;
    public static int       ITEMDATA_ITEMTYPE_EQUIPMENT_SHIRT   = 13;
    public static int       ITEMDATA_ITEMTYPE_EQUIPMENT_PANT    = 14;
    public static int       ITEMDATA_ITEMTYPE_CONSUMABLE        = 2;
    public static int       ITEMDATA_ITEMTYPE_RESOURCE          = 3;
    public static string    ITEMDATA_COLUMN_HPBASE              = "HpBase";
    public static string    ITEMDATA_COLUMN_HPGROWTH            = "HpGrowth";
    public static string    ITEMDATA_COLUMN_ATKBASE             = "AtkBase";
    public static string    ITEMDATA_COLUMN_ATKGROWTH           = "AtkGrowth";
    public static string    ITEMDATA_COLUMN_DEFBASE             = "DefBase";
    public static string    ITEMDATA_COLUMN_DEFGROWTH           = "DefGrowth";
    public static string    ITEMDATA_COLUMN_RARITY              = "Rarity";
    public static string    ITEMDATA_COLUMN_BUYABLE             = "BuyAble";
    public static string    ITEMDATA_COLUMN_MODELNAME           = "ModelName";
    
    public static string    DUNGEONDATA_CLASSNAME               = "DungeonData";
    public static string    DUNGEONDATA_COLUMN_DUNGEON_ID       = "DungeonID";
    public static string    DUNGEONDATA_COLUMN_DUNGEON_NAME     = "DungeonName";
    public static string    DUNGEONDATA_COLUMN_MONSTER_TABLE    = "MonsterTable";
    public static string    DUNGEONDATA_COLUMN_ITEM_TABLE       = "ItemTable";
    public static string    DUNGEONDATA_COLUMN_BASE_MULTIPLIER  = "BaseMultiplier";
    public static string    DUNGEONDATA_COLUMN_FLOOR_NUMBER     = "FloorNumber";

    public static string    MONSTERDATA_CLASSNAME               = "MonsterData";
    public static string    MONSTERDATA_COLUMN_MONSTERID        = "MonsterID";
    public static string    MONSTERDATA_COLUMN_MONSTERNAME      = "MonsterName";
    public static string    MONSTERDATA_COLUMN_GOLDBASE         = "GoldDropBase";
    public static string    MONSTERDATA_COLUMN_GOLDGROWTH       = "GoldDropGrowth";
    public static string    MONSTERDATA_COLUMN_EXPBASE          = "ExpBase";
    public static string    MONSTERDATA_COLUMN_EXPGROWTH        = "ExpGrowth";
    public static string    MONSTERDATA_COLUMN_HPBASE           = "HpBase";
    public static string    MONSTERDATA_COLUMN_HPGROWTH         = "HpGrowth";
    public static string    MONSTERDATA_COLUMN_ATKBASE          = "AtkBase";
    public static string    MONSTERDATA_COLUMN_ATKGROWTH        = "AtkGrowth";
    public static string    MONSTERDATA_COLUMN_DEFBASE          = "DefBase";
    public static string    MONSTERDATA_COLUMN_DEFGROWTH        = "DefGrowth";

    public static string    CRAFTDATA_CLASSNAME                 = "CraftData";
    public static string    CRAFTDATA_COLUMN_CRAFTID            = "CraftID";
    public static string    CRAFTDATA_COLUMN_ITEMID             = "ItemID";
    public static string    CRAFTDATA_COLUMN_RESOURCE_TABLE     = "ResourceTable";
    public static string    CRAFTDATA_COLUMN_GOLD_COST          = "GoldCost";

    public static int       UIBUTTON_TYPE_SHOP_WEAPON           = 1;
    public static int       UIBUTTON_TYPE_SHOP_SHIELD           = 2;
    public static int       UIBUTTON_TYPE_SHOP_SHIRT            = 3;
    public static int       UIBUTTON_TYPE_SHOP_PANT             = 4;
    public static int       UIBUTTON_TYPE_SHOP_CONSUMABLE       = 5;
    public static int       UIBUTTON_TYPE_INVENTORY_WEAPON      = 6;
    public static int       UIBUTTON_TYPE_INVENTORY_SHIELD      = 7;
    public static int       UIBUTTON_TYPE_INVENTORY_SHIRT       = 8;
    public static int       UIBUTTON_TYPE_INVENTORY_PANT        = 9;
    public static int       UIBUTTON_TYPE_INVENTORY_CONSUMABLE  = 10;
    
    public static int       WINDOWSTATE_BUY                     = 1;
    public static int       WINDOWSTATE_SELL                    = 2;
    public static int       WINDOWSTATE_CONSUMABLE_BUY          = 3;
    public static int       WINDOWSTATE_CONSUMABLE_SELL         = 4;

}
