﻿using UnityEngine;
using System.Collections;

public class Rotate_Object : MonoBehaviour {

    public float speed = 0;
    // 0 = forward, 1 = up
    public int direction = 0;

    void Update()
    {

        if(direction == 0)
            transform.Rotate(Vector3.forward, speed * Time.deltaTime);
        if (direction == 1)
            transform.Rotate(Vector3.up, speed * Time.deltaTime);
    }

    /* Set speed of rotation
     * @param speed : speed of rotation
     */
    public void SetSpeed(float speed)
    {
        this.speed = speed * 10;
    }
}
