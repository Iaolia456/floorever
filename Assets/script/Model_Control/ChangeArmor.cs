﻿using UnityEngine;
using System.Collections;

public class ChangeArmor : MonoBehaviour {

    string currentRightHand = "Weapons_Right";
    string currentLeftHand = "Weapons_Left";
    string currentHand = "N";
    string currentHead = "Girl";
    string currentBody = "N";
    string currentLeg = "N";
    string currentFoot = "N";

    // Change head model
    public void ChangeHeadTo()
    {
        if (currentHead.Equals("Head_Girl"))
        {
            HideObject("Head_Girl");
            currentHead = "Head_Boy";
        }
        else {
            ShowObject("Head_Girl");
            currentHead = "Head_Girl";
        }
    }

    /* Change body model
     * @param name : object's name
     */
    public void ChangeBodyTo(string name)
    {
        HideObject("Body_"+currentBody);
        HideObject("Hand_" + currentHand);
        ShowObject("Body_"+name);
        ShowObject("Hand_"+name);
        currentBody = name;
        currentHand = name;
    }

    /* Change leg model
     *  @param name : object's name
     */
    public void ChangeLegTo(string name)
    {
        HideObject("Leg_" + currentLeg);
        HideObject("Foot_" + currentFoot);
        ShowObject("Leg_" + name);
        ShowObject("Foot_" + name);
        currentLeg = name;
        currentFoot = name;
        /*
        HideObject(currentLeg);
        ShowObject(name);
        currentLeg = name;*/
    }

    /* Hide object on right hand
     *  @param name : object's name
     */
    public void HideRightHand(string name)
    {
        HideObject(name);
        currentRightHand = name;
    }

    /* Show object on right hand
     *  @param name : object's name
     */
    public void ShowRightHand(string name)
    {
        if (name.Equals(currentRightHand)) return;
        ShowObject(name);
        if (!currentRightHand.Equals("Weapons_Right"))
            HideRightHand(currentRightHand);
        currentRightHand = name;
    }

    /* Hide object on left hand
     *  @param name : object's name
     */
    public void HideLeftHand(string name)
    {
        HideObject(name);
        currentLeftHand = name;
    }

    /* Show object on left hand
     *  @param name : object's name
     */
    public void ShowLeftHand(string name)
    {
        if (name.Equals(currentLeftHand)) return;
        ShowObject(name);
        if (!currentLeftHand.Equals("Weapons_Left"))
            HideLeftHand(currentLeftHand);
        currentLeftHand = name;
    }

    /* Hide any object
     *  @param name : object's name
     */
    void HideObject(string name)
    {
        Renderer[] renderer = GameObject.Find(name).GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderer)
        {
            r.enabled = false;
        }
    }

    /* Show any object
     *  @param name : object's name
     */
    void ShowObject(string name)
    {
        Renderer[] renderer = GameObject.Find(name).GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderer)
        {
            r.enabled = true;
        }
    }
}
