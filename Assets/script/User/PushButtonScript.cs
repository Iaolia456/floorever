﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;
using System;
using System.Net;
using System.Text;
using System.IO;
using UnityEngine.UI;
using MiniJSON;

public class PushButtonScript : MonoBehaviour
{
    RESTfulHandler restfulHandler;
    public InputField usernameField, passwordField, sessionTokenField, objectIDField,httpResponseField,inventoryInput,consumableInput,ItemIDInput,ItemLevelInput,GoldInput;
    Queue updateUIQueue = new Queue();

    // Use this for initialization
    void Start()
    {
        //Ignore MonoDev certificate Error. 
        //WHY U NO TRUST US 
        //Rong Hai Nak Mak T-T 
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        
        //Pass callback Method to Script
        restfulHandler = new RESTfulHandler(addResponseToQueue);


      

        Dictionary<string, string> test = new Dictionary<string, string>();
        Dictionary<int, int> test2 = new Dictionary<int, int>();
        test2.Add(10, 5);
        test2.Add(20, 1);
        test.Add("Inventory", Json.Serialize(test2));
        Debug.Log(Json.Serialize(test));

    }

    // Update is called once per frame
    /*
     * Handle Incoming Queue and update UI according to data received.
     * */
    void Update()
    {
        if (updateUIQueue.Count > 0)
        {
            httpResponseField.text = (string)updateUIQueue.Peek();
            string responseString = (string)updateUIQueue.Peek();
            Dictionary<string,object> responseJson = Json.Deserialize(responseString) as Dictionary<string,object>;
            if (responseJson.ContainsKey(DATABASE_CONSTANTS.USER_COLUMN_USERNAME)) usernameField.text = (string)responseJson[DATABASE_CONSTANTS.USER_COLUMN_USERNAME];
            if (responseJson.ContainsKey(DATABASE_CONSTANTS.USER_COLUMN_PASSWORD)) passwordField.text = (string)responseJson[DATABASE_CONSTANTS.USER_COLUMN_PASSWORD];
            if (responseJson.ContainsKey(DATABASE_CONSTANTS.OBJECT_ID)) objectIDField.text = (string)responseJson[DATABASE_CONSTANTS.OBJECT_ID];
            if (responseJson.ContainsKey(DATABASE_CONSTANTS.SESSION_TOKEN)) sessionTokenField.text = (string)responseJson[DATABASE_CONSTANTS.SESSION_TOKEN];
            if (responseJson.ContainsKey(DATABASE_CONSTANTS.USER_COLUMN_CONSUMABLE)) consumableInput.text = Convert.ToString(responseJson[DATABASE_CONSTANTS.USER_COLUMN_CONSUMABLE]);
            if (responseJson.ContainsKey(DATABASE_CONSTANTS.USER_COLUMN_INVENTORY)) inventoryInput.text = Json.Serialize(Json.Deserialize((string)responseJson[DATABASE_CONSTANTS.USER_COLUMN_INVENTORY]) as List<object>);
            if (responseJson.ContainsKey(DATABASE_CONSTANTS.USER_COLUMN_GOLD)) GoldInput.text = Convert.ToInt32(responseJson[DATABASE_CONSTANTS.USER_COLUMN_GOLD]).ToString();
            updateUIQueue.Dequeue();
        }
    }
    
    /*
     * Callback method to be pass down async methods. (Http Response)
     * */
    public void addResponseToQueue(string response)
    {
        updateUIQueue.Enqueue(response);
    }



    public void RegisUserClicked()
    {
        Dictionary<string,object> tempJson = new Dictionary<string,object>();
        List<object> inventory = new List<object>();
        int gold = 0;
        int consumable = 0;
        if (inventoryInput.text.Length > 0) inventory = Json.Deserialize(inventoryInput.text) as List<object>;
        if (consumableInput.text.Length > 0) consumable = Convert.ToInt32(consumableInput.text);
        if (GoldInput.text.Length > 0) gold = Convert.ToInt32(GoldInput.text);
        tempJson.Add(DATABASE_CONSTANTS.USER_COLUMN_GOLD, gold);
        tempJson.Add(DATABASE_CONSTANTS.USER_COLUMN_INVENTORY, Json.Serialize(inventory));
        tempJson.Add(DATABASE_CONSTANTS.USER_COLUMN_CONSUMABLE, consumable);
        restfulHandler.WebReqRegisUserAsync(usernameField.text, passwordField.text, tempJson);

        httpResponseField.text = "Registering User...";
    }


     public void UpdateUserClicked()
    {
        Dictionary<string, object> tempJson = new Dictionary<string, object>();
        List<object> inventory = new List<object>();
        int gold = 0;
        int consumable = 0;
        Debug.Log(Json.Serialize(inventory));
        if (inventoryInput.text.Length > 0) inventory = Json.Deserialize(inventoryInput.text) as List<object>;
        if (consumableInput.text.Length > 0) consumable = Convert.ToInt32(consumableInput.text);
        if (GoldInput.text.Length > 0) gold = Convert.ToInt32(GoldInput.text);
        tempJson.Add(DATABASE_CONSTANTS.USER_COLUMN_GOLD, gold);
        Debug.Log(Json.Serialize(inventory));
        tempJson.Add(DATABASE_CONSTANTS.USER_COLUMN_INVENTORY, Json.Serialize(inventory));
        tempJson.Add(DATABASE_CONSTANTS.USER_COLUMN_CONSUMABLE, consumable);
        restfulHandler.WebReqUpdateUserAsync(objectIDField.text, sessionTokenField.text, tempJson);

         httpResponseField.text = "Updating User...";
    }

    public void LoginClicked()
    {

        restfulHandler.WebReqLoginUserAsync(usernameField.text, passwordField.text);
        httpResponseField.text = "Logging in...";
    }

    public void GetCurrentUserClicked()
    {
        restfulHandler.WebReqGetCurrentUserAsync(sessionTokenField.text);

        httpResponseField.text = "Getting Current User...";
    }

    public void testQueryClicked()
    {
        DropItems dropItems = new DropItems(addResponseToQueue);
        List<int> dummyItemList = new List<int>();
        dummyItemList.Add(1);
        dummyItemList.Add(3);
        dummyItemList.Add(4);
        dummyItemList.Add(5);
        dummyItemList.Add(6);
        dummyItemList.Add(7);
        dummyItemList.Add(9);
        dropItems.getDropStat(dummyItemList);
    }

    public void addItemtoInventory()
    {
        List<object> oldInventory = new List<object>();
        if(inventoryInput.text.Length > 0) oldInventory = Json.Deserialize(inventoryInput.text) as List<object>;
        oldInventory.Add(new KeyValuePair<int, int>(Convert.ToInt32(ItemIDInput.text), Convert.ToInt32(ItemLevelInput.text)));

        inventoryInput.text = Json.Serialize(oldInventory);

    }


}
