﻿using UnityEngine;
using System.Collections.Generic;

public class ImmortalObject : MonoBehaviour {

    public static string DeviceIDUserObjectID;

    public static Dictionary<string, Dictionary<string, object>> fullItemList;
    public static Dictionary<string, Dictionary<string, object>> fullMonsterList;

    public static List<KeyValuePair<int,int>> playerInventory;
    public static int playerGold;
    public static int playerConsumable;

    public static int[] playerEquipments = { 1,2,3,4 };
    public static int[] playerEquipmentsLevel = { 0,0,0,0 };
	
}
