﻿using UnityEngine;
using System.Collections.Generic;

public class RoomStruct {
    public int id;
    public int w;
    public int h;
    public int type;
    public IntVector3 pos;

    public RoomStruct(int id, IntVector3 pos, int w, int h)
    {
        this.id = id;
        this.pos = pos;
        this.w = w;
        this.h = h;
    }
}
