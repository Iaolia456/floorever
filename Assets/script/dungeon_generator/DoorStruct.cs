﻿using UnityEngine;
using System;
using System.Collections;

/* A data structure class used for door generation */
public class DoorStruct : EntitiesStructure {
    public bool isLock;
    public int belongToRoomID;

    public DoorStruct(int id, int belongToRoomID, IntVector3 pos, int rotation, bool isLock)
        : base(id, pos, rotation)
    {
        this.isLock = isLock;
        this.belongToRoomID = belongToRoomID;
    }
}
