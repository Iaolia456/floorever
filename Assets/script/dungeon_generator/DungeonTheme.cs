﻿using UnityEngine;
using MiniJSON;
using System;
using System.Net;
using System.Collections.Generic;

public class DungeonTheme : MonoBehaviour
{
    public List<GameObject> monsters;
    public List<GameObject> props;
    public List<GameObject> stairs;

    public List<KeyValuePair<int, int>> dropTable = new List<KeyValuePair<int, int>>();
    public List<string> spawnableMonsterID = new List<string>();
    public int baseItemLevel;
    public int noOfFloor;

    public string themeName;
    public int dungeonID;

    bool gotRespond = false;

    void Start()
    {
        //Ignore MonoDev certificate Error. 
        //WHY U NO TRUST US 
        //Rong Hai Nak Mak T-T 
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        
        FloorHandler.getFloor(dungeonID);
    }

    void Update()
    {
        string respond = FloorHandler.getMessage();
        if (!gotRespond && respond != null)
        {
            print(respond);
            Dictionary<string, object> result = Json.Deserialize(respond) as Dictionary<string, object>;

            if (result.ContainsKey("results"))
            {
                List<object> resultList = result["results"] as List<object>;
                foreach (Dictionary<string, object> aResult in resultList)
                {
                    loopUpdateComponent(aResult);
                }
            }
            else
                loopUpdateComponent(result);
        }
    }

    private void loopUpdateComponent(Dictionary<string, object> result)
    {
        print("dungeon theme");
        if (result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_BASE_MULTIPLIER)) baseItemLevel = Convert.ToInt32(result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_BASE_MULTIPLIER]);
        if (result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_ITEM_TABLE))
        {
            List<System.Object> respond = Json.Deserialize(result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_ITEM_TABLE].ToString()) as List<System.Object>;
            
            foreach (string anItem in respond)
            {
                string anItemWithoutSquareBracket = anItem.Substring(1, anItem.Length - 2);
                string[] anItemValue = anItemWithoutSquareBracket.Split(',');
                dropTable.Add(new KeyValuePair<int, int>(int.Parse(anItemValue[0]), int.Parse(anItemValue[1])));
            }
        }
        if (result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_FLOOR_NUMBER)) noOfFloor = Convert.ToInt32(result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_FLOOR_NUMBER]);
        if (result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_MONSTER_TABLE))
        {
            List<System.Object> respond = Json.Deserialize(result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_MONSTER_TABLE].ToString()) as List<System.Object>;

            foreach (string monsterId in respond)
            {
                monsters.Add(Resources.Load<GameObject>("Prefab/Dungeon/Monster/Mon_ID_" + monsterId));
                spawnableMonsterID.Add(monsterId);
            }
        }
        if (result.ContainsKey(DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_NAME)) themeName = result[DATABASE_CONSTANTS.DUNGEONDATA_COLUMN_DUNGEON_NAME].ToString();

        gotRespond = true;
        Camera.main.GetComponent<GameController>().StartGame();
    }
}