﻿using UnityEngine;
using System.Collections;

public class PropStruct : EntitiesStructure {
    public int type;
    public int w;
    public int h;

    public PropStruct(int id, IntVector3 pos, int rotation, int type, int w, int h)
        : base(id, pos, rotation)
    {
        this.type = type;
        this.w = w;
        this.h = h;
    }
}
