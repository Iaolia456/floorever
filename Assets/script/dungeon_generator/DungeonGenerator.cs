﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class DungeonGenerator : MonoBehaviour {
    private const int VERTEX_COUNT = 16; // <= GRID_COUNT_X * GRID_COUNT_Y
    private const int MAP_WIDTH = 60; // divisible by GRID_COUNT_X
    private const int MAP_HEIGHT = 60; // divisible by GRID_COUNT_Y
    private const int GRID_COUNT_X = 4;
    private const int GRID_COUNT_Y = 4;

    private const int MAX_ROOM_W = 14;
    private const int MAX_ROOM_H = 14;
    private const int MIN_ROOM_W = 7;
    private const int MIN_ROOM_H = 7;
    private const int PADDING_X = MAX_ROOM_W;
    private const int PADDING_Y = MAX_ROOM_H;
    private const int PATH_ROOM_THRESHOLD = 10; // should >= MAX_ROOM_W, room will generate at straight path longer than this number
    private const int MAX_STRAIGHT_PATH_LENGTH = 20; // > PATH_ROOM_THRESHOLD

    private const float MIN_PROP_OCCUPIED = 0.1f; // percentage of occupied tile by prop per room
    private const float MAX_PROP_OCCUPIED = 0.15f; // > MIN_PROP_OCCUPIED

    private int[] MONSTER_PER_ROOM_WEIGHT = { 0, 1, 1, 2, 2, 2, 3 };
    private int[] CHEST_PER_ROOM_WEIGHT = { 0, 0, 1, 1, 1, 1, 2 };

    DungeonTheme theme;
    List<Vertex> vertexPos = new List<Vertex>();
    List<RoomStruct> rooms = new List<RoomStruct>();
    Dictionary<string, List<EntitiesStructure>> entities = new Dictionary<string, List<EntitiesStructure>>();
    bool[,] graph = new bool[VERTEX_COUNT, VERTEX_COUNT];
    int[,] roomTile = new int[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];
    int[,] pathTile = new int[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];
    int[,] propAndEntityTile = new int[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];
    bool[,] walkable = new bool[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];

    //debug
    public GameObject vertexMarker;
    public GameObject path;
    public GameObject pathWall;
    public GameObject pathWithDoor;
    public GameObject roomFloor;
    public GameObject roomWall;
    public GameObject door;
    public GameObject prop;
    public GameObject propMargin;
    public GameObject walkableMarker;

	void Start () {
        
    }

    public Level GenerateDungeon(DungeonTheme theme)
    {
        this.theme = theme;
        ResetGenerator();
        PathGeneration();
        RoomGeneration();
        PropGeneration();
        RemoveFloatingDoor();

        MonsterGeneration();
        IntVector3 spawnPoint = SetSpawnPoint();
        IntVector3 toNextFloorPoint = SetToNextFloorPoint();

        DisplayFloor();
        //PrintFloor();

        Level level = new Level(rooms, roomTile, pathTile, propAndEntityTile, walkable, entities, spawnPoint, toNextFloorPoint);
        return level;
    }

    #region main generation method
    private void PathGeneration()
    {
        List<int> occupiedGrid = new List<int>();
        for (int i = 0; i < VERTEX_COUNT; i++)
        {
            int takingGrid = 0;
            do
            {
                takingGrid = 0;
                takingGrid += Random.Range(0, GRID_COUNT_X) * 10;
                takingGrid += Random.Range(0, GRID_COUNT_Y);
            } while (occupiedGrid.Contains(takingGrid));
            occupiedGrid.Add(takingGrid);

            int GRID_SIZE_X = MAP_WIDTH / GRID_COUNT_X;
            int GRID_SIZE_Y = MAP_HEIGHT / GRID_COUNT_Y;
            int startGridX = PADDING_X + ((takingGrid / 10) * GRID_SIZE_X);
            int startGridY = PADDING_Y + ((takingGrid % 10) * GRID_SIZE_Y);

            int x = Random.Range(startGridX, startGridX + GRID_SIZE_X);
            int z = Random.Range(startGridY, startGridY + GRID_SIZE_Y);

            Vertex newVertex = new Vertex(i, new IntVector3(x, 0, z));
            vertexPos.Add(newVertex);
            pathTile[x, z] = TileType.PATH_VERTICES;
        }

        //connect the vertexes
        foreach (Vertex vertex in vertexPos)
        {
            Vertex closestVertex = FindClosestVertex(vertex);
            ConnectVertex(vertex, closestVertex);
        }

        while (FixDisconnectedGraph()) ;
    }

    List<EntitiesStructure> doors = new List<EntitiesStructure>();
    private void RoomGeneration()
    {
        //stage 1 : generate room at vertices
        foreach (Vertex v in vertexPos)
        {
            bool success = false;
            int triedCount = 0;
            while (!success && triedCount < 5)
            {
                if (GenerateRoom(v.position))
                    success = true;
                else
                    triedCount++;
            } 
        }

        //stage 2.1 : generate room at long straight path (horizontal)
        for (int i = 0; i < MAP_HEIGHT; i++)
        {
            int pathLength = 0;
            int pathStartX = -1;
            bool isOnPath = false;
            for (int j = 0; j < MAP_WIDTH; j++)
            {
                if (pathTile[PADDING_Y + i, PADDING_X + j] == TileType.PATH && pathLength < MAX_STRAIGHT_PATH_LENGTH)
                {
                    if (pathStartX == -1)
                        pathStartX = j;
                    isOnPath = true;
                    pathLength++;
                }
                else if (isOnPath)
                {
                    isOnPath = false;
                    if (pathLength > PATH_ROOM_THRESHOLD)
                    {
                        IntVector3 pos = new IntVector3(PADDING_Y + i, 0, PADDING_X + Random.Range(pathStartX, j));
                        GenerateRoom(pos);
                    }
                    pathLength = 0;
                    pathStartX = -1;
                }
            }
        }

        //stage 2.2 : generate room at long straight path (vertical)
        for (int i = 0; i < MAP_WIDTH; i++)
        {
            int pathLength = 0;
            int pathStartY = -1;
            bool isOnPath = false;
            for (int j = 0; j < MAP_HEIGHT; j++)
            {
                if (pathTile[PADDING_X + j, PADDING_Y + i] == TileType.PATH && pathLength < MAX_STRAIGHT_PATH_LENGTH)
                {
                    if (pathStartY == -1)
                        pathStartY = j;
                    isOnPath = true;
                    pathLength++;
                }
                else if (isOnPath)
                {
                    isOnPath = false;
                    if (pathLength > PATH_ROOM_THRESHOLD)
                    {
                        IntVector3 pos = new IntVector3(PADDING_X + Random.Range(pathStartY, j), 0, PADDING_Y + i);
                        GenerateRoom(pos);
                    }
                    pathLength = 0;
                    pathStartY = -1;
                }  
            }
        }
    }

    private void PropGeneration()
    {
        List<EntitiesStructure> props = new List<EntitiesStructure>();
        foreach (RoomStruct r in rooms)
        {
            int roomSize = r.h * r.w;
            int occupied = 0;
            int maxOccupied = (int)(Random.Range(roomSize * MIN_PROP_OCCUPIED, roomSize * MAX_PROP_OCCUPIED));
            int tried = 0;
            int generatedChestCount = 0;
            int chestCount = CHEST_PER_ROOM_WEIGHT[Random.Range(0, CHEST_PER_ROOM_WEIGHT.Length)];

            while (occupied < maxOccupied || generatedChestCount < chestCount)
            {
                tried++;
                if (tried >= 30)
                    break;

                IntVector3 propPos = new IntVector3(Random.Range(r.pos.x, r.pos.x + r.w), 0, Random.Range(r.pos.z, r.pos.z + r.h));
                
                int type = 0; //chest
                if (generatedChestCount >= chestCount) //if finish generate chest
                    type = Random.Range(1, theme.props.Count);

                Prop propScript = theme.props[type].GetComponent<Prop>();
                IntVector3 size = new IntVector3(propScript.w, 1, propScript.h);

                if (CheckPropOverlap(propPos, size))
                    continue;

                GenerateProp(propPos, size);
                if (type == 0)
                    generatedChestCount++;

                props.Add(new PropStruct(props.Count, propPos, 1, type, size.x, size.z));
                occupied += size.x * size.z;
            }
        }

        entities.Add("prop", props);
    }

    private void MonsterGeneration()
    {
        List<EntitiesStructure> monsters = new List<EntitiesStructure>();
        foreach (RoomStruct r in rooms)
        {
            int monsCount = MONSTER_PER_ROOM_WEIGHT[Random.Range(0, MONSTER_PER_ROOM_WEIGHT.Length)];
            for (int i = 0; i < monsCount; i++)
            {
                int type = Random.Range(1, theme.monsters.Count);
                TrySpawnMonster(monsters, r, type);
            }
        }

        RoomStruct bossRoom = rooms[Random.Range(0, rooms.Count)];
        TrySpawnMonster(monsters, bossRoom, 0);

        entities.Add("monster", monsters);
    }

    private IntVector3 SetSpawnPoint()
    {
        int bossRoomID = ((MonsterStruct)(entities["monster"][entities["monster"].Count - 1])).belongToRoomID;
        int spawnPointRoomID = bossRoomID;
        while (spawnPointRoomID == bossRoomID)
            spawnPointRoomID = Random.Range(0, rooms.Count - 1);

        RoomStruct r = rooms[spawnPointRoomID];
        IntVector3 spawnPos = null;
        bool success = false;
        while (!success)
        {
            spawnPos = new IntVector3(Random.Range(r.pos.x, r.pos.x + r.w), 0, Random.Range(r.pos.z, r.pos.z + r.h));
            if (propAndEntityTile[spawnPos.z, spawnPos.x] == TileType.MONSTER || propAndEntityTile[spawnPos.z, spawnPos.x] == TileType.PROP)
                continue;
            else
                success = true;
        }

        walkable[spawnPos.z, spawnPos.x] = false;
        return spawnPos;
    }

    private IntVector3 SetToNextFloorPoint()
    {
        return entities["monster"][entities["monster"].Count - 1].pos;
    }
    #endregion

    #region helper method
    private void TrySpawnMonster(List<EntitiesStructure> monsters, RoomStruct r, int type)
    {
        int tried = 0;
        bool success = false;
        while (!success && tried <= 10)
        {
            tried++;
            int posX = Random.Range(r.pos.x, r.pos.x + r.w);
            int posY = Random.Range(r.pos.z, r.pos.z + r.h);
            int rotation = Random.Range(1, 4);
            
            if (propAndEntityTile[posY, posX] != TileType.MONSTER && propAndEntityTile[posY, posX] != TileType.PROP)
            {
                propAndEntityTile[posY, posX] = TileType.MONSTER;
                monsters.Add(new MonsterStruct(monsters.Count, r.id, new IntVector3(posX, 0, posY), rotation, type, theme.spawnableMonsterID[type]));
                success = true;
                walkable[posY, posX] = false;
            }
        }
    }

    private void RemoveFloatingDoor()
    {
        List<EntitiesStructure> removeDoor = new List<EntitiesStructure>();
        foreach (DoorStruct d in doors)
        {
            bool remove = false;
            if (d.rotation == TileRotation.EAST && (roomTile[d.pos.z - 1, d.pos.x] == TileType.ROOM || pathTile[d.pos.z - 1, d.pos.x] == TileType.PATH_WITH_DOOR))
                remove = true;
            else if (d.rotation == TileRotation.SOUTH && (roomTile[d.pos.z, d.pos.x - 1] == TileType.ROOM || pathTile[d.pos.z, d.pos.x - 1] == TileType.PATH_WITH_DOOR))
                remove = true;
            else if (d.rotation == TileRotation.WEST && (roomTile[d.pos.z + 1, d.pos.x] == TileType.ROOM || pathTile[d.pos.z + 1, d.pos.x] == TileType.PATH_WITH_DOOR))
                remove = true;
            else if (d.rotation == TileRotation.NORTH && (roomTile[d.pos.z, d.pos.x + 1] == TileType.ROOM || pathTile[d.pos.z, d.pos.x + 1] == TileType.PATH_WITH_DOOR))
                remove = true;

            if (remove)
            {
                removeDoor.Add(d);
                roomTile[d.pos.z, d.pos.x] = TileType.ROOM;
                pathTile[d.pos.z, d.pos.x] = TileType.PATH;
                walkable[d.pos.z, d.pos.x] = true;
            }
        }

        foreach (DoorStruct d in removeDoor)
        {
            doors.Remove(d);
        }

        entities.Add("door", doors);
    }

    private bool GenerateRoom(IntVector3 pos)
    {
        int w = Random.Range(MIN_ROOM_W, MAX_ROOM_W);
        int h = Random.Range(MIN_ROOM_H, MAX_ROOM_H);
        int y = pos.x - (h / 2);
        int x = pos.z - (w / 2);

        if (CheckRoomOverlap(w, h, x, y))
            return false;

        int roomID = rooms.Count;
        rooms.Add(new RoomStruct(roomID, new IntVector3(x + 1, 0, y + 1), w - 2, h - 2));

        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            { 
                walkable[y + i, x + j] = true;
                if (i == 0 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i - 1, x + j] == TileType.PATH) {
                    roomTile[y + i, x + j] = TileType.DOOR;
                    propAndEntityTile[y + i + 1, x + j] = TileType.PROP_MARGIN;
                    propAndEntityTile[y + i - 1, x + j] = TileType.PROP_MARGIN;
                    doors.Add(new DoorStruct(doors.Count, roomID, new IntVector3(x + j, 0, y + i), TileRotation.EAST, false));
                }
                else if (j == 0 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i, x + j - 1] == TileType.PATH) {
                    roomTile[y + i, x + j] = TileType.DOOR;
                    propAndEntityTile[y + i, x + j + 1] = TileType.PROP_MARGIN;
                    propAndEntityTile[y + i, x + j - 1] = TileType.PROP_MARGIN;
                    doors.Add(new DoorStruct(doors.Count, roomID, new IntVector3(x + j, 0, y + i), TileRotation.SOUTH, false));
                } 
                else if (i == h - 1 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i + 1, x + j] == TileType.PATH) {
                    roomTile[y + i, x + j] = TileType.DOOR;
                    propAndEntityTile[y + i - 1, x + j] = TileType.PROP_MARGIN;
                    propAndEntityTile[y + i + 1, x + j] = TileType.PROP_MARGIN;
                    doors.Add(new DoorStruct(doors.Count, roomID, new IntVector3(x + j, 0, y + i), TileRotation.WEST, false));
                } 
                else if (j == w - 1 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i, x + j + 1] == TileType.PATH) {
                    roomTile[y + i, x + j] = TileType.DOOR;
                    propAndEntityTile[y + i, x + j - 1] = TileType.PROP_MARGIN;
                    propAndEntityTile[y + i, x + j + 1] = TileType.PROP_MARGIN;
                    doors.Add(new DoorStruct(doors.Count, roomID, new IntVector3(x + j, 0, y + i), TileRotation.NORTH, false));
                }
                else if (i == 0 || j == 0 || i == h - 1 || j == w - 1)
                {
                    roomTile[y + i, x + j] = TileType.WALL;
                    walkable[y + i, x + j] = false;
                }
                else
                    roomTile[y + i, x + j] = TileType.ROOM;

                if (roomTile[y + i, x + j] == TileType.DOOR)
                    pathTile[y + i, x + j] = TileType.PATH_WITH_DOOR;
            }
        }

        return true;
    }

    private void GenerateProp(IntVector3 propPos, IntVector3 size)
    {
        for (int i = -1; i < size.x + 1; i++)
        {
            for (int j = -1; j < size.z + 1; j++)
            {
                if (i != -1 && i != size.x && j != -1 && j != size.z)
                {
                    propAndEntityTile[propPos.z + i, propPos.x + j] = TileType.PROP;
                    walkable[propPos.z + i, propPos.x + j] = false;
                }
                else if (roomTile[propPos.z + i, propPos.x + j] == TileType.ROOM)
                    propAndEntityTile[propPos.z + i, propPos.x + j] = TileType.PROP_MARGIN;
            }
        }
    }

    private bool CheckPropOverlap(IntVector3 propPos, IntVector3 size)
    {
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.z; j++)
            {
                if (propAndEntityTile[propPos.z + i, propPos.x + j] != TileType.AIR || roomTile[propPos.z + i, propPos.x + j] != TileType.ROOM)
                    return true;
            }
        }

        return false;
    }

    private bool CheckRoomOverlap(int w, int h, int x, int y)
    {
        for (int i = 0; i < h; i++)
        {
            for (int j = 0; j < w; j++)
            {
                //border
                if (i == 0 || j == 0 || i == h - 1 || j == w - 1)
                {
                    if (i == 0 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i - 1, x + j] != TileType.PATH)
                        return true;
                    if (j == 0 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i, x + j - 1] != TileType.PATH)
                        return true;
                    if (i == h - 1 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i + 1, x + j] != TileType.PATH)
                        return true;
                    if (j == w - 1 && pathTile[y + i, x + j] == TileType.PATH && pathTile[y + i, x + j + 1] != TileType.PATH)
                        return true;
                }
                //inside
                else if (roomTile[y + i, x + j] == TileType.ROOM || roomTile[y + i, x + j] == TileType.WALL)
                    return true;
            }
        }

        return false;
    }

    bool[] visited = new bool[VERTEX_COUNT];
    private bool FixDisconnectedGraph()
    {
        visited = new bool[VERTEX_COUNT];
        DepthFirstSearch(vertexPos[0]);

        int firstDisconnectVertex = -1;
        List<Vertex> visitedVertex = new List<Vertex>();
        for (int i = 0; i < VERTEX_COUNT;i++)
        {
            if (!visited[i] && firstDisconnectVertex == -1)
                firstDisconnectVertex = i;
            else if (visited[i])
                visitedVertex.Add(vertexPos[i]);
        }

        if (firstDisconnectVertex > -1)
        {
            ConnectVertex(vertexPos[Random.Range(0, visitedVertex.Count)], vertexPos[firstDisconnectVertex]);
            return true;
        }

        return false;
    }

    private void DepthFirstSearch(Vertex startingVertex)
    {
        visited[startingVertex.id] = true;
        List<Vertex> connectedVertex = new List<Vertex>();
        for (int i = 0; i < VERTEX_COUNT; i++)
        {
            if (startingVertex.id == i) continue;
            if (graph[startingVertex.id, i])
                connectedVertex.Add(vertexPos[i]);
        }

        foreach (Vertex v in connectedVertex)
        {
            if (!visited[v.id])
                DepthFirstSearch(v);
        }
    }

    private Vertex FindClosestVertex(Vertex vertex)
    {
        Vertex closestVertex = new Vertex(-1, new IntVector3(-1, -1, -1));
        float closestDistance = 999999;
        foreach (Vertex candidateVertex in vertexPos)
        {
            if (candidateVertex == vertex) continue;
            float distance = IntVector3.Distance(vertex.position, candidateVertex.position);
            if (distance < closestDistance)
            {
                closestVertex = candidateVertex;
                closestDistance = distance;
            } 
        }
        return closestVertex;
    }

    private void ConnectVertex(Vertex a, Vertex b)
    {
        graph[a.id, b.id] = true;
        graph[b.id, a.id] = true;
        IntVector3 aPos = a.position;
        IntVector3 bPos = b.position;
        int xStep = aPos.x < bPos.x ? xStep = 1 : xStep = -1;
        int zStep = aPos.z < bPos.z ? zStep = 1 : zStep = -1;

        for (int x = aPos.x; x != bPos.x + xStep; x += xStep)
        {
            walkable[x, aPos.z] = true;
            if (pathTile[x, aPos.z] != TileType.PATH_VERTICES)
                pathTile[x, aPos.z] = TileType.PATH;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (pathTile[x + i, aPos.z + j] == TileType.AIR)
                        pathTile[x + i, aPos.z + j] = TileType.WALL;
                }
            }
        }

        for (int z = aPos.z; z != bPos.z + zStep; z += zStep)
        {
            walkable[bPos.x, z] = true;
            if (pathTile[bPos.x, z] != TileType.PATH_VERTICES)
                pathTile[bPos.x, z] = TileType.PATH;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if (pathTile[bPos.x + i, z + j] == TileType.AIR)
                        pathTile[bPos.x + i, z + j] = TileType.WALL;
                }
            }
        }
    }
    #endregion

    public void ResetGenerator()
    {
        vertexPos = new List<Vertex>();
        rooms = new List<RoomStruct>();
        entities = new Dictionary<string, List<EntitiesStructure>>();
        doors = new List<EntitiesStructure>();
        graph = new bool[VERTEX_COUNT, VERTEX_COUNT];
        pathTile = new int[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];
        roomTile = new int[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];
        propAndEntityTile = new int[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];
        walkable = new bool[MAP_WIDTH + (PADDING_X * 2), MAP_HEIGHT + (PADDING_Y * 2)];
    }

    //debug
    public void DisplayFloor()
    {
        GameObject debug = new GameObject("DEBUG");
        GameObject pathDebug = new GameObject("DEBUG_PathTile");
        for (int i = 0; i < pathTile.GetLength(0); i++)
        {
            for (int j = 0; j < pathTile.GetLength(1); j++)
            {
                if (pathTile[i, j] == TileType.PATH_VERTICES)
                    (Instantiate(vertexMarker, new Vector3(j, 2, i), Quaternion.identity) as GameObject).transform.parent = pathDebug.transform;
                if (pathTile[i, j] == TileType.PATH)
                    (Instantiate(path, new Vector3(j, 2, i), Quaternion.identity) as GameObject).transform.parent = pathDebug.transform;
                if (pathTile[i, j] == TileType.WALL)
                    (Instantiate(pathWall, new Vector3(j, 2, i), Quaternion.identity) as GameObject).transform.parent = pathDebug.transform;
                if (pathTile[i, j] == TileType.PATH_WITH_DOOR)
                    (Instantiate(pathWithDoor, new Vector3(j, 2, i), Quaternion.identity) as GameObject).transform.parent = pathDebug.transform;
            }
        }
        pathDebug.SetActive(false);

        GameObject roomDebug = new GameObject("DEBUG_RoomTile");
        for (int i = 0; i < roomTile.GetLength(0); i++)
        {
            for (int j = 0; j < roomTile.GetLength(1); j++)
            {
                if (roomTile[i, j] == TileType.ROOM)
                    (Instantiate(roomFloor, new Vector3(j, 1, i), Quaternion.identity) as GameObject).transform.parent = roomDebug.transform;
                if (roomTile[i, j] == TileType.DOOR)
                    (Instantiate(door, new Vector3(j, 1, i), Quaternion.identity) as GameObject).transform.parent = roomDebug.transform;
                if (roomTile[i, j] == TileType.WALL)
                    (Instantiate(roomWall, new Vector3(j, 1, i), Quaternion.identity) as GameObject).transform.parent = roomDebug.transform;
            }
        }
        roomDebug.SetActive(false);

        GameObject propDebug = new GameObject("DEBUG_PropTile");
        for (int i = 0; i < propAndEntityTile.GetLength(0); i++)
        {
            for (int j = 0; j < propAndEntityTile.GetLength(1); j++)
            {
                if (propAndEntityTile[i, j] == TileType.PROP)
                    (Instantiate(prop, new Vector3(j, 3, i), Quaternion.identity) as GameObject).transform.parent = propDebug.transform;
                if (propAndEntityTile[i, j] == TileType.PROP_MARGIN)
                    (Instantiate(propMargin, new Vector3(j, 3, i), Quaternion.identity) as GameObject).transform.parent = propDebug.transform;
            }
        }
        propDebug.SetActive(false);

        GameObject walkDebug = new GameObject("DEBUG_WalkableTile");
        for (int i = 0; i < propAndEntityTile.GetLength(0); i++)
        {
            for (int j = 0; j < propAndEntityTile.GetLength(1); j++)
            {
                if (walkable[i, j])
                    (Instantiate(walkableMarker, new Vector3(j, 4, i), Quaternion.identity) as GameObject).transform.parent = walkDebug.transform;
            }
        }
        walkDebug.SetActive(false);

        pathDebug.transform.parent = debug.transform;
        roomDebug.transform.parent = debug.transform;
        propDebug.transform.parent = debug.transform;
        walkDebug.transform.parent = debug.transform;
        //debug.SetActive(false);
    }

    public void PrintFloor()
    {
        for (int i = 0; i < pathTile.GetLength(0); i++)
        {
            string s = "";
            for (int j = 0; j < pathTile.GetLength(1); j++)
            {
                s += pathTile[i, j] + " ";
            }
            print(s);
        }
    }
}
