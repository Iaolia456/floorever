﻿using UnityEngine;
using System.Collections.Generic;

public class Level {
    public int floorNo;
    public List<RoomStruct> rooms;
    public int[,] roomTile;
    public int[,] pathTile;
    public int[,] propTile;
    public bool[,] walkable;
    public Dictionary<string, List<EntitiesStructure>> entities;
    public IntVector3 spawnPoint; //stair to prev floor (stair up)
    public IntVector3 stairDownPoint; //stair to next floor (stair down)
    public List<GameObject>[,] referenceMap;
    public byte[,] fog = null;

    public Level(List<RoomStruct> rooms, int[,] roomTile, int[,] pathTile, int[,] propTile, bool[,] walkable, Dictionary<string, List<EntitiesStructure>> entities, IntVector3 spawnPoint, IntVector3 toNextFloorPoint)
    {
        this.rooms = rooms;
        this.roomTile = roomTile;
        this.pathTile = pathTile;
        this.propTile = propTile;
        this.walkable = walkable;
        this.entities = entities;
        this.spawnPoint = spawnPoint;
        this.stairDownPoint = toNextFloorPoint;
        referenceMap = new List<GameObject>[roomTile.GetLength(0), roomTile.GetLength(1)];
    }

    public void ResetRefMap()
    {
        for (int i = 0; i < referenceMap.GetLength(0); i++)
        {
            for (int j = 0; j < referenceMap.GetLength(1); j++)
            {
                referenceMap[i, j] = new List<GameObject>();
            }
        }
    }
}
