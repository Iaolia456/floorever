﻿using UnityEngine;
using System.Collections;

public class MonsterStruct : EntitiesStructure {
    public int belongToRoomID;
    public int monsterType;
    public string monsterID;

    public MonsterStruct(int id, int belongToRoomID, IntVector3 pos, int rotation, int type, string monsterID)
        : base(id, pos, rotation)
    {
        this.monsterID = monsterID;
        this.belongToRoomID = belongToRoomID;
        this.monsterType = type;
    }
}
