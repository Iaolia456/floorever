﻿using UnityEngine;
using System.Collections;

public class EntitiesStructure {
    public int id;
    public IntVector3 pos;
    public int rotation;

    public EntitiesStructure()
    {

    }

    public EntitiesStructure(int id, IntVector3 pos, int rotation)
    {
        this.id = id;
        this.pos = pos;
        this.rotation = rotation;
    }
}
