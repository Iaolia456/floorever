﻿using UnityEngine;
using System.Collections;

public class Vertex {
    public int id { get; set; }
    public IntVector3 position { get; set; }

    public Vertex(int id, IntVector3 position)
    {
        this.id = id;
        this.position = position;
    }
}
