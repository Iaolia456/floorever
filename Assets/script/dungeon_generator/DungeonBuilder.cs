﻿using UnityEngine;
using System.Collections.Generic;

public class DungeonBuilder : MonoBehaviour {
    private const float DUNGEON_SCALE = 1.0f;
    public GameObject playerModel;

    GameObject dungeonParent;
    GameObject monsterParent;
    int buildingFloor;

	void Start () {
	
	}

    public void Build(int floorNo, Level level, DungeonTheme theme) {
        buildingFloor = floorNo;
        Destroy(dungeonParent);
        Destroy(monsterParent);
        level.ResetRefMap();
        dungeonParent = new GameObject("Dungeon");
        dungeonParent.transform.position = Vector3.zero;
        monsterParent = new GameObject("Monster");
        monsterParent.transform.position = Vector3.zero;

        RenderPathAndWall(level, theme);
        RenderDoors(level, theme);
        RenderProp(level, theme);
        RenderMonster(level, theme);
        SpawnPlayer(level);

        dungeonParent.transform.localScale = new Vector3(DUNGEON_SCALE, DUNGEON_SCALE, DUNGEON_SCALE);

        GameObject.Find("Pathfinder").GetComponent<Pathfinder>().StartPathFindingWithCustomMap(level.walkable);
        Camera.main.GetComponent<GameController>().DungeonFinishBuild();
    }

    private void RenderPathAndWall(Level level, DungeonTheme theme)
    {
        for (int i = 1; i < level.pathTile.GetLength(0); i++)
        {
            for (int j = 1; j < level.pathTile.GetLength(1); j++)
            {
                if (i == level.pathTile.GetLength(0) - 1) continue;
                if (j == level.pathTile.GetLength(1) - 1) continue;

                //for path
                if ((level.roomTile[i, j] != TileType.ROOM && level.pathTile[i, j] != TileType.WALL && level.pathTile[i, j] != TileType.AIR))
                {
                    string walls = "";
                    if (level.pathTile[i + 1, j] == TileType.WALL)
                        walls += "R";
                    if (level.pathTile[i - 1, j] == TileType.WALL)
                        walls += "L";
                    if (level.pathTile[i, j - 1] == TileType.WALL)
                        walls += "U";
                    if (level.pathTile[i, j + 1] == TileType.WALL)
                        walls += "D";

                    GameObject newPathCell = Instantiate(Resources.Load("Prefab/Dungeon/" + theme.themeName + "/Cell/Path_Cell_" + walls), new Vector3(j, 0, i), Quaternion.identity) as GameObject;
                    newPathCell.transform.parent = dungeonParent.transform;
                    level.referenceMap[i, j].Add(newPathCell);
                }

                //for room border
                else if (level.roomTile[i, j] != TileType.AIR && level.roomTile[i, j] != TileType.WALL)
                {
                    string walls = "";
                    if (level.roomTile[i + 1, j] == TileType.WALL)
                        walls += "R";
                    if (level.roomTile[i - 1, j] == TileType.WALL)
                        walls += "L";
                    if (level.roomTile[i, j - 1] == TileType.WALL)
                        walls += "U";
                    if (level.roomTile[i, j + 1] == TileType.WALL)
                        walls += "D";

                    GameObject newPathCell = Instantiate(Resources.Load("Prefab/Dungeon/" + theme.themeName + "/Cell/Path_Cell_" + walls), new Vector3(j, 0, i), Quaternion.identity) as GameObject;
                    newPathCell.transform.parent = dungeonParent.transform;
                    level.referenceMap[i, j].Add(newPathCell);
                }
            }
        }
    }

    private void RenderDoors(Level level, DungeonTheme theme)
    {
        foreach (DoorStruct d in level.entities["door"])
        {
            GameObject newDoor = Instantiate(Resources.Load("Prefab/Dungeon/" + theme.themeName + "/Cell/Door_Cell"), new Vector3(d.pos.x, 0, d.pos.z), Quaternion.Euler(0, (d.rotation - 1) * 90, 0)) as GameObject;
            newDoor.transform.parent = dungeonParent.transform;
            level.referenceMap[d.pos.z, d.pos.x].Add(newDoor);
        }
    }

    private void RenderProp(Level level, DungeonTheme theme)
    {
        foreach (PropStruct prop in level.entities["prop"])
        {
            Vector3 pos;
            if (prop.w == 1 && prop.h == 1)
                pos = new Vector3(prop.pos.x, 0, prop.pos.z);
            else
                pos = new Vector3((prop.pos.x + (prop.h / 2.0f)) - 0.5f, 0, (prop.pos.z + (prop.w / 2.0f)) - 0.5f);

            GameObject newProp = Instantiate(theme.props[prop.type], pos, Quaternion.identity) as GameObject;
            newProp.transform.parent = dungeonParent.transform;
            level.referenceMap[(int)pos.z, (int)pos.x].Add(newProp);
            newProp.GetComponent<Prop>().id = prop.id;
        }

        //stair to next floor
        GameObject stair = Instantiate(theme.stairs[0], level.stairDownPoint.ToVector3(), Quaternion.identity) as GameObject;
        stair.transform.parent = dungeonParent.transform;
        foreach (GameObject cell in level.referenceMap[level.stairDownPoint.z, level.stairDownPoint.x])
        {
            if (cell.tag == "Tile")
                cell.GetComponent<PathCell>().HideFloor();
        }
        level.referenceMap[level.stairDownPoint.z, level.stairDownPoint.x].Add(stair);

        //stair to prev floor
        GameObject stairToPrevFloor = Instantiate(theme.stairs[1], level.spawnPoint.ToVector3(), Quaternion.identity) as GameObject;
        stairToPrevFloor.transform.parent = dungeonParent.transform;
        level.referenceMap[level.spawnPoint.z, level.spawnPoint.x].Add(stairToPrevFloor);
    }

    private void RenderMonster(Level level, DungeonTheme theme)
    {
        foreach (MonsterStruct monster in level.entities["monster"])
        {
            GameObject newMonster = Instantiate(theme.monsters[monster.monsterType], new Vector3(monster.pos.x * DUNGEON_SCALE, 0, monster.pos.z * DUNGEON_SCALE), Quaternion.Euler(0, (monster.rotation - 1) * 90, 0)) as GameObject;
            newMonster.transform.parent = monsterParent.transform;

            EnemyScript monsterScript = newMonster.GetComponent<EnemyScript>();
            Dictionary<string, int> maxStat = new Dictionary<string, int>();
            var monsterStat = ImmortalObject.fullMonsterList[monster.monsterID];
            maxStat.Add("hp", int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPBASE].ToString()) + (int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPGROWTH].ToString()) * (theme.baseItemLevel + buildingFloor)));
            maxStat.Add("atk", int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKBASE].ToString()) + (int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKGROWTH].ToString()) * (theme.baseItemLevel + buildingFloor)));
            maxStat.Add("def", int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFBASE].ToString()) + (int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFGROWTH].ToString()) * (theme.baseItemLevel + buildingFloor)));
            string monsterName = monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERNAME].ToString();
            int goldDrop = int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDBASE].ToString()) + (int.Parse(monsterStat[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDGROWTH].ToString()) * (theme.baseItemLevel + buildingFloor));
            
            monsterScript.stats = maxStat;
            monsterScript.currentStats = new Dictionary<string,int>(maxStat);
            monsterScript.enemyName = monsterName;
            monsterScript.goldDrop = goldDrop;
            newMonster.GetComponent<EnemyScript>().id = monster.id;
        }
    }

    private void SpawnPlayer(Level level)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player_Char");
        if (player == null)
        {
            GameObject playerGO = Instantiate(playerModel, level.spawnPoint.ToVector3(), Quaternion.identity) as GameObject;
            PlayerController playerScript = playerGO.GetComponent<PlayerController>();
            ChangeArmor changeArmorScript = playerGO.GetComponent<ChangeArmor>();
            Dictionary<string, int> maxStat = new Dictionary<string, int>();
            maxStat.Add("hp", 50);
            maxStat.Add("atk", 2);
            maxStat.Add("def", 2);
            foreach (int itemId in ImmortalObject.playerEquipments)
            {
                if (itemId == 0)
                    continue;

                int itemAtk = int.Parse(ImmortalObject.fullItemList[itemId + ""][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKBASE].ToString());
                int itemDef = int.Parse(ImmortalObject.fullItemList[itemId + ""][DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFBASE].ToString());
                int itemHp = int.Parse(ImmortalObject.fullItemList[itemId + ""][DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPBASE].ToString());
                int itemType = int.Parse(ImmortalObject.fullItemList[itemId + ""][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE].ToString());
                string modelName = ImmortalObject.fullItemList[itemId + ""][DATABASE_CONSTANTS.ITEMDATA_COLUMN_MODELNAME].ToString();

                maxStat["hp"] += itemHp;
                maxStat["atk"] += itemAtk;
                maxStat["def"] += itemDef;

                if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_WEAPON)
                    changeArmorScript.ShowRightHand(modelName);
                if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIELD)
                    changeArmorScript.ShowLeftHand(modelName);
                if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIRT)
                    changeArmorScript.ChangeBodyTo(modelName);
                if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_PANT)
                    changeArmorScript.ChangeLegTo(modelName);
            }

            playerScript.stats = maxStat;
            playerScript.currentStats = new Dictionary<string,int>(maxStat);
        }  
    }
}
