﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(FileIO))]
[RequireComponent(typeof(DungeonGenerator))]
[RequireComponent(typeof(DungeonBuilder))]
public class FloorManager : MonoBehaviour {
    DungeonBuilder builder;
    DungeonGenerator generator;

    public int currentFloor = -1;
    List<Level> floors = new List<Level>();

    //debug
    public DungeonTheme theme;

	void Start () {
        builder = GetComponent<DungeonBuilder>();
        generator = GetComponent<DungeonGenerator>();

        //read the dungeon selected from ImmortalObject.selectedDungeon
        //instantiate theme game object from prefab
        //start the dungeon generator and pass the theme GO: generator.GenerateDungeon(themeObject)
	}

    //next floor
    public void ChangeFloorDown()
    {
        currentFloor++;
        Level level;
        if (currentFloor >= floors.Count)
        {
            level = generator.GenerateDungeon(theme);
            floors.Add(level);
        }
        else
            level = floors[currentFloor];
        
        builder.Build(currentFloor, level, theme);
        GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().CreateMapFromCustomWalkable(level.walkable);
        print("changed to " + (currentFloor + 1) + " now have " + floors.Count + " floors");
    }

    //prev floor
    public void ChangeFloorUp()
    {
        currentFloor--;
        print("changing to floor " + (currentFloor + 1));
        builder.Build(currentFloor, floors[currentFloor], theme);
        GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().CreateMapFromCustomWalkable(floors[currentFloor].walkable);
        print("changed to " + (currentFloor + 1) + " now have " + floors.Count + " floors");
    }

    public Level getCurrentFloor()
    {
        return floors[currentFloor];
    }
}
