﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraControl : MonoBehaviour {
    public GameObject cameraCenter;
    public GameObject cameraLook;
    public GameObject cameraPosition;
    public Camera camera;
    public GameObject focusHero;
    float speed;

    TouchController touchController;
    FocusHero focusHeroScript;

	void Start () {
        Input.multiTouchEnabled = true;
        speed = 5 * Time.deltaTime;
        touchController = gameObject.GetComponent<TouchController>();
        focusHeroScript = focusHero.GetComponent<FocusHero>();
	}
	
	void Update () {
        //debug.text = "FPS : " + Mathf.Floor(1.0f / Time.smoothDeltaTime);
        camera.transform.LookAt(cameraLook.transform.position);
        if (touchController.isPanningCamera)
        {
            Move(InputX(true) * speed, InputY(true) * speed);
            focusHeroScript.StopFollowingHero();
        }

        if (touchController.isZoomingCamera)
        {
            Rotate(Input.touches[1].deltaPosition.x * speed * 30);
            Distance(Input.touches[1].deltaPosition.y * speed);
        }
	}

    /* Touch Position on X-Axis
     * @param isDevice : true for run on mobile
     */
    float InputX(bool isDevice)
    {
        if (isDevice)
        {
            return Input.touches[0].deltaPosition.x;
        }
        return Mathf.Ceil(Input.mousePosition.x);
    }

    /* Touch Position on Y-Axis
     * @param isDevice : true for run on mobile
     */
    float InputY(bool isDevice)
    {
        if (isDevice)
        {
            return Input.touches[0].deltaPosition.y;
        }
        return Mathf.Ceil(Input.mousePosition.y);
    }

    /* Rotate Camenra around focus
     * @param value : angle of rotation
     */
    void Rotate(float value)
    {
        //cameraLook.transform.Rotate(new Vector3(0,value*100,0));
        cameraCenter.transform.Rotate(new Vector3(0,value*50*Time.deltaTime,0));
    }

    /* Pan Camera
     * @param x, z : position in X and Z Axis
     */
    void Move(float x, float z)
    {
        //cameraCenter.transform.Translate(-x,0,-z);
        cameraCenter.transform.Translate(Vector3.forward * z * 50 * Time.deltaTime);
        cameraCenter.transform.Translate(Vector3.left * -x * 50 * Time.deltaTime);
    }

    /* Camera Distance from floor
     * @param y : position in Y axis
     */
    void Distance(float y)
    {
        cameraPosition.transform.Translate(0, y * 50 * Time.deltaTime, 0);
    }
}
