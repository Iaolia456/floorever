﻿using UnityEngine;
using System.Collections;

public class ChangeScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ToScene(int scene)
    {
        if (scene == 1)
            Application.LoadLevel("Test_Model_Performance");
        if (scene == 2)
            Application.LoadLevel("Test_Raycast");
        if (scene == 3)
            Application.LoadLevel("Town");
        if (scene == 4)
            Application.LoadLevel("Dungeon");
    }
}
