﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SFXManager : MonoBehaviour
{
    /* Get AudioSource from this gameObject */
    private AudioSource[] audioList;
    /* Get AudioSource from scene */
    private AudioSource bgm;

    /* Load Sound Pack
        1 = Menu Scene, 2 = GamePlay */
    public int soundPack;
        /* Slot for fever type */
    private int ferverSlot = 2;

    /* Volume of sfx sound */
    private float sfxVolume = 1;
    /* Volume of bgm sound */
    private float bgmVolume = 1;
    /* Play sample sfx sound while setting */
    private float playSampleSFXDelay = 0f;
    /* Mute all sound */
    private bool isMute;

    public void Awake()
    {
        isMute = false;
        /*if(GameObject.Find("BGM")!=null)
            bgm = GameObject.Find("BGM").GetComponent<AudioSource>();*/
        audioList = this.GetComponents<AudioSource>();
        if (soundPack == 0)
            menuSoundPack();
        if (soundPack == 1)
            gamePlaySoundPack();
        //LoadVolumeData();
    }


    public void Update()
    {
        playSampleSFXDelay -= Time.deltaTime;
    }

    /* Load sound for scene gameplay */
    void gamePlaySoundPack()
    {
        audioList[0].clip = Loads("Darkness3");
        audioList[1].clip = Loads("Move");
        audioList[2].clip = Loads("Chest");
        audioList[3].clip = Loads("Item3");
        audioList[4].clip = Loads("Blow1");
    }

    /* Load sound for scene menu */
    void menuSoundPack()
    {
        audioList[0].clip = Loads("click");
        audioList[1].clip = Loads("Open5");
    }

    /* Load audio resource from folder
     * @param name : file's name of sound
     * @return : AudioClip of result
     */
    public AudioClip Loads(string name)
    {
        return (AudioClip)Resources.Load("Sound/" + name);
    }

    /*
     * Find sound name and play it
     * @param name : name of sound
     */
    public void Play(string name)
    {
        if(audioList != null)
        foreach (AudioSource audio in audioList)
        {
            if (audio.clip != null && audio.clip.name.Equals(name))
            {
                audio.Play();
                return;
            }
        }
    }

    /* Set mute to all soud
     * @param val : UI Toggle
     */
    public void SetMute(GameObject val)
    {
        isMute = val.GetComponent<Toggle>().isOn;
        if (isMute)
        {
            PlayerPrefs.SetInt("isMute", 1);
            bgm.volume = 0;
        }
        else
        {
            PlayerPrefs.SetInt("isMute", 0);
            bgm.volume = bgmVolume;
        }

        sfxVolume = PlayerPrefs.GetFloat("sfxVolume");
        foreach (AudioSource audio in audioList)
        {
            if (audio.clip != null)
            {
                if (isMute)
                    audio.volume = 0;
                else
                    audio.volume = sfxVolume;
            }
        }
    }

    /* Set volume of sfx
     * @param val : UI Slider of object
     */
    public void SetSFXVolume(GameObject val)
    {
        sfxVolume = val.GetComponent<Slider>().value;
        PlayerPrefs.SetFloat("sfxVolume", sfxVolume);
        if(!isMute)
            SetAllSFXVolume(sfxVolume);
        else
            SetAllSFXVolume(0);
        if (playSampleSFXDelay <= 0){
            PlayClick();
            playSampleSFXDelay = 0.5f;
        }
    }

    /* Set volume of bgm
    * @param val : UI Slider of object
    */
    public void SetBGMVolume(GameObject val)
    {
        bgmVolume = val.GetComponent<Slider>().value;
        if(!isMute)
            bgm.volume = bgmVolume;
        else
            bgm.volume = 0;
        PlayerPrefs.SetFloat("bgmVolume", bgmVolume);
    }

    /* Load setting data from player's device */
    public void LoadVolumeData()
    {
        if (PlayerPrefs.HasKey("bgmVolume"))
        {
            bgmVolume = PlayerPrefs.GetFloat("bgmVolume");
            if (bgm != null)
            {
                bgm.volume = bgmVolume;
            }
        }
        else
            PlayerPrefs.SetFloat("bgmVolume", 1f);

        if (PlayerPrefs.HasKey("sfxVolume"))
        {
            sfxVolume = PlayerPrefs.GetFloat("sfxVolume");
            SetAllSFXVolume(sfxVolume);
        }
        else
            PlayerPrefs.SetFloat("sfxVolume", 1f);

        if (PlayerPrefs.HasKey("isMute"))
        {
            int getMute = PlayerPrefs.GetInt("isMute");
            if (getMute == 1) //true
            {
                isMute = true;
                if(bgm != null)
                    bgm.volume = 0;
                SetAllSFXVolume(0);
            }
            else
                isMute = false;
        }
        else
            PlayerPrefs.SetFloat("isMute", 0);
    }

    /* Set volume of all sfx
     * @param val : volume
     */
    private void SetAllSFXVolume(float val)
    {
        foreach (AudioSource audio in audioList)
        {
            if (audio.clip != null)
            {
                audio.volume = val;
            }
        }
    }

    // ================Menu Pack ================
    
    public void PlayClick()
    {
        Play("click");
    }

    public void PlayStart()
    {
        Play("Open5");
    }

    public void PlayClosePop()
    {
        Play("pop");
    }

    public void PlayErrorPop()
    {
        Play("pop");
    }

    public void PlaySuccessPop()
    {
        Play("pop");
    }

    // ================ GamePlay Pack ================

    public void PlayHitSound()
    {
        Play("Blow1");
    }

    public void PlayOpenChestSound()
    {
        Play("Chest");
    }

    public void PlayUseItem()
    {
        Play("Item3");
    }

    public void PlayChangeFloorSound()
    {
        audioList[1].clip = Loads("Move");
        Play("Move");
    }

    public void PlayMonsterDead()
    {
        Play("Darkness3");
    }

    public void PlayOpenDoor()
    {
        audioList[1].clip = Loads("Open3");
        Play("Open3");
    }

    public void PlayCloseDoor()
    {
        audioList[1].clip = Loads("Close2");
        Play("Close2");
    }
}
