﻿using UnityEngine;
using System.Collections;

public class ModelControl : MonoBehaviour {

    Vector3 startPosition;
    Quaternion startDirection;
    Animator animator;
    AnimatorStateInfo stateInfo;
    bool useLookAt = false;
    bool isAttacking = false;
    // Delay to update isAttacking after start attack animation
    float delay = 0.1f;

	void Start () {
        //animator = GetComponent<Animator>();
        startPosition = transform.position;
        startDirection = transform.rotation;
	}
	
	void Update () {
        //stateInfo = animator.GetCurrentAnimatorStateInfo(0);
        LookAtDestination();
        if (isAttacking)
        {
            if (delay >= 0)
            {
                delay -= Time.deltaTime;
            }
            else
            {
                isAttacking = false;
                SetStateToIdle();
                delay = 0.1f;
            }
        }
	}

    // Set animation to idle
    public void SetStateToIdle()
    {
        Animator[] animators= GetComponentsInChildren<Animator>();
        foreach (Animator ani in animators)
        {
            ani.SetBool("run", false);
            ani.SetBool("attack", false);
        }
        //animator.SetBool("run", false);
        //animator.SetBool("attack", false);
    }

    // Set animation to run
    public void SetStateToRun()
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        foreach (Animator ani in animators)
        {
            ani.SetBool("run", true);
            ani.SetBool("attack", false);
        }
            //animator.SetBool("run", true);
            //animator.SetBool("attack", false);
    }

    // Set animation to attack 
    public void SetStateToAttack()
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        foreach (Animator ani in animators)
        {
            ani.SetBool("run", false);
            ani.SetBool("attack", true);
        }
        //animator.SetBool("run", false);
        //animator.SetBool("attack", true);
        isAttacking = true;
    }

    /* Use for make model look at "Destination" Object
     * @param value : true for use this function
     */
    public void SetUseLookAt(bool value)
    {
        useLookAt = value;
    }

    // Model all ways look to "Destination" object
    void LookAtDestination()
    {
        if (!useLookAt) return;
        GameObject des = GameObject.Find("Destination");
        if (des != null)
        {
            transform.LookAt(new Vector3(des.transform.position.x, transform.position.y, des.transform.position.z));
        }
    }

    /* Model will look to target position
     * @param target : position of target
     */
    public void LookTo(Vector3 target)
    {
        SetUseLookAt(false);
        transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
    }

    // Reset position and face direction to first position
    public void ResetPosition()
    {
        transform.position = startPosition;
        transform.rotation = startDirection;
    }
}
