﻿using UnityEngine;
using System.Collections.Generic;

public static class StatCalculator{



    /*
     * Return collection of BaseStat<int> + ( BaseGrowth<float> * Multiplier(iLevel or Floor)<float> ) 
     * 
     * The concept of Item & Monster Progression are the same, they have finite growth rate to be multiplied with the multiplier whether its floor or item level.
     * */
    public static Dictionary<string,int> calculateStat(Dictionary<string, StatStruct<int,float>> InputStat, float Multiplier)
    {
        Dictionary<string,int> finalStat = new Dictionary<string,int>();
        foreach(KeyValuePair<string,StatStruct<int,float>> aStat in InputStat)
        {
            finalStat.Add(aStat.Key, Mathf.FloorToInt(aStat.Value.BaseStat() + (aStat.Value.BaseGrowth() * Multiplier)));
        }
        return finalStat;
    }



}
