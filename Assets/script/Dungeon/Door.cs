﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
    public const float ROTATE_SPEED = 700f;
    public const float CLOSE_DELAY = 0.1f; //if no collision detect with CLOSE_DELAY second, door will close
    public GameObject door;

    bool opened = false;
    bool opening = false;
    float timeSinceLastStay = 0;

	void Start () {
        
	}
	
	void Update () {
        timeSinceLastStay += Time.deltaTime;
        if (opening)
            Opening();
        else if (timeSinceLastStay >= CLOSE_DELAY)
            Closing();
	}

    private void Opening()
    {
        if (!opened)
        {
            Camera.main.GetComponent<SFXManager>().PlayOpenDoor();
            opened = true;
        }

        Vector3 newRotation = door.transform.localRotation.eulerAngles;
        newRotation.y += Time.deltaTime * ROTATE_SPEED;
        if (newRotation.y > 180) 
        {
            newRotation.y = 180;
            opening = false;
        }
        door.transform.localRotation = Quaternion.Euler(newRotation);
    }

    private void Closing()
    {
        if (opened)
        {
            Camera.main.GetComponent<SFXManager>().PlayCloseDoor();
            opened = false;
        }

        Vector3 newRotation = door.transform.localRotation.eulerAngles;
        newRotation.y -= Time.deltaTime * ROTATE_SPEED;
        if (newRotation.y == 90)
            return;
        if (newRotation.y < 90)
        {
            newRotation.y = 90;
        }
        door.transform.localRotation = Quaternion.Euler(newRotation);
    }

    void OnTriggerStay(Collider other)
    {
        timeSinceLastStay = 0;
        if (other.tag == "Enemy" || other.tag == "Player_Char")
        {
            opening = true;
        }
    }

    /*void OnTriggerEnter(Collider other)
    {
        //print("enter");
        if (other.tag == "Enemy" || other.tag == "Player_Char")
        {
            entityCount++;
        }
    }

    void OnTriggerExit(Collider other)
    {
        //print("exit");
        if (other.tag == "Enemy" || other.tag == "Player_Char")
        {
            entityCount--;
        }
    }*/
}
