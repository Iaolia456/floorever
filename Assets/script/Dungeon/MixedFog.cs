﻿using UnityEngine;
using System.Collections;

public class MixedFog : MonoBehaviour, IFoggable
{
    public GameObject tile;
    public GameObject prop;

	void Start () {
	
	}

    public void SetFogOfWar(int type)
    {
        tile.GetComponent<TileFog>().SetFogOfWar(type);
        prop.GetComponent<PropFog>().SetFogOfWar(type);
    }
}
