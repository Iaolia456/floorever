﻿using UnityEngine;
using System.Collections;

public interface IEntity {
    void Interact();
}
