﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class InventoryItem : MonoBehaviour {
    public Image itemIcon;
    public Text itemQuantity;

    KeyValuePair<int, int> item;
    int quantity;

	void Start () {

	}
	
	void Update () {
	
	}

    public void SetItemToDisplay(Sprite itemIcon, KeyValuePair<int, int> item, int quantity) 
    {
        this.item = item;
        this.quantity = quantity;
        itemQuantity.text = quantity + "";
        this.itemIcon.sprite = itemIcon;
    }

    public void AddItemQuantity(int quantityToBeAdded)
    {
        int oldQuantity = int.Parse(itemQuantity.text);
        quantity += quantityToBeAdded;
        itemQuantity.text = (oldQuantity + quantityToBeAdded) + "";
    }

    public void SetItemQuantity(int quantity)
    {
        this.quantity = quantity;
        itemQuantity.text = quantity + "";
    }

    public void ShowConfirmationDialog()
    {
        //dic<itemPropName, data(can be both string/number)>
        Dictionary<string, object> itemDetail = ImmortalObject.fullItemList[this.item.Key + ""];
        if (int.Parse(itemDetail["ItemType"] + "") == 2)
            GameObject.FindGameObjectWithTag("Inventory").GetComponent<InventoryScript>().ShowConfirmationDialog(item, "Use " + itemDetail["ItemName"] + "?");
    }
}
