﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FocusHero : MonoBehaviour {
    public Sprite focusing;
    public Sprite free;

    float CAMERA_SPEED; //match the hero's RUN_SPEED
    GameController controller;
    GameObject heroChaseCam;
    bool followingHero = true;
    Image buttonImageScript;

    void Start () {
        controller = Camera.main.GetComponent<GameController>();
        heroChaseCam = GameObject.FindGameObjectWithTag("Hero_Chase_Camera");
        buttonImageScript = gameObject.GetComponent<Image>();
	}
	
	void Update () {
        if (!controller.gameRunning)
            return;

        if (followingHero)
        {
            Vector3 newPos = controller.player.transform.position;
            newPos.y = +1;
            heroChaseCam.transform.position = newPos;
        }
	}

    public void ToggleFollowing()
    {
        followingHero = !followingHero;
        if (followingHero)
            buttonImageScript.sprite = free;
        else
            buttonImageScript.sprite = focusing;
    }

    public void StopFollowingHero()
    {
        if (followingHero)
        {
            followingHero = false;
            buttonImageScript.sprite = focusing;
        }
    }
}
