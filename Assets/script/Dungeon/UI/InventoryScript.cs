﻿using UnityEngine;
using System.Collections.Generic;

public class InventoryScript : MonoBehaviour {
    public GameObject confirmDialog;
    public GameObject itemGrid;
    public GameObject itemSlotPrefab;
    public Dictionary<KeyValuePair<int, int>, int> items = new Dictionary<KeyValuePair<int, int>, int>();
    Dictionary<int, GameObject> inventoryItemGO = new Dictionary<int, GameObject>();

    public Sprite[] itemIcons;

	void Start () {
        
	}
	
	void Update () {
	
	}

    public void ShowConfirmationDialog(KeyValuePair<int, int> usingItem, string message)
    {
        confirmDialog.SetActive(true);
        confirmDialog.GetComponent<UseItemConfirmationPopup>().ShowConfirmationDialog(this, usingItem, message);
    }

    public void UseItem(KeyValuePair<int, int> item)
    {
        GameObject itemToUse = Instantiate(Resources.Load("Prefab/Dungeon/Item_Prefab/Item_" + item.Key)) as GameObject;
        (itemToUse.GetComponent(typeof(IItem)) as IItem).ApplyEffect(Camera.main.GetComponent<GameController>().player);
        RemoveItemFromInventory(item, 1);
    }

    public void UpdateUI()
    {
        foreach (var item in items)
        {
            if (inventoryItemGO.ContainsKey(item.Key.Key))
            {
                inventoryItemGO[item.Key.Key].GetComponent<InventoryItem>().SetItemQuantity(item.Value);
            }
            else
            {
                GameObject newInvSlot = Instantiate(itemSlotPrefab) as GameObject;
                newInvSlot.GetComponent<InventoryItem>().SetItemToDisplay(itemIcons[item.Key.Key - 1], item.Key, item.Value);
                newInvSlot.transform.SetParent(itemGrid.transform);
                newInvSlot.transform.localScale = new Vector3(1, 1, 1);
                inventoryItemGO.Add(item.Key.Key, newInvSlot);
            }
        }
    }

    public void AddItemToInventory(KeyValuePair<int, int> itemInfo, int quantity)
    {
        if (itemInfo.Key == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE)
            ImmortalObject.playerConsumable += quantity;

        if (items.ContainsKey(itemInfo)) //already have that item in inv and item can stack
        {
            items[itemInfo] += quantity;
        }
        else //don't have item
        {
            items.Add(itemInfo, quantity);
        }

        UpdateUI();
    }

    public void RemoveItemFromInventory(KeyValuePair<int, int> itemInfo, int quantity)
    {
        if (items.ContainsKey(itemInfo))
        {
            items[itemInfo] -= quantity;
            ImmortalObject.playerConsumable -= quantity;
            if (items[itemInfo] == 0)
            {
                items.Remove(itemInfo);
                ImmortalObject.playerConsumable = 0;
            }

            DeviceIDUserHandler.EasyUpdateUser();
        }

        UpdateUI();
    }

    public Dictionary<KeyValuePair<int, int>, int> RandomDestroyItems(int count)
    {
        Dictionary<KeyValuePair<int, int>, int> toRemove = new Dictionary<KeyValuePair<int, int>, int>();
        for (int i = 0; i < count; i++)
        {
            int removeAt = Random.Range(0, items.Count - 1);
            int j = 0;
            foreach (KeyValuePair<int, int> item in items.Keys)
            {
                if (j == removeAt)
                {
                    toRemove.Add(item, items[item]);
                    break;
                }
                j++;
            }
        }

        foreach (KeyValuePair<int, int> toRemoveItem in toRemove.Keys)
        {
            print("Lost " + "(id:" + toRemoveItem.Key + ") " + ImmortalObject.fullItemList[toRemoveItem.Key + ""]["ItemName"]);
            items.Remove(toRemoveItem);
        }

        return toRemove;
    }
}
