﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InfoPanel : MonoBehaviour {
    public Text nameText;
    public Slider hp;
    public Text hpText;

	void Start () {
	
	}

    public void SetValue(string entityName, int hp, int maxHp)
    {
        nameText.text = entityName;
        this.hp.value = hp / (float)maxHp;
        hpText.text = hp + "";
    }
}
