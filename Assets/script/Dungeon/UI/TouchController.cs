﻿using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour {
    const float TAP_DISTANCE_THRESHOLD = 5f; //touch up and touch down must below TAP_THRESHOLD to register raycast hit
    public bool isZoomingCamera = false;
    public bool isPanningCamera = false;
    public bool isTappingTerrain = false;
    public bool isTappingUI = false;

    bool touching = false;
    Vector2 touchDownPos;
    Vector2 touchUpPos;

	void Start () {
	
	}
	
	void Update () {
        //mobile
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.touchCount > 0)
            {
                if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(0) || UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(-1))
                {
                    isTappingUI = true;
                    return;
                }
                if (Input.GetMouseButton(0) && !touching)
                {
                    touching = true;
                    touchDownPos = Input.mousePosition;
                }
                else if (Input.touchCount == 2)
                {
                    isZoomingCamera = true;
                }
                if (Input.touches[0].deltaPosition.magnitude > TAP_DISTANCE_THRESHOLD)
                    isPanningCamera = true;
            }
            else if (!Input.GetMouseButton(0) && touching && !isZoomingCamera)
            {
                touchUpPos = Input.mousePosition;
                if (Vector2.Distance(touchDownPos, touchUpPos) <= TAP_DISTANCE_THRESHOLD)
                {
                    isTappingTerrain = true;
                }
                touching = false;
            }
            else
            {
                isZoomingCamera = false;
                isTappingTerrain = false;
                isPanningCamera = false;
                isTappingUI = false;
                touching = false;
            }
        }
        else //pc
        {
            if (Input.GetMouseButton(0))
            {
                isTappingTerrain = true;
            }
            else
                isTappingTerrain = false;
        }
        
        
	}
}
