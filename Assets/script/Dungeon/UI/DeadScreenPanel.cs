﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class DeadScreenPanel : MonoBehaviour {
    public GameObject inventorySlot;
    public GameObject lostItemPanelParent;
    public GameObject inventory;

	void Start () {
	
	}

    public void DisplayItemsLost(Dictionary<KeyValuePair<int, int>, int> itemsLost)
    {
        InventoryScript invScript = inventory.GetComponent<InventoryScript>();
        foreach (var item in itemsLost)
        {
            GameObject invSlot = Instantiate(inventorySlot) as GameObject;
            invSlot.GetComponent<InventoryItem>().SetItemToDisplay(invScript.itemIcons[item.Key.Key - 1], item.Key, item.Value);
            invSlot.transform.SetParent(lostItemPanelParent.transform);
        }
    }
}
