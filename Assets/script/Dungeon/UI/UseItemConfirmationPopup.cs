﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UseItemConfirmationPopup : MonoBehaviour {
    public Text messageText;
    KeyValuePair<int, int> usingItem;
    InventoryScript host;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ShowConfirmationDialog(InventoryScript host, KeyValuePair<int, int> usingItem, string message)
    {
        messageText.text = message;
        this.host = host;
        this.usingItem = usingItem;
    }

    public void ClosePopup()
    {
        gameObject.SetActive(false);
    }

    public void Yes()
    {
        ClosePopup();
        host.UseItem(usingItem);
    }
}
