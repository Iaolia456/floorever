﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UI : MonoBehaviour {
    public GameObject playerInfoPanel;
    public GameObject targetInfoPanel;
    public Text floorNo;
    public GameObject processingTurn;
    public GameObject loadingScreen;
    public GameObject gameOverScreen;

    InfoPanel playerInfoPanelScript;
    InfoPanel targetInfoPanelScript;

    GameController controller;

    //debug
    public Text fpsCounter;

	void Start () {
        controller = Camera.main.GetComponent<GameController>();
        playerInfoPanelScript = playerInfoPanel.GetComponent<InfoPanel>();
        targetInfoPanelScript = targetInfoPanel.GetComponent<InfoPanel>();
	}
	
	void Update () {
        fpsCounter.text = Mathf.Floor(1.0f / Time.smoothDeltaTime) + " FPS";
	}

    public void UpdateInfoPanel()
    {
        //update player info
        var playerParams = controller.player.GetComponent<PlayerController>().currentStats;
        var playerMaxStatsParams = controller.player.GetComponent<PlayerController>().stats;
        playerInfoPanelScript.SetValue("Aiolia", playerParams["hp"], playerMaxStatsParams["hp"]);

        //update target info
        if (controller.target != null && controller.target.tag == "Enemy")
        {
            targetInfoPanel.SetActive(true);
            EnemyScript target = controller.target.GetComponent<EnemyScript>();
            var targetParams = target.currentStats;
            var targetMaxStatsParams = target.stats;
            targetInfoPanelScript.SetValue(target.enemyName, targetParams["hp"], targetMaxStatsParams["hp"]);
        }
        else
        {
            targetInfoPanel.SetActive(false);
        }
    }

    public void SetProcessingTurnIcon(bool visible)
    {
        processingTurn.gameObject.SetActive(visible);
    }

    public void SetFloorNumber(int floorNumber)
    {
        floorNo.text = "Floor " + (floorNumber + 1);
    }

    public void ShowLoadingScreen()
    {
        loadingScreen.SetActive(true);
    }

    public void HideLoadingScreen()
    {
        loadingScreen.SetActive(false);
    }

    public void ShowDeadScreen(Dictionary<KeyValuePair<int, int>, int> lostItem)
    {
        gameOverScreen.SetActive(true);
        var deadScreen = gameOverScreen.transform.GetChild(0).gameObject;
        deadScreen.SetActive(true);
        deadScreen.GetComponent<DeadScreenPanel>().DisplayItemsLost(lostItem);
    }

    public void ShowReturnToTownScreen()
    {
        gameOverScreen.SetActive(true);
        var returnToTownScreen = gameOverScreen.transform.GetChild(1).gameObject;
        returnToTownScreen.SetActive(true);
    }
}
