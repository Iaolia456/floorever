﻿using UnityEngine;
using System.Collections;

public class TileFog : MonoBehaviour, IFoggable
{
    void Start () {
        
	}

    public void SetFogOfWar(int type)
    {
        foreach (Transform child in transform)
        {
            if (child.gameObject.name == "Floor")
                child.gameObject.renderer.material = FogOfWar.tileFogMat[0 + type];
            else if (child.gameObject.name == "Wall")
                child.gameObject.renderer.material = FogOfWar.tileFogMat[3 + type];
            else if (child.gameObject.name == "Door")
                child.gameObject.renderer.material = FogOfWar.tileFogMat[6 + type];
        }
    }
}
