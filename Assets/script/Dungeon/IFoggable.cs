﻿using UnityEngine;
using System.Collections;

public interface IFoggable {
    void SetFogOfWar(int type);
}
