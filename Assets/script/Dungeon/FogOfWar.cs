﻿using UnityEngine;
using System.Collections.Generic;

public class FogOfWar : MonoBehaviour {
    public const int VISIBLE = 2;
    public const int EXPLORED = 1;
    public const int UNEXPLORE = 0;
    public const float UPDATE_INTERVAL = 0.05f;
    public const int VISIBILITY_LENGTH = 9; //square length around the player that player can see, better be odd number
    public static Material[] tileFogMat = new Material[12];

    public byte[,] fog;
    GameController controller;
    Vector3 playerPos;
    bool fogRunning;

    //debug
    public GameObject visibleMarker;
    public GameObject exploredMarker;
    public GameObject unexploredMarker;
    public bool enable;
    byte[,] godEye;
    GameObject debug;

    float updateTimer = 0;

	void Start () {
        
	}
	
	void Update () {
        //debug
        if (!enable)
            return;

        updateTimer += Time.deltaTime;
        if (updateTimer < UPDATE_INTERVAL || !fogRunning)
            return;

        updateTimer = 0;
        playerPos = controller.player.transform.position;
        UpdateFog();
        //Debug();
	}

    public void LoadMaterial()
    {
        string themeName = GameObject.FindGameObjectWithTag("Dungeon_Theme").GetComponent<DungeonTheme>().themeName;
        tileFogMat[0] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Floor_Unexplored") as Material;
        tileFogMat[1] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Floor_Explored") as Material;
        tileFogMat[2] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Floor_Visible") as Material;
        tileFogMat[3] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Wall_Unexplored") as Material;
        tileFogMat[4] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Wall_Explored") as Material;
        tileFogMat[5] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Wall_Visible") as Material;
        tileFogMat[6] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Door_Unexplored") as Material;
        tileFogMat[7] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Door_Explored") as Material;
        tileFogMat[8] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Dun_Door_Visible") as Material;
        tileFogMat[9] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Stair_Down_Unexplored") as Material;
        tileFogMat[10] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Stair_Down_Explored") as Material;
        tileFogMat[11] = Resources.Load("Prefab/Dungeon/" + themeName + "/Mat/Stair_Down_Visible") as Material;
    }

    public void StartFogOfWar()
    {
        controller = Camera.main.GetComponent<GameController>();
        if (controller.currentFloor.fog != null)
        {
            fog = controller.currentFloor.fog;
            for (int i = 0; i < fog.GetLength(0); i++)
            {
                for (int j = 0; j < fog.GetLength(1); j++)
                {
                    UpdateMatAt(new Vector3(j, 0, i), fog[i,j]);
                }
            }

            fogRunning = true;
            return;
        }

        fog = new byte[controller.currentFloor.roomTile.GetLength(0), controller.currentFloor.roomTile.GetLength(1)];
        for (int i = 0; i < fog.GetLength(0); i++)
        {
            for (int j = 0; j < fog.GetLength(1); j++)
            {
                UpdateMatAt(new Vector3(j, 0, i), UNEXPLORE);
            }
        }

        //debug
        if (!enable)
        {
            godEye = new byte[controller.currentFloor.roomTile.GetLength(0), controller.currentFloor.roomTile.GetLength(1)];
            for (int i = 0; i < fog.GetLength(0); i++)
            {
                for (int j = 0; j < fog.GetLength(1); j++)
                {
                    UpdateMatAt(new Vector3(j, 0, i), VISIBLE);
                    godEye[i, j] = VISIBLE;
                }
            }
            fog = godEye;
        }

        fogRunning = true;
    }

    public void StopFogOfWar()
    {
        fogRunning = false;
    }

    private void UpdateFog()
    {
        for (int i = -((VISIBILITY_LENGTH / 2) + 1); i < (VISIBILITY_LENGTH / 2) + 1; i++)
        {
            for (int j = -((VISIBILITY_LENGTH / 2) + 1); j < (VISIBILITY_LENGTH / 2) + 1; j++) 
            {
                //out of visibility length => explored
                if (i == -((VISIBILITY_LENGTH / 2) + 1) || j == -((VISIBILITY_LENGTH / 2) + 1) || i == (VISIBILITY_LENGTH / 2) || j == (VISIBILITY_LENGTH / 2))
                {
                    if (fog[(int)playerPos.z + i, (int)playerPos.x + j] == VISIBLE)
                    {
                        fog[(int)playerPos.z + i, (int)playerPos.x + j] = EXPLORED;
                        Vector3 tilePos = new Vector3(playerPos.x + j, 0, playerPos.z + i);
                        UpdateMatAt(tilePos, EXPLORED);
                    }
                        
                }
                //in visibility length
                else
                {
                    List<Vector3> points = Line(new Vector3(playerPos.x + i, 0, playerPos.z + j));
                    foreach (Vector3 v in points)
                    {
                        IntVector3 vInt = IntVector3.ToIntVector3(v);
                        if (controller.currentFloor.roomTile[vInt.z, vInt.x] == TileType.WALL ||
                            (controller.currentFloor.pathTile[vInt.z, vInt.x] == TileType.WALL && controller.currentFloor.roomTile[vInt.z, vInt.x] != TileType.ROOM))
                            break;

                        fog[(int)v.z, (int)v.x] = VISIBLE;
                        UpdateMatAt(v, VISIBLE);
                    }
                }
            }
        }
    }

    private void UpdateMatAt(Vector3 pos, int type)
    {
        foreach (GameObject go in controller.currentFloor.referenceMap[(int)pos.z, (int)pos.x])
        {
            (go.GetComponent(typeof(IFoggable)) as IFoggable).SetFogOfWar(type);

            /*if (go.tag == "Tile")
                go.GetComponent<TileFog>().SetFogOfWar(type);
            else if (go.tag == "Prop" || go.tag == "Entity")
                go.GetComponent<PropFog>().SetFogOfWar(type);
            else if (go.tag == "Mixed")
                go.GetComponent<MixedFog>().SetFogOfWar(type);*/
        }
    }

    private List<Vector3> Line(Vector3 end)
    {
        List<Vector3> points = new List<Vector3>();
        float n = DiagonalDistance(playerPos, end);
        for (int step = 0; step <= n; step++) {
            float t = n == 0? 0.0f : step / n;
            points.Add(RoundPoint(Vector3.Lerp(playerPos, end, t)));
        }
        
        return points;
    }

    private float DiagonalDistance(Vector3 p0, Vector3 p1) {
        float dx = p1.x - p0.x;
        float dz = p1.z - p0.z;
        return Mathf.Max(Mathf.Abs(dx), Mathf.Abs(dz));
    }

    private Vector3 RoundPoint(Vector3 p)
    {
        return new Vector3(Mathf.Round(p.x), Mathf.Round(p.y), Mathf.Round(p.z));
    }

    private void Debug()
    {
        Destroy(debug);
        debug = new GameObject();
        for (int i = 0; i < fog.GetLength(0); i++)
        {
            for (int j = 0; j < fog.GetLength(1); j++)
            {
                if (fog[i, j] == VISIBLE)
                {
                    var go = Instantiate(visibleMarker, new Vector3(j, 0.2f, i), Quaternion.identity) as GameObject;
                    go.transform.parent = debug.transform;
                }
                else if (fog[i, j] == EXPLORED)
                {
                    var go = Instantiate(exploredMarker, new Vector3(j, 0.2f, i), Quaternion.identity) as GameObject;
                    go.transform.parent = debug.transform;
                }
            }
        }
    }
}
