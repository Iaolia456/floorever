﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class EnemyScript : Pathfinding {
    //animation speed doesn't affect any stats
    public const float RUN_SPEED = 5.0f;
    public float MOVE_SPEED = 1.0f;
    public float DETECTION_RANGE = 5.0f;
    public int ENERMY_MEMORY = 3; //will pursuit player for [x] turn
    public int ITEM_DROP_CHANCE;

    //need to fetch this from server
    public int id;
    public string enemyName;
    public Dictionary<string, int> stats; //original stats - 100% stat
    public Dictionary<string, int> currentStats; //stats effected by buff/debuff
    public int goldDrop;
    HideEnemy hideEnemyScript;

    GameController controller;
    Animator animator;
    int turnSinceLastPlayerDetect = 0;
    bool pursuiting = false;
    bool attacked = false;

    bool isAttacking = false;
    // Delay to update isAttacking after start attack animation
    float delay = 0.1f;

	void Start () {
        animator = GetComponentInChildren<Animator>();
        controller = Camera.main.GetComponent<GameController>();
        hideEnemyScript = gameObject.transform.GetChild(0).gameObject.GetComponent<HideEnemy>();
	}
	
	void Update () {
        if (Path.Count > 0)
            Pursuit();

        if (controller.fow.enable)
        {
            if (controller.fow.fog[(int)gameObject.transform.position.z, (int)gameObject.transform.position.x] == FogOfWar.VISIBLE)
                hideEnemyScript.Show();
            else
                hideEnemyScript.Hide();
        }

        if (isAttacking)
        {
            if (delay >= 0)
            {
                delay -= Time.deltaTime;
            }
            else
            {
                isAttacking = false;
                SetStateToIdle();
                delay = 0.1f;
            }
        }
	}

    int movedTile = 0;
    int playerMoveTileCount = 0;
    Vector3 playerPos;
    public void PerformAction(Vector3 playerPos, int movedTileCount)
    {
        attacked = false;
        if (movedTileCount == 0)
        {
            controller.FinishProcessingEnemy();
            return;
        }
        this.playerPos = playerPos;
        playerMoveTileCount = movedTileCount;
        //print(gameObject + " moveable " + Mathf.Round(MOVE_SPEED * movedTileCount) + " tiles " + DistanceToPlayer() + " away from player");
        
        //if player is within attack range (1 tile)
        if (DistanceToPlayer() <= 1.5f)
        {
            //attack player
            StartPursuit();
            AttackPlayer();
            controller.FinishProcessingEnemy();
        }
        //if player within monster's detection range
        else if (DistanceToPlayer() <= DETECTION_RANGE)
        {
            StartPursuit();
            SetCurrentTileWalkable(true);
            
            FindPath(gameObject.transform.position, playerPos, PathFindingFinish);
        }
        //player is not in range but monster still has 'memory'
        else if (pursuiting && turnSinceLastPlayerDetect < ENERMY_MEMORY)
        {
            turnSinceLastPlayerDetect++;
            SetCurrentTileWalkable(true);
            
            FindPath(gameObject.transform.position, playerPos, PathFindingFinish);
        }
        //idle stage: no pursuiting, player within range
        else
        {
            StopPursuit();
            Path = new List<Vector3>();
            SetCurrentTileWalkable(false);
            controller.FinishProcessingEnemy();
        }
    }

    private float DistanceToPlayer()
    {
        return Vector3.Distance(gameObject.transform.position, playerPos);
    }

    private void StartPursuit()
    {
        turnSinceLastPlayerDetect = 0;
        pursuiting = true;
    }

    private void StopPursuit()
    {
        pursuiting = false;
        SetStateToIdle();
    }

    private void SetCurrentTileWalkable(bool walkable)
    {
        controller.currentFloor.walkable[(int)gameObject.transform.position.z, (int)gameObject.transform.position.x] = walkable;
        GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().DynamicNodeUpdate((int)gameObject.transform.position.x, (int)gameObject.transform.position.z, walkable);
    }

    private void Pursuit()
    {
        LookTo(Path[0]);
        SetStateToRun();
        bool nextPathWalkable = controller.currentFloor.walkable[(int)Path[0].z, (int)Path[0].x];
        if (nextPathWalkable)
            transform.position = Vector3.MoveTowards(transform.position, Path[0], Time.deltaTime * RUN_SPEED);
        
        if (Vector3.Distance(transform.position, Path[0]) < 0.1F)
        {
            movedTile++;
            gameObject.transform.position = Path[0];
            Path.RemoveAt(0);
        }
        //reach destination or out of move or next tile has obstrucle
        int moveLimit = (int)Mathf.Ceil(MOVE_SPEED * playerMoveTileCount);
        if (movedTile >= moveLimit || !nextPathWalkable || Path.Count == 0)
        {
            Path = new List<Vector3>();
            SetStateToIdle();
            movedTile = 0;
            //if player still within detection range
            if (DistanceToPlayer() <= DETECTION_RANGE)
                turnSinceLastPlayerDetect = 0;
            if (movedTile < moveLimit && DistanceToPlayer() <= 1.2f)
                AttackPlayer(); //if there're turn left for monster

            controller.currentFloor.walkable[(int)gameObject.transform.position.z, (int)gameObject.transform.position.x] = false;
            controller.FinishProcessingEnemy();
            return;
        }
    }

    private void PathFindingFinish()
    {
        if (Path.Count == 0)
        {
            SetCurrentTileWalkable(false);
            controller.FinishProcessingEnemy();
        }
    }

    private void FinishSurroundingPlayer()
    {
        if (DistanceToPlayer() <= 1.2f)
            AttackPlayer();

        PathFindingFinish();
    }

    private void AttackPlayer()
    {
        if (!attacked)
        {
            controller.player.GetComponent<PlayerController>().ReceiveDamageFrom(gameObject);
            LookTo(controller.player.transform.position);
            SetStateToAttack();
        }
        
        attacked = true;
    }

    public void ReceiveDamageFrom(GameObject other)
    {
        //TODO need atk/def calculation
        LookTo(other.transform.position);
        if (other.tag == "Player_Char")
        {
            var playerStat = other.GetComponent<PlayerController>().currentStats;
            int damage = playerStat["atk"] - currentStats["def"];
            if (damage > 0)
                currentStats["hp"] -= damage;
            else
                currentStats["hp"] -= 1;
        }

        if (currentStats["hp"] <= 0)
            Dead();
    }

    public void Dead()
    {
        Camera.main.GetComponent<SFXManager>().PlayMonsterDead();
        gameObject.tag = "Dead_Enemy";
        Instantiate(Resources.Load("Prefab/Dungeon/Monster/Death_Smoke") as GameObject, gameObject.transform.position, Quaternion.Euler(-90, 0, 0));
        SetCurrentTileWalkable(true);
        controller.EnemyDead(gameObject);
        Destroy(gameObject);
    }

    // Set animation to idle
    public void SetStateToIdle()
    {
        animator.SetBool("run", false);
        animator.SetBool("attack", false);
    }

    // Set animation to run
    public void SetStateToRun()
    {
        animator.SetBool("run", true);
        animator.SetBool("attack", false);
    }

    // Set animation to attack 
    public void SetStateToAttack()
    {
        animator.SetBool("run", false);
        animator.SetBool("attack", true);
        isAttacking = true;
    }

    /* Model will look to target position
     * @param target : position of target
     */
    public void LookTo(Vector3 target)
    {
        transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
    }
}
