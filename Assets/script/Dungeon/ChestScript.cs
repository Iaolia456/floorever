﻿using UnityEngine;
using System.Collections.Generic;
using MiniJSON;

public class ChestScript : MonoBehaviour, IEntity {
    GameController controller;
    FloorManager floorMan;
    DungeonTheme theme;

	void Start () {
        theme = GameObject.FindGameObjectWithTag("Dungeon_Theme").GetComponent<DungeonTheme>();
        floorMan = Camera.main.GetComponent<FloorManager>();
        controller = Camera.main.GetComponent<GameController>();
	}

    public void Interact()
    {
        int dropCount = 1;
        List<KeyValuePair<int, int>> drops = ItemHandler.RandomizeItem(theme.dropTable, theme.baseItemLevel + floorMan.currentFloor, dropCount);
        controller.GetItems(drops);

        //delete chest
        Camera.main.GetComponent<SFXManager>().PlayOpenChestSound();
        Instantiate(Resources.Load("Prefab/Dungeon/Monster/Death_Smoke") as GameObject, gameObject.transform.position, Quaternion.Euler(-90, 0, 0));
        controller.currentFloor.walkable[(int)gameObject.transform.position.z, (int)gameObject.transform.position.x] = true;
        GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().DynamicNodeUpdate((int)gameObject.transform.position.x, (int)gameObject.transform.position.z, true);
        controller.PropDestroy(gameObject);
        controller.ProcessTurn(0);
        Destroy(gameObject);
    }
}
