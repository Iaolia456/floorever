﻿using UnityEngine;
using System.Collections;

public interface IItem {
    void ApplyEffect(GameObject to);
}
