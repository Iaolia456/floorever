﻿using UnityEngine;
using System.Collections;

public class Potion : MonoBehaviour, IItem {

	void Start () {
        Destroy(gameObject, 5);
	}
	
    public void ApplyEffect(GameObject to)
    {
        if (to.tag == "Player_Char")
        {
            Camera.main.GetComponent<SFXManager>().PlayUseItem();
            int maxHp = to.GetComponent<PlayerController>().stats["hp"];
            int currentHp = to.GetComponent<PlayerController>().currentStats["hp"];

            if (currentHp + 60 <= maxHp)
                to.GetComponent<PlayerController>().currentStats["hp"] += 60;
            else
                to.GetComponent<PlayerController>().currentStats["hp"] = maxHp;
        }
    }
}
