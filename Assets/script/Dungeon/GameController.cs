﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
    public const float TURN_DELAY = 0.25f;
    public Level currentFloor;
    public FogOfWar fow;
    public GameObject player;
    public GameObject target; //target enemy/entity of the player
    public GameObject inventoryUI;
    
    public bool gameRunning = false;
    bool processingTurn = false;
    float timeSinceAction = 0;

    DungeonTheme theme;
    InventoryScript inventory;
    FloorManager floorManager;
    UI ui;

    List<GameObject> enemies = new List<GameObject>();
    PlayerController playerController;
    Raycast raycaster;

    //debug
    public GameObject walkableMarker;
    
	void Start () {
        ui = Camera.main.GetComponent<UI>();
        floorManager = Camera.main.GetComponent<FloorManager>();
        inventory = inventoryUI.GetComponent<InventoryScript>();
        inventory.itemIcons = Resources.LoadAll<Sprite>("UI/Item_Icon/Icon_Set");
        theme = GameObject.FindGameObjectWithTag("Dungeon_Theme").GetComponent<DungeonTheme>();
    }

	void Update () {
        timeSinceAction += Time.deltaTime;
        if (gameRunning)
            UpdateUI();
        if (!gameRunning || processingTurn || timeSinceAction < TURN_DELAY)
            return;

        TakeTurn();
	}

    public void EndGame(int itemLostCount)
    {
        gameRunning = false;
        fow.StopFogOfWar();

        if (itemLostCount == 0) //walk out dungeon
            ui.ShowReturnToTownScreen();
        else
            ui.ShowDeadScreen(inventory.RandomDestroyItems(itemLostCount));

        foreach (var item in inventory.items)
        {
            if (int.Parse(ImmortalObject.fullItemList[item.Key.Key + ""][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE].ToString()) == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE)
            {
                ImmortalObject.playerConsumable = item.Value;
            }
            else
            {
                for (int i = 0; i < item.Value; i++)
                {
                    ImmortalObject.playerInventory.Add(item.Key);
                }
            }
        }

        DeviceIDUserHandler.EasyUpdateUser();
    }

    public void StartGame()
    {
        print("start game");
        if (!gameRunning)
        {
            raycaster = Camera.main.GetComponent<Raycast>();
            fow = GameObject.FindGameObjectWithTag("Fog_Of_War").GetComponent<FogOfWar>();
            fow.LoadMaterial();
            movingDown = true;
            print("start game in if");
            floorManager.ChangeFloorDown();
        }
    }

    public void DungeonFinishBuild()
    {
        enemies = new List<GameObject>();
        player = GameObject.FindGameObjectWithTag("Player_Char");
        playerController = player.GetComponent<PlayerController>();
        currentFloor = Camera.main.GetComponent<FloorManager>().getCurrentFloor();
        fow.StartFogOfWar();

        AddItemToPlayerInventory();
        StartCoroutine(FindEnemies());
    }

    private void AddItemToPlayerInventory()
    {
        if (ImmortalObject.playerConsumable > 0)
        {
            KeyValuePair<int, int> potion = new KeyValuePair<int, int>(15, 1);
            inventory.AddItemToInventory(potion, ImmortalObject.playerConsumable);
        }
    }

    //wait one update for Unity to destroy old enemy object then do FindAllWithTag
    System.Collections.IEnumerator FindEnemies()
    {
        yield return 1;

        var enemiesArray = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject go in enemiesArray)
            enemies.Add(go);

        if (movingDown)
            player.transform.position = currentFloor.spawnPoint.ToVector3();
        else
            player.transform.position = currentFloor.stairDownPoint.ToVector3();

        Vector3 newPos = GameObject.FindGameObjectWithTag("Hero_Chase_Camera").transform.position;
        newPos.x = player.transform.position.x;
        newPos.z = player.transform.position.z;
        newPos.y += 1;
        GameObject.FindGameObjectWithTag("Hero_Chase_Camera").transform.position = newPos;

        ui.HideLoadingScreen();
        gameRunning = true;
        processingTurn = false;
    }

    int finishProcess = 0;
    int movedTileCount = 0;
    public void ProcessTurn(int movedTileCount)
    {
        print("processing turn for " + enemies.Count);
        finishProcess = 0;
        this.movedTileCount = movedTileCount;
        if (enemies[0] == null) //i don't know why this ghost enemy keeps coming
            enemies.RemoveAt(0);
        enemies[0].GetComponent<EnemyScript>().PerformAction(player.transform.position, movedTileCount);
    }

    GameObject walkDebug;
    public void EndTurn()
    {
        int enemyNo = 0;
        foreach (GameObject e in enemies)
        {
            GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().DynamicNodeUpdate((int)e.transform.position.x, (int)e.transform.position.z, false);
            currentFloor.entities["monster"][enemyNo].pos = IntVector3.ToIntVector3(e.transform.position);
        }

        //debug
        /*Destroy(walkDebug);
        walkDebug = new GameObject("DEBUG_WalkableTile");
        for (int i = 0; i < currentFloor.walkable.GetLength(0); i++)
        {
            for (int j = 0; j < currentFloor.walkable.GetLength(1); j++)
            {
                if (currentFloor.walkable[i, j])
                    (Instantiate(walkableMarker, new Vector3(j, 0.1f, i), Quaternion.identity) as GameObject).transform.parent = walkDebug.transform;
            }
        }*/

        processingTurn = false;
    }
    
    public void FinishProcessingEnemy()
    {
        finishProcess++;
        if (finishProcess == enemies.Count)
            EndTurn();
        else
        {
            if (enemies[finishProcess] == null)
                enemies.RemoveAt(finishProcess);
            enemies[finishProcess].GetComponent<EnemyScript>().PerformAction(player.transform.position, movedTileCount);
        }
            
    }
    
    //processing the player's action
    GameObject queuedTarget = null;
    private void TakeTurn()
    {
        if (raycaster.isHit && !processingTurn)
        {
            timeSinceAction = 0;
            bool foundEntity = false;
            bool foundEnemy = false;
            RaycastHit[] raycastHits = raycaster.raycastHits;

            foreach (RaycastHit r in raycastHits)
            {
                if (r.collider.tag == "Entity")
                    foundEntity = true;
                if (r.collider.tag == "Enemy")
                    foundEnemy = true;
            }

            foreach (RaycastHit r in raycastHits)
            {
                //if unexplored tile or tap at the same place
                Vector3 raycastHit = new Vector3(Mathf.Round(r.point.x), r.point.y, Mathf.Round(r.point.z));
                if (raycastHit == player.transform.position || fow.fog[(int)raycastHit.z, (int)raycastHit.x] == FogOfWar.UNEXPLORE) 
                {
                    EndTurn();
                    break;
                } 

                //attack the enemy
                else if (r.collider.tag == "Enemy")
                {
                    queuedTarget = r.collider.gameObject;
                    if (Vector3.Distance(player.transform.position, r.collider.gameObject.transform.position) >= 1.5f)
                        MovePlayerToDestination(raycastHit, this.AttackTarget);
                    else
                        AttackTarget();

                    break;
                }

                //interact with entities
                else if (!foundEnemy && r.collider.tag == "Entity")
                {
                    queuedTarget = r.collider.gameObject;
                    if (Vector3.Distance(player.transform.position, r.collider.gameObject.transform.position) >= 1.5f)
                        MovePlayerToDestination(raycastHit, this.InteractEntity);
                    else
                        InteractEntity();

                    break;
                }

                //move action
                else if (!foundEntity && !foundEnemy && r.collider.tag == "Floor")
                {
                    MovePlayerToDestination(raycastHit);
                    break;
                } 
            } 
        }
    }

    private void AttackTarget()
    {
        processingTurn = true;
        target = queuedTarget.collider.gameObject;
        playerController.Attack(target);
        ProcessTurn(1);
    }

    private void InteractEntity()
    {
        processingTurn = true;
        (queuedTarget.GetComponent(typeof(IEntity)) as IEntity).Interact();
    }

    private void MovePlayerToDestination(Vector3 Destination, Action callback = null)
    {
        processingTurn = true;
        GameObject.FindGameObjectWithTag("Destination").transform.position = Destination;
        playerController.WalkToDestination(Destination, currentFloor.walkable[(int)Destination.z, (int)Destination.x], callback);
    }

    public void EnemyDead(GameObject enemy)
    {
        enemies.Remove(enemy);
        if (enemy == target)
            target = null;

        int enemyId = enemy.GetComponent<EnemyScript>().id;
        MonsterStruct toRemove = null;
        foreach (MonsterStruct mon in currentFloor.entities["monster"])
        {
            if (mon.id == enemyId)
            {
                toRemove = mon;
                break;
            }
        }
        currentFloor.entities["monster"].Remove(toRemove);

        EnemyScript script = enemy.GetComponent<EnemyScript>();
        int temp = UnityEngine.Random.Range(0, 100);
        print(temp);
        if (temp <= script.ITEM_DROP_CHANCE)
        {
            int dropCount = 1;
            List<KeyValuePair<int, int>> drops = ItemHandler.RandomizeItem(theme.dropTable, theme.baseItemLevel + floorManager.currentFloor, dropCount);
            GetItems(drops);
        }
        
        ImmortalObject.playerGold += script.goldDrop;
    }

    public void PropDestroy(GameObject prop)
    {
        int objId = prop.GetComponent<Prop>().id;
        PropStruct toRemove = null;
        foreach (PropStruct p in currentFloor.entities["prop"])
        {
            if (p.id == objId)
            {
                toRemove = p;
                break;
            }
        }
        currentFloor.entities["prop"].Remove(toRemove);
        currentFloor.referenceMap[toRemove.pos.z, toRemove.pos.x].Remove(prop);
    }

    public void GetItems(List<KeyValuePair<int, int>> items)
    {
        foreach (KeyValuePair<int, int> item in items)
        {
            print("Got " + "(id:" + item.Key + ") " +ImmortalObject.fullItemList[item.Key + ""]["ItemName"]);
            inventory.AddItemToInventory(item, 1); //still support only one drop per time

        }
    }

    bool movingDown;
    public void ChangeFloorDown()
    {
        print("change floor down con");
        Camera.main.GetComponent<SFXManager>().PlayChangeFloorSound();
        gameRunning = false;
        fow.StopFogOfWar();
        if (currentFloor != null)
            currentFloor.fog = fow.fog;

        for (int i = 0; i < enemies.Count; i++) 
        {
            currentFloor.entities["monster"][i].pos = IntVector3.ToIntVector3(enemies[i].transform.position);
        }

        IntVector3 currentPos = IntVector3.ToIntVector3(player.transform.position);
        GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().DynamicNodeUpdate(currentPos.x, currentPos.z, true);
        currentFloor.walkable[currentPos.z, currentPos.x] = true;

        floorManager.ChangeFloorDown();
        movingDown = true;
    }

    public void ChangeFloorUp()
    {
        print("change floor up game con");
        Camera.main.GetComponent<SFXManager>().PlayChangeFloorSound();
        gameRunning = false;
        fow.StopFogOfWar();
        if (floorManager.currentFloor == 0)
        {
            print("end game");
            EndGame(0);
            return;
        }
        
        if (currentFloor != null)
            currentFloor.fog = fow.fog;

        for (int i = 0; i < enemies.Count; i++)
        {
            currentFloor.entities["monster"][i].pos = IntVector3.ToIntVector3(enemies[i].transform.position);
        }

        IntVector3 currentPos = IntVector3.ToIntVector3(player.transform.position);
        GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().DynamicNodeUpdate(currentPos.x, currentPos.z, true);
        currentFloor.walkable[currentPos.z, currentPos.x] = true;

        floorManager.ChangeFloorUp();
        movingDown = false;
    }

    private void UpdateUI()
    {
        ui.UpdateInfoPanel();
        ui.SetProcessingTurnIcon(processingTurn || timeSinceAction < TURN_DELAY);
        ui.SetFloorNumber(floorManager.currentFloor);
    }
}
