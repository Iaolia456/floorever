﻿using UnityEngine;
using System.Collections;

public class HideEnemy : MonoBehaviour {
    SkinnedMeshRenderer[] renderers;

	void Start () {
        renderers = GetComponentsInChildren<SkinnedMeshRenderer>();
	}
	
    void Update () {
	
	}

    public void Hide()
    {
        foreach (var v in renderers)
            v.enabled = false;
    }

    public void Show()
    {
        foreach (var v in renderers)
            v.enabled = true;
    }
}
