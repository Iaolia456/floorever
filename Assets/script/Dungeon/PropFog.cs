﻿using UnityEngine;
using System.Collections;

public class PropFog : MonoBehaviour, IFoggable {
    public GameObject[] model; //0 = explored, 1 = visible

    void Start () {
	
	}

    public void SetFogOfWar(int type)
    {
        if (type == FogOfWar.VISIBLE)
        {
            model[0].SetActive(false);
            model[1].SetActive(true);
        }
        else if (type == FogOfWar.EXPLORED)
        {
            model[0].SetActive(true);
            model[1].SetActive(false);
        }
        else
        {
            model[0].SetActive(false);
            model[1].SetActive(false);
        }
    }
}
