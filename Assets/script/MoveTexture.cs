﻿using UnityEngine;
using System.Collections;

public class MoveTexture : MonoBehaviour {

    Vector2 uvOffset = Vector2.zero;
    public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2(1f, 0.0f);
    public string textureName = "_MainTex";
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        uvOffset += (uvAnimationRate * Time.deltaTime*0.1f);
        if (renderer.enabled)
        {
            renderer.materials[materialIndex].SetTextureOffset(textureName, uvOffset);
        }
	}
}
