﻿using UnityEngine;
using System.Collections;

public class DestroyObject : MonoBehaviour {
    public float time;

	void Start () {
        Destroy(gameObject, time);
	}
}
