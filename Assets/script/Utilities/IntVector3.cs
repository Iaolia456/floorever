﻿using UnityEngine;
using System.Collections;

public class IntVector3 {
    public int x;
    public int y;
    public int z;

    public IntVector3(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public IntVector3(Vector3 v)
    {
        this.x = (int)v.x;
        this.y = (int)v.y;
        this.z = (int)v.z;
    }

    public static int Distance(IntVector3 a, IntVector3 b)
    {
        Vector3 av = new Vector3(a.x, a.y, a.z);
        Vector3 bv = new Vector3(b.x, b.y, b.z);
        return (int)Vector3.Distance(av, bv);
    }

    public static IntVector3 ToIntVector3(Vector3 v)
    {
        return new IntVector3((int)v.x, (int)v.y, (int)v.z);
    }

    public Vector3 ToVector3()
    {
        return new Vector3(x, y, z);
    }
}
