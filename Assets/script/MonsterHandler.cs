﻿using System;
using System.Collections.Generic;
public static class MonsterHandler{

    private static RESTfulHandler RESTFUL_SERVICE;
    private static string message;
    private static Boolean responseFlag;
    private static Action<string> globalCallBack;
    static MonsterHandler()
    {
        message = null;
        responseFlag = false;
        globalCallBack = setMessage;
        RESTFUL_SERVICE = new RESTfulHandler(globalCallBack);
    }

    public static void setMessage(string response)
    {
        message = response;
        responseFlag = true;
    }

    public static string getMessage()
    {
        if (responseFlag)
        {
            responseFlag = false;
            return message;
        }
        else return null;
    }

    public static void addMonster(int monsterID, string monsterName, int HPBase = 0,float HPGrowth = 0, int ATKBase = 0, float ATKGrowth = 0, int DEFBase = 0, float DEFGrowth = 0, int EXPBase = 0, float EXPGrowth = 0, int GoldBase = 0, float GoldGrowth = 0)
    {
        
        responseFlag = false;
        Dictionary<string,object> jsonOBJ = new Dictionary<string,object>();
        
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERID               ,monsterID);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERNAME             ,monsterName);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPBASE, HPBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPGROWTH, HPGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKBASE, ATKBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKGROWTH, ATKGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFBASE, DEFBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFGROWTH, DEFGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPBASE, EXPBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPGROWTH, EXPGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDBASE, GoldBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDGROWTH, GoldGrowth);

        RESTFUL_SERVICE.WebReqPOSTclassAsync(DATABASE_CONSTANTS.MONSTERDATA_CLASSNAME,jsonOBJ);

    }


    public static void getMonster(int monsterID)
    {
        responseFlag = false;
        Dictionary<string, object> queryParam = new Dictionary<string, object>();
        queryParam.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERID               ,monsterID);
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.MONSTERDATA_CLASSNAME,MiniJSON.Json.Serialize(queryParam));
    }

    public static void getMonsterList()
    {
        responseFlag = false;
        //TO BE : TOWN LEVEL CONSTRAIN
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.MONSTERDATA_CLASSNAME);
    }

    internal static void deleteMonster(string objectID)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqDELETEclass(DATABASE_CONSTANTS.MONSTERDATA_CLASSNAME, objectID);
    }

    internal static void UpdateMonster(string objectID, int monsterID, string monsterName, int HPBase = 0, float HPGrowth = 0, int ATKBase = 0, float ATKGrowth = 0, int DEFBase = 0, float DEFGrowth = 0, int EXPBase = 0, float EXPGrowth = 0, int GoldBase = 0, float GoldGrowth = 0)
    {
        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERID, monsterID);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERNAME, monsterName);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPBASE, HPBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_HPGROWTH, HPGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKBASE, ATKBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_ATKGROWTH, ATKGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFBASE, DEFBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_DEFGROWTH, DEFGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPBASE, EXPBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_EXPGROWTH, EXPGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDBASE, GoldBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.MONSTERDATA_COLUMN_GOLDGROWTH, GoldGrowth);

        RESTFUL_SERVICE.WebReqPUTclassAsync(DATABASE_CONSTANTS.MONSTERDATA_CLASSNAME,objectID,jsonOBJ);
    }

    public static Dictionary<int,int> RandomizeMonster(Dictionary<int, int> DropableMonsters, int monsterLevel = 1,int DropAmount = 1)
    {
        List<int> MonstersPool = new List<int>();
        foreach (KeyValuePair<int, int> anMonster in DropableMonsters)
        {
            for (int i = 0; i < anMonster.Value; i++)
            {
                MonstersPool.Add(anMonster.Key);
            }
        }

        Dictionary<int,int> dropList = new Dictionary<int,int>();
        for (int i = 0; i < DropAmount; i++ )
        {
            dropList.Add(MonstersPool[new System.Random().Next(0, MonstersPool.Count - 1)],monsterLevel);
        }
            return dropList;
    }


}
