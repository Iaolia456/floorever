﻿using System;
using System.Collections.Generic;
using MiniJSON;
public static class DeviceIDUserHandler
{

    private static RESTfulHandler RESTFUL_SERVICE;
    private static string message;
    private static Boolean responseFlag;
    private static Action<string> globalCallBack;
    static DeviceIDUserHandler()
    {
        message = null;
        responseFlag = false;
        globalCallBack = setMessage;
        RESTFUL_SERVICE = new RESTfulHandler(globalCallBack);
    }

    public static void setMessage(string response)
    {
        message = response;
        responseFlag = true;
    }

    public static string getMessage()
    {
        if (responseFlag)
        {
            responseFlag = false;
            return message;
        }
        else return null;
    }

    public static void addUser(string deviceID,string posessedConsumables, string inventory, int gold)
    {

        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_DEVICEID, deviceID);
        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_CONSUMABLE, posessedConsumables);
        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_INVENTORY, inventory);
        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_GOLD, gold);

        RESTFUL_SERVICE.WebReqPOSTclassAsync(DATABASE_CONSTANTS.DEVICEID_CLASSNAME,jsonOBJ);

    }


    public static void getUser(string deviceID)
    {
        responseFlag = false;
        Dictionary<string, object> queryParam = new Dictionary<string, object>();
        queryParam.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_DEVICEID, deviceID);
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.DEVICEID_CLASSNAME, MiniJSON.Json.Serialize(queryParam));
    }

    public static void login(string username, string password)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqLoginUserAsync(username, password);
    }

    

    /*internal static void deleteUser(string objectID)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqDELETEclass(DATABASE_CONSTANTS.USERDATA_CLASSNAME, objectID);
    
    }*/


    public static void UpdateUser(string objectID, string deviceID, string posessedConsumables, string inventory, int gold)
    {
        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_CONSUMABLE, posessedConsumables);
        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_INVENTORY, inventory);
        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_GOLD, gold);

        RESTFUL_SERVICE.WebReqPUTclassAsync(DATABASE_CONSTANTS.DEVICEID_CLASSNAME, objectID, jsonOBJ);
    }

    /*public static Dictionary<int, int> RandomizeUser(Dictionary<int, int> DropableUsers, int userLevel = 1, int DropAmount = 1)
    {
        List<int> UsersPool = new List<int>();
        foreach (KeyValuePair<int, int> anUser in DropableUsers)
        {
            for (int i = 0; i < anUser.Value; i++)
            {
                UsersPool.Add(anUser.Key);
            }
        }

        Dictionary<int, int> dropList = new Dictionary<int, int>();
        for (int i = 0; i < DropAmount; i++)
        {
            dropList.Add(UsersPool[new System.Random().Next(0, UsersPool.Count - 1)], userLevel);
        }
        return dropList;
    }*/

    public static void EasyUpdateUser()
    {
        UnityEngine.Debug.Log("Easy updating player " + ImmortalObject.DeviceIDUserObjectID + " device id " + UnityEngine.SystemInfo.deviceUniqueIdentifier);
        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_CONSUMABLE, ImmortalObject.playerConsumable + "");
        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_INVENTORY, Json.Serialize(ImmortalObject.playerInventory));
        jsonOBJ.Add(DATABASE_CONSTANTS.DEVICEID_COLUMN_GOLD, ImmortalObject.playerGold);

        RESTFUL_SERVICE.WebReqPUTclassAsync(DATABASE_CONSTANTS.DEVICEID_CLASSNAME, ImmortalObject.DeviceIDUserObjectID, jsonOBJ);
    }
}
