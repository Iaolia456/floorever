﻿using UnityEngine;
using System.Collections;
using Parse;
using System;
using System.Net;
using System.Text;
using System.IO;
using MiniJSON;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Threading;



/* 
 * Controller for RESTful http requests.
 * CONSTRUCTOR PARAMETER
 * Action<string> callBackMethod - Global Callback method to handle HTTP Response.
 * 
 * METHODS:
 * 
 * WebReqPOST/GET/PUT/DELETE 
 * - Send request to Defined Server (This case Floorever Server) with predefined consts.
 * - Non-Postfix are Synchronous method, -Async postfix are Asynchronous.
 * - PUT,GET will have 2 variation that receive either sessionToken or not.
 * 
 * WebPOST/GET/PUT/DELETE Class
 * - Build on top of WebReq method groups.
 * - 2015/09/04 Still in development.
 * 
 * WebReq Regis/Login/Update/GetCurrent/ChangePassword User
 * - Build on top of WebReq method groups.
 * - Have both Sync & Async version. (Sync version is underdeveloped and highly advise to avoid using them)
 * 
 * REQUIRED FIELDS
 * 
 * WebReqs
 * string urlextension - (Please look in https://www.parse.com/docs/rest#general)
 * string payload - Json Serialized string
 * 
 * WebReqRegisUser
 * string username
 * string password
 * string personalData - Json Serialized string, usually blank at time of user creation.
 * 
 * WebReqLoginUser
 * string username
 * string password
 * 
 * WebReqGetCurrentUser
 * string sessionToken - Come along with Login response.
 * 
 * WebReqUpdateUser
 * string objectID     - Come along with Login response.
 * string sessionToken - Come along with Login response.
 * string personalData - Json Serialized string, Data to be update.
 * 
 * WebReqChangePasswordUser
 * string password
 * string objectID     - Come along with Login response.
 * string sessionToken - Come along with Login response.
 * string personalData - Json Serialized string, Data to be update.
 * 
 * @Author : Lattasit Haritaworn , 5410545061
 * 
 */
public class RESTfulHandler{

    private static Action<string>  InvokeResponseCallback;
    private const int       TIMEOUT_CONFIG = 5000;
    private const string    BASE_URL = "https://api.parse.com/1";
    private const string    CONTENT_TYPE = "application/json";
    private const string    APP_KEY_HEADER = "X-Parse-Application-Id";
    private const string    APP_KEY_BODY = "nE2kiRGnR1mt7sUGD5oCIUoCim2DQjuO8tUQSbB9";
    private const string    RESTFUL_KEY_HEADER = "X-Parse-REST-API-Key";
    private const string    RESTFUL_KEY_BODY = "ZXTngZYFsL31Q47mwD6ojWeqjCh9w7HwxVbqQxq6";
    private const string    SESSION_TOKEN_HEADER = "X-Parse-Session-Token";

    public RESTfulHandler(Action<string> callbackMethod)
    {
        InvokeResponseCallback = callbackMethod;
    }
   
    // Use this for initialization
    void Start()
    {
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
    }

    

    /*
     * Set up callback reference.
     * */
    public void setResponseCallbackMethod(Action<string> callbackMethod)
    {
        InvokeResponseCallback = callbackMethod;
    }

    void Update()
    {

    }

    /*
     * Async callback method invoked after beginGetResponse method
     * */
    private static void GetResponseCallback(IAsyncResult asynchronousResult)
    {
        HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
        HttpWebResponse response = null;
        
        //Handle Exceptions
        //Expected exceptions such as 404(Wrong URL), 101(Login error)
        try
        {
            
            response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
        }
        catch (WebException e)
        {
            //Use WebException as response
            Debug.Log("Got Exception : " + e.Message);
            response = e.Response as HttpWebResponse;
        }
        

        //Read response string
        Stream streamResponse = response.GetResponseStream();
        StreamReader streamRead = new StreamReader(streamResponse);
        string responseString = streamRead.ReadToEnd();

        // Close the stream object
        streamResponse.Close();
        streamRead.Close();

        // Release the HttpWebResponse
        response.Close();
        Debug.Log(responseString);
        InvokeResponseCallback(responseString);
    }

    /*
     * Include sessionID in case of updating user data
     * Ex.Update User Inventory
     * */
    private HttpWebRequest setupParseRestfulRequestHeader(HttpWebRequest request, string method, string sessionID = null)
    {
        request.Timeout = TIMEOUT_CONFIG;
        request.Method = method;
        request.ContentType = CONTENT_TYPE;
        request.Headers.Add(APP_KEY_HEADER, APP_KEY_BODY);
        request.Headers.Add(RESTFUL_KEY_HEADER, RESTFUL_KEY_BODY);
        if(sessionID != null)request.Headers.Add(SESSION_TOKEN_HEADER, sessionID);
        return request;
    }

    private HttpWebRequest encodePayload(HttpWebRequest request, string payload)
    {
        //Encode Dictionary Payload
        var encodedData = Encoding.ASCII.GetBytes(payload);
        request.ContentLength = encodedData.Length;
        Debug.Log("Payload : " + payload);
        using (var stream = request.GetRequestStream())
        {
            stream.Write(encodedData, 0, encodedData.Length);
        }

        return request;
    }

    private string WebReqPOST(string URLextension, string payload)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "POST");
        request = encodePayload(request,payload);

        //Send Payload
        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
        
        Debug.Log(responseString);

        return responseString;
    }
    private void WebReqPOSTAsync(string URLextension, string payload, string sessionToken = null)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "POST",sessionToken);
        request = encodePayload(request, payload);

        //Send Payload
        request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
    }

    private string WebReqPUT(string URLextension, string payload, string sessionToken)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "PUT",sessionToken);
        request = encodePayload(request, payload);
        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }

    private string WebReqPUT(string URLextension, string payload)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "PUT");
        request = encodePayload(request, payload);

        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }

    private void WebReqPUTAsync(string URLextension, string payload, string sessionToken = null)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "PUT",sessionToken);
        request = encodePayload(request, payload);

        //Send Payload
        request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
    }


    private string WebReqDELETE(string URLextension, string payload, string sessionToken)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "DELETE");
        request.Headers.Add("X-Parse-Session-Token", sessionToken);
        request = encodePayload(request, payload);

        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }

    private string WebReqDELETE(string URLextension, string payload)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "DELETE");
        request = encodePayload(request, payload);

        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }

    private void WebReqDELETEAsync(string URLextension, string payload = null, string sessionToken = null)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "DELETE",sessionToken);
        if(payload != null) request = encodePayload(request, payload);

        //Send Payload
        request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
    }

    

    private string WebReqGET(string URLextension)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "GET");


        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }
    private string WebReqGET(string URLextension, string sessionToken)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "GET",sessionToken);


        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }


    private void WebReqGETAsync(string URLextension,string payload = null, string sessionToken = null)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + URLextension);
        request = setupParseRestfulRequestHeader(request, "GET",sessionToken);
        if (payload != null) encodePayload(request,payload);


        //Send Payload

        request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
    }

    
    public void WebReqGETclass(string className)
    {
        WebReqGETAsync("/classes/" + className);
    }
    
    public void WebReqGETclass(string className, string whereExtension)
    {
        WebReqGETAsync("/classes/" + className + "?where=" + whereExtension);
    }

    public void WebReqGETclass(string className, Dictionary<string,object> whereExtension)
    {
        WebReqGETAsync("/classes/" + className + "?where=" + Json.Serialize(whereExtension));
    }


    public void WebReqDELETEclass(string className, string objectID)
    {
        WebReqDELETEAsync("/classes/" + className + "/" + objectID);
    }

    public string WebReqPOSTclass(string className, Dictionary<string,object> miniJson)
    {
        return WebReqPOST("/classes/" + className, Json.Serialize(miniJson));
    }


    public void WebReqPOSTclassAsync(string className, Dictionary<string,object> miniJson)
    {
        WebReqPOSTAsync("/classes/" + className, Json.Serialize(miniJson));
    }

    public string WebReqPOSTclass(string className, string miniJson)
    {
        return WebReqPOST("/classes/" + className, miniJson);
    }

    public string WebReqPUTclass(string className, string objectID, Dictionary<string,object> miniJson)
    {
        return WebReqPUT("/classes/" + className + "/" + objectID, Json.Serialize(miniJson));
    }
    public string WebReqPUTclass(string className, string objectID, string miniJson)
    {
        return WebReqPUT("/classes/" + className + "/" + objectID, miniJson);
    }

    public void WebReqPUTclassAsync(string className, string objectID, Dictionary<string, object> miniJson)
    {
        WebReqPUTAsync("/classes/" + className + "/" + objectID, Json.Serialize(miniJson));
    }

    public void WebReqPUTclassAsync(string className, string objectID, string miniJson)
    {
        WebReqPUTAsync("/classes/" + className + "/" + objectID, miniJson);
    }

    public string WebReqRegisUser(string username, string password, Dictionary<string,object> personalData)
    {
        personalData.Add("username",username);
        personalData.Add("password",password);
        return WebReqPOST("", Json.Serialize(personalData));
    }

    public string WebReqRegisUser(string username, string password, string personalData)
    {
        Dictionary<string, object> tempDict = Json.Deserialize(personalData) as Dictionary<string,object>;
        tempDict.Add("username", username);
        tempDict.Add("password", password);
        return WebReqPOST("/users", Json.Serialize(tempDict));
    }


    public string WebReqLoginUser(string username, string password)
    {
        return WebReqGET("/login/?username=" + username + "&password=" + password);
    }

    public string WebReqUpdateUser(string username, string password, string objectID, string sessionToken, Dictionary<string,object> personalData)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + "/users/" + objectID);
        var METHOD_CONFIG = "PUT";
        request.Timeout = TIMEOUT_CONFIG;
        request.Method = METHOD_CONFIG;
        request.ContentType = CONTENT_TYPE;
        request.Headers.Add(APP_KEY_HEADER, APP_KEY_BODY);
        request.Headers.Add(RESTFUL_KEY_HEADER, RESTFUL_KEY_BODY);
        request.Headers.Add("X-Parse-Session-Token", sessionToken);

        //Encode Dictionary Payload
        var encodedData = Encoding.ASCII.GetBytes(Json.Serialize(personalData));
        request.ContentLength = encodedData.Length;

        using (var stream = request.GetRequestStream())
        {
            stream.Write(encodedData, 0, encodedData.Length);
        }

        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }

    public string WebReqUpdateUser(string username, string password, string objectID, string sessionToken, string personalData)
    {
        //Config WebRequest
        var request = (HttpWebRequest)WebRequest.Create(BASE_URL + "/users/" + objectID);
        var METHOD_CONFIG = "PUT";
        request.Timeout = TIMEOUT_CONFIG;
        request.Method = METHOD_CONFIG;
        request.ContentType = CONTENT_TYPE;
        request.Headers.Add(APP_KEY_HEADER, APP_KEY_BODY);
        request.Headers.Add(RESTFUL_KEY_HEADER, RESTFUL_KEY_BODY);
        request.Headers.Add("X-Parse-Session-Token", sessionToken);

        //Encode Dictionary Payload
        var encodedData = Encoding.ASCII.GetBytes(personalData);
        request.ContentLength = encodedData.Length;

        using (var stream = request.GetRequestStream())
        {
            stream.Write(encodedData, 0, encodedData.Length);
        }

        var response = (HttpWebResponse)request.GetResponse();
        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

        Debug.Log(responseString);

        return responseString;
    }
    public void WebReqRegisUserAsync(string username, string password, string personalData)
    {
        Dictionary<string, object> tempDict = Json.Deserialize(personalData) as Dictionary<string, object>;
        tempDict.Add("username", username);
        tempDict.Add("password", password);
        WebReqPOSTAsync("/users", Json.Serialize(tempDict));
    }

    public void WebReqRegisUserAsync(string username, string password, Dictionary<string,object> personalData)
    {
        personalData.Add("username", username);
        personalData.Add("password", password);
        WebReqPOSTAsync("/users", Json.Serialize(personalData));
    }


    public void WebReqUpdateUserAsync(string objectID, string sessionToken, string personalData)
    {
        WebReqPUTAsync("/users/" + objectID, personalData,sessionToken);
    }

    public void WebReqUpdateUserAsync(string objectID, string sessionToken, Dictionary<string,object> personalData)
    {

        WebReqPUTAsync("/users/" + objectID, Json.Serialize(personalData),sessionToken);
    }

    public void WebReqChangePasswordUserAsync(string password, string objectID, string sessionToken)
    {
        Dictionary<string, object> tempDict = new Dictionary<string, object>();
        tempDict.Add("password", password);
        WebReqPUTAsync("/users/" + objectID, Json.Serialize(tempDict), sessionToken);
    }

    public void WebReqLoginUserAsync(string username, string password)
    {
        WebReqGETAsync("/login/?username=" + username + "&password=" + password);
    }

    public void WebReqGetCurrentUserAsync(string sessionToken)
    {
        WebReqGETAsync("/users/me",sessionToken : sessionToken);
    }

    
    
}
