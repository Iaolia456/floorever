﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using MiniJSON;

public class PanelSetting : MonoBehaviour
{

    string name;
    Dictionary<string, object> ItemData;
    public int itemLevel;
    public int itemType;
    public bool equipped;
    public Text uiText;
    public GameObject icon;
    public TownUIUpdate uiUpdate;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setItemName(string str)
    {
        //gameObject.GetComponentInChildren<Text>().text = str;
        uiText.text = str;
        name = str;
    }

    public void setItemData(Dictionary<string, object> ItemData, int itemLevel = 0)
    {
        this.ItemData = ItemData;
        this.itemLevel = itemLevel;
        setItemName(Convert.ToString(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMNAME]));
        if (Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE]) == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE)
        setItemName(Convert.ToString(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMNAME]) + " X " + ImmortalObject.playerConsumable);
    }

    public void setTownUIUpdate(TownUIUpdate townUI)
    {
        this.uiUpdate = townUI;
    }

    public void setIcon(Sprite sprite)
    {
        icon.GetComponent<Image>().overrideSprite = sprite;

    }

    public void setButtonType(int itemType)
    {
        this.itemType = itemType;
    }

    public void ClickButton()
    {
        Debug.Log("itemType : " + itemType);
        print("Name: " + name + "\nTag: " + itemType);
        if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_WEAPON)
            WeaponShopButtonClick();
        else if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_SHIRT || itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_PANT || itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_SHIELD)
            ArmorShopButtonClick();
        else if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_CONSUMABLE)
            ConsumableShopButtonClick();
        else if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_PANT || itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_SHIELD || itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_SHIRT || itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_WEAPON)
        {
            ShopButtonClick();
            if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_PANT) UpdateCurrentEquip(3);
            if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_WEAPON) UpdateCurrentEquip(0);
            if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_SHIRT) UpdateCurrentEquip(2);
            if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_SHIELD) UpdateCurrentEquip(1);
            Debug.Log(Json.Serialize(ImmortalObject.playerEquipments));
            uiUpdate.SetPrice((Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]) * 4) / 10);
        }else if (itemType == DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_CONSUMABLE)
        {
            uiUpdate.SetPrice((Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]) * 4) / 10);
        }

    }


    // ▂▃▄▅▆▇█▓▒░   Weapon_Shop     ░▒▓█▇▆▅▄▃▂

    public void WeaponShopButtonClick()
    {
        ShopButtonClick();
    }



    // ▂▃▄▅▆▇█▓▒░   Armor_Shop     ░▒▓█▇▆▅▄▃▂

    public void ArmorShopButtonClick()
    {
        ShopButtonClick();
    }


    // ▂▃▄▅▆▇█▓▒░   Consumable_Shop     ░▒▓█▇▆▅▄▃▂

    public void ConsumableShopButtonClick()
    {
        ShopButtonClick();
    }


    public void UpdateCurrentEquip(int Itemtype)
    {
        int itemID = ImmortalObject.playerEquipments[Itemtype];
        int itemLevel = ImmortalObject.playerEquipmentsLevel[Itemtype];
        string atk = Convert.ToInt32(ImmortalObject.fullItemList[itemID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKBASE]) + " + " + (Convert.ToInt32(ImmortalObject.fullItemList[itemID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKGROWTH]) * itemLevel);
        string def = Convert.ToInt32(ImmortalObject.fullItemList[itemID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFBASE]) + " + " + (Convert.ToInt32(ImmortalObject.fullItemList[itemID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFGROWTH]) * itemLevel);
        string hp = Convert.ToInt32(ImmortalObject.fullItemList[itemID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPBASE]) + " + " + (Convert.ToInt32(ImmortalObject.fullItemList[itemID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPGROWTH]) * itemLevel);
        int rarity = Convert.ToInt32(ImmortalObject.fullItemList[itemID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_RARITY]);
        uiUpdate.SetCurrent(atk, def, hp, rarity, itemID, itemLevel);
        uiUpdate.SetPrice(Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]));
    }

    public void ShopButtonClick()
    {
        string atk = ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKBASE] + " + " + (Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKGROWTH]) * itemLevel);
        string def = ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFBASE] + " + " + (Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFGROWTH]) * itemLevel);
        string hp = ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPBASE] + " + " + (Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPGROWTH]) * itemLevel);
        int rarity = Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_RARITY]);
        int itemID = Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID]);
        uiUpdate.SetSelecting(atk, def, hp, rarity, itemID,itemLevel);
        uiUpdate.SetPrice(Convert.ToInt32(ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]));
    }

    public void addItemToInventory()
    {
        if (ImmortalObject.playerGold >= Convert.ToInt32(this.ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]))
        {
            ImmortalObject.playerGold = ImmortalObject.playerGold - Convert.ToInt32(this.ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]);
            ImmortalObject.playerInventory.Add(new KeyValuePair<int, int>(Convert.ToInt32(this.ItemData[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID]), 0));
        }
        updateUI();
        
    }

    public void updateUI()
    {
        Debug.Log(uiUpdate);
        uiUpdate.SetMoney(ImmortalObject.playerGold);
    }


}
    
