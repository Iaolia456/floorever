﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Net;
using System;
using MiniJSON;

public class ShopBase : MonoBehaviour {


   

    //UI Prefab
    public GameObject itemPrefab;

    //UI
    public GameObject showMoney;
    public GameObject showCurrent;
    public GameObject showSelecting;
    public GameObject showResource;
    public GameObject shopUI;
    public GameObject WAShop;
    public GameObject weaponShopContent;
    public GameObject armorShopContent;

    public GameObject ConsumableShop;
    public GameObject ConsumableShopContent;

    public GameObject inventory;
    public GameObject inventoryTab1;
    public GameObject inventoryTab2;
    public GameObject inventoryTab3;
    public GameObject inventoryWeapon;
    public GameObject inventoryWeaponContent;
    public GameObject inventoryArmor;
    public GameObject inventoryArmorContent;
    public GameObject inventoryConsumable;
    public GameObject inventoryConsumableContent;

    public GameObject craft;
    public GameObject craftContent;

    public GameObject upgrade;
    public GameObject upgradeContent;

    private RESTfulHandler httpHandler;

    public Dictionary<string, Dictionary<string, object>> fullItemList;
    public List<KeyValuePair<int, int>> playerInventory;
    public int playerGold;
    public int playerConsumable;
    public Sprite[] spriteList;

    public TownUIUpdate townUI;
    

    
	void Start () {
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        fullItemList = ImmortalObject.fullItemList;
        playerInventory = ImmortalObject.playerInventory;
        playerGold = ImmortalObject.playerGold;
        
        playerConsumable = ImmortalObject.playerConsumable;
        spriteList = Resources.LoadAll<Sprite>("UI/Item_Icon/Icon_Set");
        townUI = gameObject.GetComponent<TownUIUpdate>();
        GameObject g = Instantiate(itemPrefab) as GameObject;
        townUI.SetMoney(ImmortalObject.playerGold);


        addIcons();
        CloseAll();
        //AddDataDebug();
        //queryShopItems();
        
        foreach(KeyValuePair<string,Dictionary<string,object>> aMonster in ImmortalObject.fullMonsterList)
        {
            Debug.Log(Json.Serialize(aMonster.Value));
        }


        

	}

 
    // Update is called once per frame
    void Update()
    {
        // NOT NOW , PUT WHEN SHOP INTERACTION IS IMPLEMENTED
        /*
        string response = ItemHandler.getMessage();
        if (response != null)
        {
            ParseResponse(response);
        }
        response = UserHandler.getMessage();
        if (response != null)
        {
            ParseResponse(response);
        }
        */
            
    }

    public void addIcons()
    {

        foreach(KeyValuePair<string,Dictionary<string,object>> anItem in fullItemList)
        {
            if (Convert.ToInt32(anItem.Value[DATABASE_CONSTANTS.ITEMDATA_COLUMN_BUYABLE]) == 1)
            {
                GameObject g = Instantiate(itemPrefab) as GameObject;
                int itemType = Convert.ToInt32(anItem.Value[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE]);
                if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_WEAPON)
                {
                    g.transform.SetParent(weaponShopContent.transform, false);
                    g.transform.position = weaponShopContent.transform.position;
                    g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_WEAPON);

                }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIRT)
                {
                    g.transform.SetParent(armorShopContent.transform, false);
                    g.transform.position = armorShopContent.transform.position;
                    g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_SHIRT);

                }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_PANT)
                {
                    g.transform.SetParent(armorShopContent.transform, false);
                    g.transform.position = armorShopContent.transform.position;
                    g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_PANT);

                }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIELD)
                {
                    g.transform.SetParent(armorShopContent.transform, false);
                    g.transform.position = armorShopContent.transform.position;
                    g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_SHIELD);

                }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE)
                {
                    g.transform.SetParent(ConsumableShopContent.transform, false);
                    g.transform.position = ConsumableShopContent.transform.position;
                    g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_SHOP_CONSUMABLE);
                }

                g.transform.localScale = new Vector3(1, 1, 1);
                g.GetComponent<PanelSetting>().setItemData(anItem.Value);
                g.GetComponent<PanelSetting>().setTownUIUpdate(townUI);
                g.GetComponent<PanelSetting>().setIcon(spriteList[Convert.ToInt32(anItem.Value[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID]) - 1]);
            }
        }

        foreach(KeyValuePair<int,int> anItem in playerInventory)
        {
            GameObject g = Instantiate(itemPrefab) as GameObject;
            int itemType = System.Convert.ToInt32(fullItemList[anItem.Key.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE]);
            if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_WEAPON)
            {
                g.transform.SetParent(inventoryWeaponContent.transform,false);
                g.transform.position = inventoryWeaponContent.transform.position;
                g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_WEAPON);

            }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIRT)
            {
                g.transform.SetParent(inventoryArmorContent.transform, false);
                g.transform.position = inventoryArmorContent.transform.position;
                g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_SHIRT);

            }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_PANT)
            {
                g.transform.SetParent(inventoryArmorContent.transform, false);
                g.transform.position = inventoryArmorContent.transform.position;
                g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_PANT);

            }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIELD)
            {
                g.transform.SetParent(inventoryArmorContent.transform, false);
                g.transform.position = inventoryArmorContent.transform.position;
                g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_SHIELD);

            }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE)
            {
                g.transform.SetParent(inventoryConsumableContent.transform, false);
                g.transform.position = inventoryConsumableContent.transform.position;
                g.GetComponent<PanelSetting>().setButtonType(DATABASE_CONSTANTS.UIBUTTON_TYPE_INVENTORY_CONSUMABLE);
            }

            g.transform.localScale = new Vector3(1, 1, 1);
            g.GetComponent<PanelSetting>().setItemData(fullItemList[anItem.Key.ToString()], anItem.Value);
            g.GetComponent<PanelSetting>().setTownUIUpdate(townUI);
            g.GetComponent<PanelSetting>().setIcon(spriteList[Convert.ToInt32(fullItemList[anItem.Key.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID]) - 1]);
        }


        
    }
    
    
    private void queryShopItems()
    {

        ItemHandler.getShopItemList();
    }

    private void queryInventoryItems(string username, string password)
    {
        UserHandler.login(username, password);
    }

    public void CloseAll()
    {
        showMoney.SetActive(false);
        showCurrent.SetActive(false);
        showSelecting.SetActive(false);
        shopUI.SetActive(false);
        weaponShopContent.SetActive(false);
        armorShopContent.SetActive(false);
        WAShop.SetActive(false);
        ConsumableShop.SetActive(false);
        inventory.SetActive(false);
        inventoryWeapon.SetActive(false);
        inventoryArmor.SetActive(false);
        craft.SetActive(false);
        upgrade.SetActive(false);
        showResource.SetActive(false);

        inventoryConsumable.SetActive(false);   
        for(int i = weaponShopContent.transform.childCount - 1 ; i >= 0 ; i--)
        {
            GameObject.Destroy(weaponShopContent.transform.GetChild(i).gameObject);
        }
        for (int i = armorShopContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(armorShopContent.transform.GetChild(i).gameObject);
        }
        for (int i = ConsumableShopContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(ConsumableShopContent.transform.GetChild(i).gameObject);
        }
        for (int i = inventoryWeaponContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(inventoryWeaponContent.transform.GetChild(i).gameObject);
        }
        for (int i = inventoryArmorContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(inventoryArmorContent.transform.GetChild(i).gameObject);
        }
        for (int i = inventoryConsumableContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(inventoryConsumableContent.transform.GetChild(i).gameObject);
        }
        for (int i = inventoryConsumableContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(inventoryConsumableContent.transform.GetChild(i).gameObject);
        }
        for (int i = inventoryConsumableContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(inventoryConsumableContent.transform.GetChild(i).gameObject);
        }
        for (int i = inventoryConsumableContent.transform.childCount - 1; i >= 0; i--)
        {
            GameObject.Destroy(inventoryConsumableContent.transform.GetChild(i).gameObject);
        }
    }

    public void OpenWeaponShop()
    {

        showMoney.SetActive(true);
        showCurrent.SetActive(true);
        showSelecting.SetActive(true);
        shopUI.SetActive(true);
        WAShop.SetActive(true);
        weaponShopContent.SetActive(true);
        addIcons();
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_BUY);
    }

    public void OpenArmorShop()
    {
        showMoney.SetActive(true);
        showCurrent.SetActive(true);
        showSelecting.SetActive(true);
        shopUI.SetActive(true);
        WAShop.SetActive(true);
        armorShopContent.SetActive(true);
        addIcons();
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_BUY);
    }

    public void OpenConsumbleShop()
    {
        showMoney.SetActive(true);
        shopUI.SetActive(true);
        ConsumableShop.SetActive(true);
        addIcons();
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_CONSUMABLE_BUY);
    }

    public void OpenInventory()
    {
        showMoney.SetActive(true);
        showCurrent.SetActive(true);
        showSelecting.SetActive(true);
        shopUI.SetActive(true);
        inventory.SetActive(true);
        inventoryWeapon.SetActive(true);
        inventoryTab1.GetComponent<Button>().interactable = false;
        inventoryTab2.GetComponent<Button>().interactable = true;
        inventoryTab3.GetComponent<Button>().interactable = true;
        addIcons();
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_SELL);
    }

    public void OpenInventoryWeapon()
    {
        showMoney.SetActive(true);
        showCurrent.SetActive(true);
        showSelecting.SetActive(true);
        shopUI.SetActive(true);
        inventory.SetActive(true);
        inventoryWeapon.SetActive(true);
        inventoryTab1.GetComponent<Button>().interactable = false;
        inventoryTab2.GetComponent<Button>().interactable = true;
        inventoryTab3.GetComponent<Button>().interactable = true;
        addIcons();
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_SELL);
    }

    public void OpenInventoryArmor()
    {
        showMoney.SetActive(true);
        showCurrent.SetActive(true);
        showSelecting.SetActive(true);
        shopUI.SetActive(true);
        inventory.SetActive(true);
        inventoryArmor.SetActive(true);
        inventoryTab1.GetComponent<Button>().interactable = true;
        inventoryTab2.GetComponent<Button>().interactable = false;
        inventoryTab3.GetComponent<Button>().interactable = true;
        addIcons();
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_SELL);
    }

    public void OpenInventoryConsumable()
    {
        Debug.Log("ConsumableShop SELL ");
        showMoney.SetActive(true);
        showCurrent.SetActive(true);
        showSelecting.SetActive(true);
        shopUI.SetActive(true);
        inventory.SetActive(true);
        inventoryConsumable.SetActive(true);
        inventoryTab1.GetComponent<Button>().interactable = true;
        inventoryTab2.GetComponent<Button>().interactable = true;
        inventoryTab3.GetComponent<Button>().interactable = false;
        addIcons();
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_CONSUMABLE_SELL);
    }

    public void OpenCraft()
    {
        showMoney.SetActive(true);
        craft.SetActive(true);
        shopUI.SetActive(true);
        showResource.SetActive(true);
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_BUY);
    }

    public void OpenUpgrade()
    {
        showMoney.SetActive(true);
        upgrade.SetActive(true);
        shopUI.SetActive(true);
        showResource.SetActive(true);
        townUI.setWindowState(DATABASE_CONSTANTS.WINDOWSTATE_BUY) ;
    }

    

    


}
