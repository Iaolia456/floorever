﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class TownUIUpdate : MonoBehaviour {

    //UI
    public Text money;
    public Text resoruceName;
    public Text resoruceQuaility;

    public Text cATK;
    public Text cDEF;
    public Text cHP;
    public Image[] cRare;
    public static int cID;
    public static int cILVL;

    public Text sATK;
    public Text sDEF;
    public Text sHP;
    public Image[] sRare;
    public static int sID;
    public static int sILVL;

    public Text price;
    public Text aATK;
    public Text aDEF;
    public Text aHP;
    public Image[] aRare;

    public ShopBase shopBase;

    public int OpenedWindow;


	// Use this for initialization
	void Start () {
        //SetCurrent(123, 456, 789, 1);
        //SetSelecting(11, 2, 1234, 2);
        //SetMoney(1234567890);
        SetCurrent("0 + 0", "0 + 0", "0 + 0", 0, 0, 0);
        SetSelecting("0 + 0", "0 + 0", "0 + 0", 0, 0, 0);
        //SetResource(new string[]{"a","bb"},new int[]{1,22});
        shopBase = gameObject.GetComponent<ShopBase>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetMoney(int value)
    {
        money.text = value + "";
    }

    public void SetPrice(int value)
    {
        price.text = value + "$";
    }

    public void SetCurrent(string atk, string def, string hp, int rare,int itemID,int itemLevel)
    {
        cATK.text = "ATK : "+atk;
        cDEF.text = "DEF : "+def;
        cHP.text = "HP : " + hp;
        cID = itemID;
        cILVL = itemLevel;
        for (int i = 0; i < 4; i++)
        {
            if (i < rare)
            {
                cRare[i].color = new Color(255, 255, 255);
            }
            else
            {
                cRare[i].color = new Color(0, 0, 0);
            }
        }
        
    }

    public void SetSelecting(string atk, string def, string hp, int rare, int itemID, int itemLevel)
    {
        sATK.text = "ATK : " + atk;
        sDEF.text = "DEF : " + def;
        sHP.text = "HP : " + hp;
        sID = itemID;
        sILVL = itemLevel;
        for (int i = 0; i < 4; i++)
        {
            if (i < rare)
            {
                sRare[i].color = new Color(255, 255, 255);
            }
            else
            {
                sRare[i].color = new Color(0, 0, 0);
            }
        }

    }

    public void BuySellButtonClicked()
    {
        if (OpenedWindow == DATABASE_CONSTANTS.WINDOWSTATE_BUY)
        {
            if (ImmortalObject.playerGold >= Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]))
            {
                ImmortalObject.playerGold = ImmortalObject.playerGold - Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]);
                ImmortalObject.playerInventory.Add(new KeyValuePair<int, int>(Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID]), 0));
            }
            SetMoney(ImmortalObject.playerGold);
        }else if(OpenedWindow == DATABASE_CONSTANTS.WINDOWSTATE_SELL)
        {
            ImmortalObject.playerGold = ImmortalObject.playerGold + (Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]) * 4) / 10;
            ImmortalObject.playerInventory.Remove(new KeyValuePair<int, int>(sID, sILVL));
            SetMoney(ImmortalObject.playerGold);
            shopBase.CloseAll();
            int itemType = Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE]);
            if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_WEAPON) { shopBase.OpenInventoryWeapon(); }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIELD) { shopBase.OpenInventoryArmor(); }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIRT) { shopBase.OpenInventoryArmor(); }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_PANT) { shopBase.OpenInventoryArmor(); }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE)   {shopBase.OpenInventoryConsumable();}

        }else if (OpenedWindow == DATABASE_CONSTANTS.WINDOWSTATE_CONSUMABLE_BUY)
        {
            if (ImmortalObject.playerGold >= Convert.ToInt32(ImmortalObject.fullItemList[15.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]))
            {
                ImmortalObject.playerGold = ImmortalObject.playerGold - Convert.ToInt32(ImmortalObject.fullItemList[15.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]);
                ImmortalObject.playerConsumable++;
                shopBase.CloseAll();
                shopBase.OpenConsumbleShop();
            }
            SetMoney(ImmortalObject.playerGold);
        }else if (OpenedWindow == DATABASE_CONSTANTS.WINDOWSTATE_CONSUMABLE_SELL)
        {
            if (ImmortalObject.playerConsumable >= 1)
            {
                ImmortalObject.playerGold = ImmortalObject.playerGold + (Convert.ToInt32(ImmortalObject.fullItemList[15.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE]) * 4) / 10;
                ImmortalObject.playerConsumable--;
                SetMoney(ImmortalObject.playerGold);
                shopBase.CloseAll();
                int itemType = DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE;
                if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_WEAPON) { shopBase.OpenInventoryWeapon(); }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIELD) { shopBase.OpenInventoryArmor(); }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIRT) { shopBase.OpenInventoryArmor(); }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_PANT) { shopBase.OpenInventoryArmor(); }
                else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_CONSUMABLE) { shopBase.OpenInventoryConsumable(); }
            }
        }
        DeviceIDUserHandler.EasyUpdateUser();
       
    }

    public void useButtonClicked()
    {
        if(OpenedWindow == DATABASE_CONSTANTS.WINDOWSTATE_SELL)
        {
            int itemType = Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE]);
            if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_WEAPON) { ImmortalObject.playerEquipments[0] = sID; ImmortalObject.playerEquipmentsLevel[0] = sILVL; }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIELD) { ImmortalObject.playerEquipments[1] = sID; ImmortalObject.playerEquipmentsLevel[1] = sILVL; }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_SHIRT) { ImmortalObject.playerEquipments[2] = sID; ImmortalObject.playerEquipmentsLevel[2] = sILVL; }
            else if (itemType == DATABASE_CONSTANTS.ITEMDATA_ITEMTYPE_EQUIPMENT_PANT) { ImmortalObject.playerEquipments[3] = sID; ImmortalObject.playerEquipmentsLevel[3] = sILVL; }

            SetCurrent(sATK.text, sDEF.text, sHP.text, Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_RARITY]), sID, sILVL);

            cATK.text = sATK.text;
            cDEF.text = sDEF.text;
            cHP.text = sHP.text;
            cID = sID;
            cILVL = sILVL;
            for (int i = 0; i < 4; i++)
            {
                if (i < Convert.ToInt32(ImmortalObject.fullItemList[sID.ToString()][DATABASE_CONSTANTS.ITEMDATA_COLUMN_RARITY]))
                {
                    cRare[i].color = new Color(255, 255, 255);
                }
                else
                {
                    cRare[i].color = new Color(0, 0, 0);
                }
            }
        }
    }

    public void SetAbility(int atk, int def, int hp, int rare)
    {
        aATK.text = "ATK : " + atk;
        aDEF.text = "DEF : " + def;
        aHP.text = "HP : " + hp;
        for (int i = 0; i < 4; i++)
        {
            if (i < rare)
            {
                aRare[i].color = new Color(255, 255, 255);
            }
            else
            {
                aRare[i].color = new Color(0, 0, 0);
            }
        }

    }

    public void SetResource(string[] name, int[] quaility)
    {
        resoruceName.text = "";
        resoruceQuaility.text = "";
        for (int i = 0; i < name.Length; i++)
        {
            resoruceName.text += name[i]+"\n";
            resoruceQuaility.text += "x " + quaility[i]+"\n";
        }
    }

    public void setWindowState(int state)
    {
        OpenedWindow = state;
    }



}
