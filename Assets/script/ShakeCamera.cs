﻿using UnityEngine;
using System.Collections;

public class ShakeCamera : MonoBehaviour {

    Vector3 c;
    float timer;
    Vector3 target;
    public GameObject look_at;
	// Use this for initialization
	void Start () {
        c = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        timer = -1;
	}
	
	// Update is called once per frame
	void Update () {
        float farX = 5;
        float farY = 1;
        float farZ = 3;
        float x = Random.Range(c.x-farX, c.x+farX);
        float y = Random.Range(c.y - farY, c.y + farY);
        float z = Random.Range(c.z - farZ, c.z + farZ);
        if (timer <= 0)
        {
            target = new Vector3(x, y, z);
            timer = 1;
        }
        else
        {
            timer -= Time.deltaTime;
        }
        transform.position = transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime*0.1f);
        transform.LookAt(look_at.transform.position);
	}
}
