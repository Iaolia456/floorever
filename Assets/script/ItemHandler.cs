﻿using System;
using System.Collections.Generic;
using UnityEngine;
public static class ItemHandler
{

    private static RESTfulHandler RESTFUL_SERVICE;
    private static string message;
    private static Boolean responseFlag;
    private static Action<string> globalCallBack;
    static ItemHandler()
    {
        message = null;
        responseFlag = false;
        globalCallBack = setMessage;
        RESTFUL_SERVICE = new RESTfulHandler(globalCallBack);
    }

    public static void setMessage(string response)
    {
        message = response;
        responseFlag = true;
    }

    public static string getMessage()
    {
        if (responseFlag)
        {
            responseFlag = false;
            return message;
        }
        else return null;
    }

    public static void addItem(int itemID, string itemName, int itemPrice = 0 , int itemType = 4,int HPBase = 0,float HPGrowth = 0, int ATKBase = 0, float ATKGrowth = 0, int DEFBase = 0, float DEFGrowth = 0,int rarity = 0,int buyable = 0)
    {
        
        responseFlag = false;
        Dictionary<string,object> jsonOBJ = new Dictionary<string,object>();
        
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID               ,itemID);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMNAME             ,itemName);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE                ,itemPrice);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE             ,itemType);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPBASE, HPBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPGROWTH, HPGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKBASE, ATKBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKGROWTH, ATKGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFBASE, DEFBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFGROWTH, DEFGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_RARITY, rarity);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_BUYABLE, buyable);

        RESTFUL_SERVICE.WebReqPOSTclassAsync(DATABASE_CONSTANTS.ITEMDATA_CLASSNAME,jsonOBJ);

    }


    public static void getItem(int itemID)
    {
        responseFlag = false;
        Dictionary<string, object> queryParam = new Dictionary<string, object>();
        queryParam.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID               ,itemID);
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.ITEMDATA_CLASSNAME,MiniJSON.Json.Serialize(queryParam));
    }

    public static void getShopItemList()
    {
        responseFlag = false;
        //TO BE : TOWN LEVEL CONSTRAIN
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.ITEMDATA_CLASSNAME);
    }

    internal static void deleteItem(string objectID)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqDELETEclass(DATABASE_CONSTANTS.ITEMDATA_CLASSNAME, objectID);
    }

    internal static void UpdateItem(string objectID, int itemID, string itemName, int itemPrice = 0, int itemType = 4, int HPBase = 0, float HPGrowth = 0, int ATKBase = 0, float ATKGrowth = 0, int DEFBase = 0, float DEFGrowth = 0, int rarity = 0, int buyable = 0)
    {
        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID, itemID);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMNAME, itemName);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_PRICE, itemPrice);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMTYPE, itemType);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPBASE, HPBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_HPGROWTH, HPGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKBASE, ATKBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_ATKGROWTH, ATKGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFBASE, DEFBase);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_DEFGROWTH, DEFGrowth);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_RARITY, rarity);
        jsonOBJ.Add(DATABASE_CONSTANTS.ITEMDATA_COLUMN_BUYABLE, buyable);

        RESTFUL_SERVICE.WebReqPUTclassAsync(DATABASE_CONSTANTS.ITEMDATA_CLASSNAME,objectID,jsonOBJ);
    }

    public static List<KeyValuePair<int, int>> RandomizeItem(List<KeyValuePair<int, int>> DropableItems, int itemLevel = 1, int DropAmount = 1)
    {
        List<int> ItemsPool = new List<int>();
        foreach (KeyValuePair<int, int> anItem in DropableItems)
        {
            for (int i = 0; i < anItem.Value; i++)
            {
                ItemsPool.Add(anItem.Key);
            }
        }
        
        List<KeyValuePair<int,int>> dropList = new List<KeyValuePair<int,int>>();
        for (int i = 0; i < DropAmount; i++ )
        {
            dropList.Add(new KeyValuePair<int,int>(ItemsPool[new System.Random().Next(0, ItemsPool.Count - 1)],itemLevel));
        }
        return dropList;
    }


}
