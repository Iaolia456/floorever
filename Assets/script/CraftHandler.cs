﻿using System;
using System.Collections.Generic;
public static class CraftHandler{

    private static RESTfulHandler RESTFUL_SERVICE;
    private static string message;
    private static Boolean responseFlag;
    private static Action<string> globalCallBack;
    static CraftHandler()
    {
        message = null;
        responseFlag = false;
        globalCallBack = setMessage;
        RESTFUL_SERVICE = new RESTfulHandler(globalCallBack);
    }

    public static void setMessage(string response)
    {
        message = response;
        responseFlag = true;
    }

    public static string getMessage()
    {
        if (responseFlag)
        {
            responseFlag = false;
            return message;
        }
        else return null;
    }

    public static void addCraft(int craftID, int itemID, int goldCost, string resourceTable)
    {
        
        responseFlag = false;
        Dictionary<string,object> jsonOBJ = new Dictionary<string,object>();

        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_CRAFTID, craftID);
        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_ITEMID, itemID);
        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_GOLD_COST, goldCost);
        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_RESOURCE_TABLE, resourceTable);
        
        RESTFUL_SERVICE.WebReqPOSTclassAsync(DATABASE_CONSTANTS.CRAFTDATA_CLASSNAME,jsonOBJ);

    }


    public static void getCraft(int craftID)
    {
        responseFlag = false;
        Dictionary<string, object> queryParam = new Dictionary<string, object>();
        queryParam.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_CRAFTID     ,craftID);
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.CRAFTDATA_CLASSNAME, MiniJSON.Json.Serialize(queryParam));
    }

    public static void getShopCraftList()
    {
        responseFlag = false;
        //TO BE : TOWN LEVEL CONSTRAIN
        RESTFUL_SERVICE.WebReqGETclass(DATABASE_CONSTANTS.CRAFTDATA_CLASSNAME);
    }

    internal static void deleteCraft(string objectID)
    {
        responseFlag = false;
        RESTFUL_SERVICE.WebReqDELETEclass(DATABASE_CONSTANTS.CRAFTDATA_CLASSNAME, objectID);
    }

    internal static void updateCraft(string objectID, int craftID, int itemID, int goldCost, string resourceTable)
    {
        responseFlag = false;
        Dictionary<string, object> jsonOBJ = new Dictionary<string, object>();
        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_CRAFTID, craftID);
        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_ITEMID, itemID);
        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_GOLD_COST, goldCost);
        jsonOBJ.Add(DATABASE_CONSTANTS.CRAFTDATA_COLUMN_RESOURCE_TABLE, resourceTable);
        
        RESTFUL_SERVICE.WebReqPUTclassAsync(DATABASE_CONSTANTS.CRAFTDATA_CLASSNAME, objectID, jsonOBJ);
    }

    public static Dictionary<int,int> RandomizeCraft(Dictionary<int, int> DropableCrafts, int craftLevel = 1,int DropAmount = 1)
    {
        List<int> CraftsPool = new List<int>();
        foreach (KeyValuePair<int, int> anCraft in DropableCrafts)
        {
            for (int i = 0; i < anCraft.Value; i++)
            {
                CraftsPool.Add(anCraft.Key);
            }
        }

        Dictionary<int,int> dropList = new Dictionary<int,int>();
        for (int i = 0; i < DropAmount; i++ )
        {
            dropList.Add(CraftsPool[new System.Random().Next(0, CraftsPool.Count - 1)],craftLevel);
        }
            return dropList;
    }


}
