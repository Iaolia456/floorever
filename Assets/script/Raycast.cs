﻿using UnityEngine;
using System.Collections;

public class Raycast : MonoBehaviour {
    const float TAP_DISTANCE_THRESHOLD = 5f;
    public RaycastHit raycastHit;
    public RaycastHit[] raycastHits;
    public bool isHit;
    public Vector3 hitPos;

    Camera heroChaseCam;
    TouchController touchController;

    Vector2 touchDownPos;
    Vector2 touchUpPos;

    void Start()
    {
        heroChaseCam = Camera.main;
        touchController = GameObject.FindGameObjectWithTag("Hero_Chase_Camera").GetComponent<TouchController>();
    }
	
	void Update () {
        if(touchController.isTappingTerrain)
            RayCast(Input.mousePosition);
        else
            isHit = false;
	}

    /* Move "Destination" object to hit point of raycast
     * @param position : position of touch screen position
     */
    void RayCast(Vector3 position)
    {
        Ray ray = heroChaseCam.ScreenPointToRay(Input.mousePosition);
        /*if (Physics.Raycast(ray.origin, ray.direction, out raycastHit, 1000.0f, 1<<8))
        {
            if (raycastHit.collider.tag == "Floor")
            {
                hitPos = new Vector3(Mathf.Round(raycastHit.point.x), raycastHit.point.y, Mathf.Round(raycastHit.point.z));
                isHit = true;
                print("raycast hit");
            }
        }*/
        raycastHits = Physics.RaycastAll(ray, 1000.0f);
        if (raycastHits.Length > 0 && !touchController.isTappingUI)
        {
            //print("we hit " + raycastHits.Length);
            isHit = true;
        }
        else //doesn't hit anything
        {
            //print("raycast miss");
            isHit = false;
        }
    }
}
