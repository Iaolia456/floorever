﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Net;
using System;
using MiniJSON;


public class TitleScript : MonoBehaviour {

    private ChangeScene changeScene;
    private RESTfulHandler httpHandler;
    public Dictionary<string, Dictionary<string, object>> fullItemList,monsterList;
    public List<KeyValuePair<int, int>> inventory;
    public int gold, consumable;
    public bool FLAG_LOADING_DATA;
    public bool FLAG_LOGGING_IN;
    public bool FLAG_LOADING_MONSTER;
    public bool FLAG_REGIS_USER;

	// Use this for initialization
	void Start () {
        changeScene = new ChangeScene();
        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };
        
        fullItemList = new Dictionary<string, Dictionary<string, object>>();
        monsterList = new Dictionary<string, Dictionary<string, object>>();
        inventory = new List<KeyValuePair<int,int>>();
        gold = 0;
        consumable = 0;

        FLAG_LOADING_DATA = false;
        FLAG_LOGGING_IN = false;
        FLAG_LOADING_MONSTER = false;
        FLAG_REGIS_USER = false;
	}
	
	// Update is called once per frame
	void Update () {
        string response = ItemHandler.getMessage();
        response = DeviceIDUserHandler.getMessage();
        response = MonsterHandler.getMessage();
        if (response != null)
        {
            if (FLAG_LOADING_DATA)
            {
                Debug.Log("Enter ITEM");
                ParseResponseItem(response);

            }
            else if(FLAG_REGIS_USER)
            {
                FLAG_REGIS_USER = false;
                loginSequence();
            }
            else if (FLAG_LOGGING_IN)
            {
                Dictionary<string, object> result = Json.Deserialize(response) as Dictionary<string, object>;
                if (result.ContainsKey("results"))
                {
                    List<object> resultList = result["results"] as List<object>;
                    if (resultList.Count > 0)
                    {
                        foreach (Dictionary<string, object> aResult in resultList)
                        {
                            DecodeResponse(aResult);
                        }
                        FLAG_LOGGING_IN = false;
                    }
                    else
                    {
                        
                        FLAG_LOGGING_IN = false;
                        regisUser();
                    }

                }
            }
            else if (FLAG_LOADING_MONSTER)
            {
                Dictionary<string, object> result = Json.Deserialize(response) as Dictionary<string, object>;
                if (result.ContainsKey("results"))
                {
                    List<object> resultList = result["results"] as List<object>;
                    if (resultList.Count > 0)
                    {
                        foreach (Dictionary<string, object> aResult in resultList)
                        {
                            DecodeMonster(aResult);
                        }
                        FLAG_LOADING_MONSTER = false;
                        ImmortalObject.fullMonsterList = monsterList;
                        loginSequence();
                    }

                }
            }
            
        }

	}

    private void ParseResponseItem(string httpResponse)
    {
        Debug.Log("DECODING ITEMS : " + httpResponse);
        Dictionary<string, object> result = MiniJSON.Json.Deserialize(httpResponse) as Dictionary<string, object>;
        if (result.ContainsKey("results"))
        {
            List<object> resultList = result["results"] as List<object>;

            foreach (Dictionary<string, object> anItem in resultList)
            {
                fullItemList.Add(anItem[DATABASE_CONSTANTS.ITEMDATA_COLUMN_ITEMID].ToString(), anItem);
            }
            ImmortalObject.fullItemList = fullItemList;
            FLAG_LOADING_DATA = false;
            queryMonsterList();
        }
            
            //////DecodeResponse(result);
            
        

    }

    private void DecodeResponse(Dictionary<string, object> result)
    {
        List<object> ItemList = Json.Deserialize((string)result[DATABASE_CONSTANTS.DEVICEID_COLUMN_INVENTORY]) as List<object>;
        gold = Convert.ToInt32(result[DATABASE_CONSTANTS.DEVICEID_COLUMN_GOLD]);
        consumable = Convert.ToInt32(result[DATABASE_CONSTANTS.DEVICEID_COLUMN_CONSUMABLE]);
        foreach (string Item in ItemList)
        {
            List<object> ItemIDLevelPair = Json.Deserialize(Item) as List<object>;
            inventory.Add(new KeyValuePair<int,int>(Convert.ToInt32(ItemIDLevelPair[0]),Convert.ToInt32(ItemIDLevelPair[1])));
                
        }

        ImmortalObject.playerInventory = inventory;
        ImmortalObject.playerGold = gold;
        ImmortalObject.playerConsumable = consumable;
        ImmortalObject.DeviceIDUserObjectID = Convert.ToString(result[DATABASE_CONSTANTS.OBJECT_ID]);

        FLAG_LOADING_DATA = false;
        ChangeSceneToTown();

    }

    private void DecodeMonster(Dictionary<string, object> aMonster)
    {
        Debug.Log("aMonster : " + Json.Serialize(aMonster));
        monsterList.Add(Convert.ToString(aMonster[DATABASE_CONSTANTS.MONSTERDATA_COLUMN_MONSTERID]),aMonster);
    }


    private void queryShopItems()
    {
        FLAG_LOADING_DATA = true;

        ItemHandler.getShopItemList();
    }

    private void loginSequence()
    {
        FLAG_LOGGING_IN = true;
        DeviceIDUserHandler.getUser(SystemInfo.deviceUniqueIdentifier);
    }

    public void TouchToStart()
    {
        queryShopItems();
    }


    public void ChangeSceneToTown()
    {
        changeScene.ToScene(3);
    }

    public void queryMonsterList()
    {
        Debug.Log("MONSTER FLAG SET TRUE");
        FLAG_LOADING_MONSTER = true;
        MonsterHandler.getMonsterList();
    }

    public void regisUser()
    {
        FLAG_REGIS_USER = true;
        List<object> itemTable = new List<object>();
        itemTable.Add(new KeyValuePair<int, int>(1, 0));
        itemTable.Add(new KeyValuePair<int, int>(2, 0));
        itemTable.Add(new KeyValuePair<int, int>(3, 0));
        itemTable.Add(new KeyValuePair<int, int>(4, 0));
        itemTable.Add(new KeyValuePair<int, int>(15, 0));
        DeviceIDUserHandler.addUser(SystemInfo.deviceUniqueIdentifier, "5", Json.Serialize(itemTable), 50);
    }
}
