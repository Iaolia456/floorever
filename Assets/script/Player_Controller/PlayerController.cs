﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class PlayerController : Pathfinding {
    public const float RUN_SPEED = 5.0f; //animation speed doesn't affect any stats
    public const int ATTACK_COST = 1; //attacking cost is equal to moving [x] tiles
    public const int MAX_DISTANCE = 15; //allow player to move only MAX_DISTANCE tiles

    //need to fetch this from server
    public Dictionary<string, int> stats; //original stats - 100% stat
    public Dictionary<string, int> currentStats; //stats effected by buff/debuff
    //etc...

    GameController controller;
    bool isAttacking = false;
    bool isMoving = false;
    // Delay to update isAttacking after start attack animation
    float delay = 0.1f;

    //pathfinding
    Vector3 destination;
    bool isDestinationValid;
    GameObject heroChaseCam;

    void Awake()
    {
        //debug
        stats = new Dictionary<string, int>();
        currentStats = new Dictionary<string, int>();
    }

	void Start () {
        heroChaseCam = GameObject.FindGameObjectWithTag("Hero_Chase_Camera");
        controller = Camera.main.GetComponent<GameController>();

        Vector3 modelPos = gameObject.transform.position;
        heroChaseCam.transform.position = new Vector3(modelPos.x, modelPos.y, modelPos.z);
	}
	
	void Update () {
        if (isMoving)
            MoveToDestination();

        if (isAttacking)
        {
            if (delay >= 0)
            {
                delay -= Time.deltaTime;
            }
            else
            {
                isAttacking = false;
                SetStateToIdle();
                delay = 0.1f;
            }
        }
	}

    int movedTile = 0;
    Action actionAfterReachDestination;
    public void WalkToDestination(Vector3 destination, bool isDestinationValid, Action callback)
    {
        this.destination = destination;
        this.isDestinationValid = isDestinationValid;
        SetStateToIdle();
        SetCurrentTileWalkable(true);

        actionAfterReachDestination = callback;
        Action pathFindingCallback = this.FinishPathFinding;
        gameObject.GetComponent<Pathfinding>().FindPath(transform.position, destination, pathFindingCallback);
    }

    private void SetCurrentTileWalkable(bool walkable)
    {
        IntVector3 currentPos = IntVector3.ToIntVector3(gameObject.transform.position);
        GameObject.FindGameObjectWithTag("Pathfinder").GetComponent<Pathfinder>().DynamicNodeUpdate(currentPos.x, currentPos.z, walkable);
        controller.currentFloor.walkable[currentPos.z, currentPos.x] = walkable;
    }

    private void MoveToDestination()
    {
        LookTo(Path[0]);
        SetStateToRun();
        Vector3 lastPath = Path[0];
        transform.position = Vector3.MoveTowards(transform.position, Path[0], Time.deltaTime * RUN_SPEED);
        
        if (Vector3.Distance(transform.position, Path[0]) < 0.1F)
        {
            movedTile++;
            Path.RemoveAt(0);
            //reach destination
            if (Path.Count == 0)
            {
                SetStateToIdle();
                if (isDestinationValid)
                    gameObject.transform.position = destination;
                else
                    gameObject.transform.position = lastPath;

                isMoving = false;
                SetCurrentTileWalkable(false);
                if (actionAfterReachDestination != null)
                    actionAfterReachDestination.Invoke();
                else
                    Camera.main.GetComponent<GameController>().ProcessTurn(movedTile);
            }
        }
    }

    private void FinishPathFinding()
    {
        if (Path.Count == 0)
        {
            print("found no path");
            Camera.main.GetComponent<GameController>().ProcessTurn(0);
        }
        else if (Path.Count >= MAX_DISTANCE) 
        {
            print("too far!");
            SetCurrentTileWalkable(false);
            Camera.main.GetComponent<GameController>().ProcessTurn(0);
        }
        else
        {
            movedTile = 0;
            isMoving = true;
        } 
    }

    public void Attack(GameObject target)
    {
        LookTo(target.transform.position);
        Camera.main.GetComponent<SFXManager>().PlayHitSound();
        SetStateToAttack();
        if (target.tag == "Enemy")
            target.GetComponent<EnemyScript>().ReceiveDamageFrom(gameObject);
    }

    public void ReceiveDamageFrom(GameObject other)
    {
        if (other.tag == "Enemy")
        {
            var enemyStat = other.GetComponent<EnemyScript>().currentStats;
            int damage = enemyStat["atk"] - currentStats["def"];
            if (damage > 0)
                currentStats["hp"] -= damage;
            else
                currentStats["hp"] -= 1;
        }

        if (currentStats["hp"] <= 0)
            Dead();
    }

    public void Dead()
    {
        controller.EndGame(1);
    }

    // Set animation to idle
    public void SetStateToIdle()
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        foreach (Animator ani in animators)
        {
            ani.SetBool("run", false);
            ani.SetBool("attack", false);
        }
        //animator.SetBool("run", false);
        //animator.SetBool("attack", false);
    }

    // Set animation to run
    public void SetStateToRun()
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        foreach (Animator ani in animators)
        {
            ani.SetBool("run", true);
            ani.SetBool("attack", false);
        }
        //animator.SetBool("run", true);
        //animator.SetBool("attack", false);
    }

    // Set animation to attack 
    public void SetStateToAttack()
    {
        Animator[] animators = GetComponentsInChildren<Animator>();
        foreach (Animator ani in animators)
        {
            ani.SetBool("run", false);
            ani.SetBool("attack", true);
        }
        //animator.SetBool("run", false);
        //animator.SetBool("attack", true);
        isAttacking = true;
    }

    /* Model will look to target position
     * @param target : position of target
     */
    public void LookTo(Vector3 target)
    {
        transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
    }
}
