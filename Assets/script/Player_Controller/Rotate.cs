﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    public float speed = 0;
    // 0 = forward, 1 = up
    public int direction = 0;

	void Update () {        
        GameObject[] g = GameObject.FindGameObjectsWithTag("Hero");
        foreach (GameObject obj in g)
        {
            if (direction == 0)
                obj.transform.Rotate(Vector3.forward, speed * Time.deltaTime);
            if (direction == 1)
                obj.transform.Rotate(Vector3.up, speed * Time.deltaTime);
        }
	}

    /* Set speed of rotation
     * @param speed : speed of rotation
     */
    public void SetSpeed(float speed)
    {
        this.speed = speed * 10;
    }
}
