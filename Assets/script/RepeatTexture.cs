﻿using UnityEngine;
using System.Collections;

public class RepeatTexture : MonoBehaviour {

	// Use this for initialization
	void Start () {
        float size_x = 1.5f;
        float size_y = 1.5f;
        transform.localScale = new Vector3(size_x, size_y, 1f);
        renderer.material.mainTextureScale = new Vector2(size_x, size_y);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
